package Experiments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import BasicsQBP.Atom;
import BasicsQBP.Proton;
import BasicsQBP.Rule;

public class SimilarityMetric 
{
	/*************************************************************************************************************/
	/** Title: measureNatural																				
	/** Description: Measure the distance between queries of the same size: 
	/** how many vars we need to change in order to get an inverse function
	/*************************************************************************************************************/
	
	public static int measureNatural(Rule q1, Rule q2)
	{
		List<Proton> lstVarsQ1 = new ArrayList<Proton>();
		List<Proton> lstVarsQ2 = new ArrayList<Proton>();
		fillLst(lstVarsQ1, q1);
		fillLst(lstVarsQ2, q2);
		
		return mapDiff(lstVarsQ1, lstVarsQ2);
	}
	
	
	/*************************************************************************************************************/
	/** Title: fillLst																				
	/** Description: Creates a list of all vars and constants in the query acoording to order of appearance
	/*************************************************************************************************************/

	private static void fillLst (List<Proton> lst, Rule q)
	{
		for (Atom a : q.getBody().getAtoms()) 
		{
			lst.addAll(a.getParams());
		}
	}
	
	
	/*************************************************************************************************************/
	/** Title: mapDiff																				
	/** Description: finds the difference between the two lists of protons
	/*************************************************************************************************************/

	private static int mapDiff (List<Proton> lst1, List<Proton> lst2)
	{
		int dist = 0;
		//init an array to track changes in vars of lst2
		List<Boolean> indicesChanged = new ArrayList<Boolean>();
		for (int i = 0; i < lst1.size(); i++) 
		{
			indicesChanged.add(false);
		}
		
		for (int i = 0; i < lst1.size(); i++) 
		{
			Proton var1 = lst1.get(i);
			Proton var2 = lst2.get(i);
			if (false == var1.equals(var2))
			{
				Collections.replaceAll(lst2, var2, var1);//in lst2 replace all occurrences of var2 with var1
				if (true == indicesChanged.get(i)) 
					dist++;
				
				updateChange(lst1, lst2, indicesChanged, var1);
			}
		}
		
		return dist;
	}
	
	
	/*************************************************************************************************************/
	/** Title: updateChange																				
	/** Description: Update the boolean array that tracks the changes in vars of lst2
	/*************************************************************************************************************/

	private static void updateChange (List<Proton> lst1, List<Proton> lst2, List<Boolean> indicesChanged, Proton var1)
	{
		for (int j = 0; j < lst1.size(); j++) 
		{
			if (lst1.get(j).equals(var1)) 
			{
				indicesChanged.set(j, true);
			}
		}
	}
}
