package Experiments;

import irisConn.ConvertTBL;
import irisConn.ParsingTools;
import algoNatural.FindConsQueryNatural;
import BasicsQBP.Atom;
import BasicsQBP.Constant;
import BasicsQBP.Rule;
import PEI.Example;
import PEI.Lin;
import PEI.Monomial;
import PEI.MonomialExample;
import PEI.PolynomialProv;

public class QueryFinding 
{
	public static void main (String [] args) throws Exception
	{
		int qnum = 1;
		//ConvertTBL.convertDB (5000, qnum);//limit to a 1000 rows of each table and choose query number 2
		for (int i = 0; i < 5; i++) 
		{
			checkQueryReverseEngineeringComplexJoin(qnum);
		}
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: exmpleDB																				
	/** Description: make an example for a query.  	 
	/*************************************************************************************************************/
	
	private static Example exmpleDB(int type)
	{
		Atom head_a = new Atom("head", new Constant("Argentina"));
		Atom head_b = new Atom("head", new Constant("Bolivia"));
		Atom head_c = new Atom("head", new Constant("Brazil"));
		
		Atom atom_a = new Atom("route", new Constant("Argentina"), new Constant("Bolivia"));
		Atom atom_b = new Atom("route", new Constant("Bolivia"), new Constant("Argentina"));
		Atom atom_c = new Atom("route", new Constant("Bolivia"), new Constant("Bolivia"));
		Atom atom_d = new Atom("route", new Constant("Brazil"), new Constant("Bolivia"));
		Atom atom_e = new Atom("route", new Constant("Bolivia"), new Constant("Brazil"));
		
		Example ex  = new Example ();
		if (0 == type)
		{
			ex.addExample(head_a, new PolynomialProv("n", new Monomial(atom_a, atom_b)));
			ex.addExample(head_b, new PolynomialProv("n", new Monomial(atom_d, new Atom(atom_e)), new Monomial(new Atom(atom_a), new Atom(atom_b)) , new Monomial(new Atom(atom_c), new Atom(atom_c))));
			ex.addExample(head_c, new PolynomialProv("n", new Monomial(new Atom(atom_d), new Atom(atom_e))));
		}
		
		else if (1 == type)
		{
			ex.addExample(head_a, new PolynomialProv("n", new Monomial(atom_a, atom_b)));
			ex.addExample(head_b, new PolynomialProv("n", new Monomial(atom_d, new Atom(atom_e)), new Monomial(new Atom(atom_a), new Atom(atom_b)) , new Monomial(new Atom(atom_c))));
			ex.addExample(head_c, new PolynomialProv("n", new Monomial(new Atom(atom_d), new Atom(atom_e))));
		}
		
		else if (2 == type)
		{
			ex.addExample(head_a, new Lin(atom_a, atom_b));
			ex.addExample(head_b, new Lin(atom_d, new Atom(atom_e), new Atom(atom_a), new Atom(atom_b) , new Atom(atom_c)));
			ex.addExample(head_c, new Lin(new Atom(atom_d), new Atom(atom_e)));
		}
		
		System.out.println(ex);
		
		return ex;
	}
	
	
	/*************************************************************************************************************/
	/** Title: exmpleDB																				
	/** Description: make an example for a query.  	 
	/*************************************************************************************************************/
	
	private static void checkQueryReverseEngineeringComplexJoin(int qnum) throws Exception
	{
		MonomialExample me = ParsingTools.makeExample();
		long startTime = System.currentTimeMillis();
		Rule q = FindConsQueryNatural.findMostSpecificQuery( me );
		long endTime = System.currentTimeMillis();
		System.out.println(q);
		double findQueryTime = (endTime - startTime);
		System.out.println("Time to find query: " + Double.toString(findQueryTime));
		Rule orig = ParsingTools.BuildRule(ConvertTBL.converedQueries(qnum));
		System.out.println("Distance from original = " + SimilarityMetric.measureNatural(orig, q));
	}
}
