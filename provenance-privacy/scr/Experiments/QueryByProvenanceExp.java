package Experiments;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.deri.iris.ConfigurationWithTopK;
import org.deri.iris.EvaluationException;
import org.deri.iris.api.basics.IPredicate;
import org.deri.iris.api.basics.IRule;
import org.deri.iris.api.basics.ITuple;
import org.deri.iris.compiler.Parser;
import org.deri.iris.compiler.ParserException;
import org.deri.iris.evaluation.stratifiedbottomup.seminaive.SemiNaiveEvaluatorWithTopK;
import org.deri.iris.facts.FactsWithTopK;
import org.deri.iris.facts.IFacts;
import org.deri.iris.optimisations.magicsets.MagicSets;
import org.deri.iris.optimisations.rulefilter.RuleFilter;
import org.deri.iris.rules.compiler.ICompiledRule;
import org.deri.iris.rules.compiler.RuleCompilerWithTopK;
import org.deri.iris.storage.IRelation;

import TopKBasics.KeyMap2;

public class QueryByProvenanceExp 
{
	/*************************************************************************************************************/
	/** Title: runIRIS_Prov																				
	/** Description: takes the IRIS file and runs an evaluation of the query on the DB using the top-k mechanism				 
	/*************************************************************************************************************/	
	
	public static void runIRIS_prov (int rowNum, int provKind, int qNum, String originalExamplePath,
			String provenanceFactsPath, String dbPath)
	{
		File program;
		KeyMap2.getInstance().Reset();
		new File(originalExamplePath).delete();
		new File(provenanceFactsPath).delete();
		program = new File(dbPath);// Create a Reader on the Datalog program file.
		//program = new File(path + "demoDB.iris");
		Reader reader;
		try 
		{
			reader = new FileReader(program);
			// Parse the Datalog program.
			Parser parser = new Parser();
			parser.parse(reader);

			// Retrieve the facts, rules and queries from the parsed program.
			Map<IPredicate, IRelation> factMap = parser.getFacts();
			List<IRule> rules = parser.getRules();

			// Create a default configuration.
			ConfigurationWithTopK configuration = new ConfigurationWithTopK();

			// Enable Magic Sets together with rule filtering.
			configuration.programOptmimisers.add(new RuleFilter());
			configuration.programOptmimisers.add(new MagicSets());

			// Convert the map from predicate to relation to a IFacts object.
			FactsWithTopK facts = new FactsWithTopK(factMap, configuration.relationFactory);

			int initialSize = 0;
			for( IPredicate predicate : facts.getPredicates() )
				initialSize += facts.get( predicate ).size();
			
			// Evaluate all queries over the knowledge base.
			List<ICompiledRule> cr = compile(rules, facts, configuration);
			SemiNaiveEvaluatorWithTopK sn = new SemiNaiveEvaluatorWithTopK();
			sn.evaluateRules(cr, facts, configuration);
			/*TopKFinder topk = new TopKFinder(cr, 3);
			Map<ITuple, List<DerivationTree2>> trees = topk.Topk("ans");*/
			
			int endSize = 0;
			for( IPredicate predicate : facts.getPredicates() )
			{
				endSize += facts.get( predicate ).size();
			}
			
			//System.out.println("number of facts derived: " + (endSize - initialSize));
			
			try
			{
				Random r = new Random();
				int rowsWritten = 0;
				Writer writer = new BufferedWriter(new FileWriter(originalExamplePath, true));//append to file
				Writer writerForRecall = new BufferedWriter(new FileWriter(provenanceFactsPath, true));//append to file
				
				for( Map.Entry<IPredicate, IRelation> entry : facts.getPredicateRelationMap().entrySet() )
				{
					IRelation relation = entry.getValue();
					List<Integer> was = new ArrayList<>();
					while(was.size() < relation.size() && rowsWritten < rowNum)
					{
						int chooseRow = r.nextInt(relation.size());
						if (was.contains(chooseRow))
						{
							continue;
						}
						was.add(chooseRow);
						
						ITuple tuple = relation.get( chooseRow );
						if (tuple.getTree() != null)
						{
							if (rowsWritten < rowNum)
							{
								//create example for learning
								if (0 == provKind)//write N(x) provenance
								{
									writer.write(tuple.getTree() + "\n"); 
								}
								else if (1 == provKind)//write trio(x) provenance
								{
									writer.write(tuple.getTree().toStringProvTRIO() + "\n");
								}
								else//write lin(x) provenance
								{
									Set<String> derivationFacts = new HashSet<>();
									/*for (DerivationTree2 tree : trees.get(tuple)) //collect all facts used in some derivation in a set to remove duplicates
									{
										derivationFacts.addAll(tree.toStringSetLIN());
									}*/									
									
									writer.write(tuple.getPredicate() + tuple);
									for (String strFact : derivationFacts) //write the set of facts
									{
										writer.write("|" + strFact);
									}
									
									writer.write("\n");
								}
								
								// create database for checking precision/recall
								// every line correlates to a provenance row
								writerForRecall.write(tuple.getTree().toStringProv() + "\n");
								rowsWritten++;
							}
						}
					}
				}
				
				//System.out.println("wrote " + rowsWritten + " rows");
				writer.close();
				writerForRecall.close();
			}
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		} 
		catch (FileNotFoundException | EvaluationException | ParserException e) 
		{
			e.printStackTrace();
		}
	}
	
	/*************************************************************************************************************/
	/** Title: wrtieQueryToFile																				
	/** Description: writes the formulated query to the IRIS file				 
	/*************************************************************************************************************/	
	
	private static void wrtieQueryToFile (String query, String path)
	{
		Writer writer = null;

		try 
		{
		    writer = new BufferedWriter(new FileWriter(path, true));
		    writer.write("\n" + query);
		    
		} 
		catch (IOException ex) 
		{
			System.out.println("IrisOperation::wrtieQueryToFile: ERROR in writing query " + query + " to file");
		} 
		finally 
		{
		   try {writer.close();} catch (Exception ex) {}
		}
	}
	
	/*************************************************************************************************************/
	/** Title: compileTopK
	/** Description: compiles the rules of IRIS				 
	/*************************************************************************************************************/	
	
	private static List<ICompiledRule> compile( List<IRule> rules, IFacts facts, ConfigurationWithTopK mConfiguration ) throws EvaluationException
	{
		assert rules != null;
		assert facts != null;
		assert mConfiguration != null;
		
		List<ICompiledRule> compiledRules = new ArrayList<ICompiledRule>();
		
		RuleCompilerWithTopK rc = new RuleCompilerWithTopK( facts, mConfiguration.equivalentTermsFactory.createEquivalentTerms(), mConfiguration );

		for( IRule rule : rules )
			compiledRules.add( rc.compileTopK( rule ) );
		
		return compiledRules;
	}
}
