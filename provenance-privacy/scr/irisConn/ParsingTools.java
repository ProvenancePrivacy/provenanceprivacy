package irisConn;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import BasicsQBP.Rule;
import BasicsQBP.Atom;
import BasicsQBP.Constant;
import BasicsQBP.Proton;
import BasicsQBP.Var;
import PEI.Monomial;
import PEI.MonomialExample;

public class ParsingTools
{
	/*************************************************************************************************************/
	/** Title: convertRow																				
	/** Description: convert a row of a tbl file to an iris fact
	/*************************************************************************************************************/

	public static MonomialExample makeExample() throws Exception
	{
		StringBuilder contentBuilder = new StringBuilder();
		try (Stream<String> stream = Files.lines( Paths.get("treesForQueryByProvenance.txt"), StandardCharsets.UTF_8)) {
			stream.forEach(s -> contentBuilder.append(s).append("\n"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return makeExample(contentBuilder.toString());
	}

	public static MonomialExample makeExample(String data) throws Exception
	{
		MonomialExample retVal = new MonomialExample();

		String[] lines = data.split("\n");
		for (String line : lines)
		{
			convertRowToExample(line, retVal);
		}

		if (!sanityCheck(retVal)) System.exit(1);

		return retVal;
	}


	/*************************************************************************************************************/
	/** Title: convertRow
	/** Description: convert a row of a tbl file to an iris fact
	/*************************************************************************************************************/

	private static void convertRowToExample (String row, MonomialExample me)
	{
		Monomial m = new Monomial();
		String [] arr = row.split("\\|");

		Atom ma;
		Atom out = BuildAtom(arr[0]);
		for (int i = 1; i < arr.length; i++)
		{
			ma = BuildAtom(arr[i]);
			m.addTuple(ma);
		}

		me.addExample(out, m);
	}


	private static Pattern pattern = Pattern.compile("'([^']+)'|\\S+");

	/*************************************************************************************************************/
	/** Title: BuildAtom
	/** Description: Takes a name string and turns it into an Atom
	/*************************************************************************************************************/

	private static Atom BuildAtom (String text)
	{
		String[] nameParams = text.split("\\(\\'");
		String name = nameParams[0].replaceAll("[^A-Za-z]+", "");
		name = name.equals("null") ? "ans" : name;

		nameParams[1] = "'" + nameParams[1].substring(0, nameParams[1].length()-1);
		String res = nameParams[1].replaceAll("','", "''");
		Matcher matcher = pattern.matcher(res);

		List<Proton> vars = new ArrayList<Proton>();//hard coded. not generic.
		while (matcher.find())
		{
			String param = matcher.group().replace("'", "").trim();
			if (!param.equals(","))
			{
				vars.add( ParseParam(param) );
			}
		}

		return new Atom (name, vars);
	}



	/*************************************************************************************************************/
	/** Title: ParseParam
	/** Description: Returns the correct param according to the name and category.
	/*************************************************************************************************************/

	private static Proton ParseParam (String param)
	{
		param.trim();
		Proton retVal = param.charAt(0) == '?' ? new Var (param.substring(1)) : new Constant (param);
		return retVal;//new Constant (param);
	}



	/*************************************************************************************************************/
	/** Title: BuildRule
	/** Description: Takes a string and turns it into a Rule
	/*************************************************************************************************************/

	public static Rule BuildRule (String str)
	{
		Pattern pattern = Pattern.compile("[a-zA-Z0-9].*?\\((.*?)\\)");
		Matcher matcher = pattern.matcher(str);
		List<Atom> atoms = new ArrayList<Atom>();

		while (matcher.find())
		{
			String[] nameParams = matcher.group().split("\\(");
			String name = nameParams[0].replaceAll("[^A-Za-z]+", "").trim();
			nameParams[1] = nameParams[1].substring(0, nameParams[1].length()-1);
			String[] params = nameParams[1].split(",");
			atoms.add( BuildAtom ( name, params ) );
		}

		return new Rule (atoms.get(0), atoms.subList(1, atoms.size()));
	}



	/*************************************************************************************************************/
	/** Title: BuildAtom
	/** Description: Takes a name string and an array of parameters and turns it into an Atom. BuildRule() helper.
	/*************************************************************************************************************/

	private static Atom BuildAtom (String name, String [] params)
	{
		Proton [] vars = new Proton [params.length];
		for (int i = 0; i < params.length; i++)
		{
			vars[i] = ParseParam(params[i].trim());
		}

		return new Atom (name, vars);
	}


	/*************************************************************************************************************/
	/** Title: sanityCheck
	/** Description: checks the example for bugs and inconsistencies
	/*************************************************************************************************************/

	public static boolean sanityCheck (MonomialExample ex) throws Exception
	{
		Map<String, Integer> relations = new HashMap<String, Integer>();

		//check all monomials are same size
		int size = ex.getMonomial(0).size();
		for (Monomial m : ex.getMonoms())
		{
			if (size != m.size())
			{
				throw new Exception("ParseDerivationTrees::sanityCheck: The monomial " + m + " is of size " + m.size() + " instead of " + size);
			}
		}

		//check all monomials of the same relation have the same num. of attr.
		for (Monomial m : ex.getMonoms())
		{
			for (Atom ma : m.getTuples())
			{
				String curRel = ma.getName();
				if (false == relations.containsKey(curRel))
					relations.put(curRel, ma.getParams().size());
			}
		}

		for (Map.Entry<String, Integer> entry : relations.entrySet())
		{
			for (Monomial m : ex.getMonoms())
			{
				for (Atom ma : m.getTuples())
				{
					if(entry.getKey().equals(ma.getName()) && entry.getValue() != ma.getParams().size())
					{
						System.out.println("ParseDerivationTrees::sanityCheck: atom " + ma + " has " + ma.getParams().size() + " attr. instead of " + entry.getValue());
						return false;
					}

				}
			}
		}

		return true;
	}
}
