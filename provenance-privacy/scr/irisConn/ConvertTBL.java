package irisConn;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class ConvertTBL 
{
	
	/*************************************************************************************************************/
	/** Title: convertDB																				
	/** Description: convert the tables of the TPC-H DB in tbl format to an iris DB				 
	/*************************************************************************************************************/	
	
	public static void convertDB (int rowLim, int qNumber)
	{
		deleteFile();
		//ConvertTBL.convertTbl("C:\\Users\\user\\git\\tpch-dbgen\\part.tbl", "part", rowLim);
		ConvertTBL.convertTbl("C:\\Users\\user\\git\\tpch-dbgen\\orders.tbl", "orders", rowLim);
		ConvertTBL.convertTbl("C:\\Users\\user\\git\\tpch-dbgen\\customer.tbl", "customer", rowLim);
		//ConvertTBL.convertTbl("C:\\Users\\user\\git\\tpch-dbgen\\supplier.tbl", "supplier", rowLim);
		ConvertTBL.convertTbl("C:\\Users\\user\\git\\tpch-dbgen\\nation.tbl", "nation", rowLim);
		ConvertTBL.convertTbl("C:\\Users\\user\\git\\tpch-dbgen\\lineitem.tbl", "lineitem", rowLim);
		//ConvertTBL.convertTbl("C:\\Users\\user\\git\\tpch-dbgen\\region.tbl", "region", rowLim);
		//ConvertTBL.convertTbl("C:\\Users\\user\\git\\tpch-dbgen\\partsupp.tbl", "partsupp", rowLim);
		writeQuery (qNumber);
	}
	
	/*************************************************************************************************************/
	/** Title: convertTable																				
	/** Description: convert a table in tbl format to an iris DB and writes it to a file				 
	/*************************************************************************************************************/	
	
	private static void convertTbl (String file, String predicate, int lineLimit)
	{
		boolean isFirstLine = true;
		boolean isLineLimit = (-1 != lineLimit) ? true : false;//check if line limit exists: -1 means no!
		try
		{
		    String line, fact;
		    int i = -2;
		    BufferedReader br = new BufferedReader(new FileReader(file)); 
		    Writer writer = new BufferedWriter(new FileWriter("TPC-H_DB" + ".iris", true));//append to file
		    while ((line = br.readLine()) != null && i < lineLimit) 
		    {
		    	fact = convertRow(predicate, line, isFirstLine);
		    	writer.write(fact + "\n");
		    	if (isLineLimit) i++;//there is a line limit, so i has to be increased
		    	isFirstLine = false;
		    }
		    
		    br.close();
		    writer.close();
		}
		
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	
	/*************************************************************************************************************/
	/** Title: writeQuery																				
	/** Description: write the TPC-H query to a file to a file				 
	/*************************************************************************************************************/	
	
	private static void writeQuery (int qNumber)
	{
		try
		{
		    Writer writer = new BufferedWriter(new FileWriter("TPC-H_DB" + ".iris", true));//append to file
		    writer.write(converedQueries(qNumber));
		    writer.close();
		}
		
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	
	/*************************************************************************************************************/
	/** Title: convertRow																				
	/** Description: convert a row of a tbl file to an iris fact				 
	/*************************************************************************************************************/	
	
	private static String convertRow (String predicate, String row, boolean isFirstLine)
	{
		String retVal = predicate + "(";
		String [] arr = row.split("\\|");
		
		for (String val : arr) 
		{
			val = val.replace(",", " ");
			retVal += "'" + val + "'" + ", ";
		}
		
		if (isFirstLine)
		{
			int attrnum = arr.length;
			System.out.println("num of attr. of " + predicate + " is " + attrnum);
		}
		
		retVal = retVal.substring(0, retVal.length() - 2);//remove extra ", " at the end
		retVal += ").";
		return retVal;
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: deleteFile																				
	/** Description: delete the DB file if it exists				 
	/*************************************************************************************************************/	
	
	private static void deleteFile()
	{
		File file = new File("TPC-H_DB" + ".iris");
    	
		if(file.delete())
		{
			System.out.println(file.getName() + " is deleted");
		}
		else
		{
			System.out.println("Delete operation is failed");
		}
	}
	
	
	/*************************************************************************************************************/
	/** Title: converedQueries																				
	/** Description: choose a converted query to check in an experiment				 
	/*************************************************************************************************************/	
	
	public static String converedQueries(int qNumber)
	{
		String query = "";
		switch(qNumber)
		{
		case 1://2 Joins
			query = "ans(?c,?od,?sd) :- customer(?c, ?n, ?a, ?nation, ?p, ?ac, ?m, ?com1), orders(?o, ?c, ?os, ?tp, ?od, ?op, ?cl, ?sp, ?com2), lineitem(?o, ?pk, ?sk, ?ln, ?q, ?ep, ?d, ?tax, ?rf, ?ls, ?sd, ?cd, ?rd, ?si, ?sm, ?com3).";
			break;
		case 2://2 Joins
			query = "ans(?s, ?name, ?s2, ?name2) :- supplier(?s, ?name, ?add, ?nation, ?p, ?acc, ?comm1), nation(?nation, ?na, ?r, ?comm2), supplier(?s2, ?name2, ?add2, ?nation, ?p2, ?acc2, ?comm3).";
			break;
		case 3://3 Joins
			query = "ans(?custkey, ?custname, ?custacc, ?nationname, ?custadd, ?custphone, ?custcomm) :- "
					+ "nation(?nationkey, ?nationname, ?regionkey, ?comm1), "
					+ "customer(?custkey, ?custname, ?custadd, ?nationkey, ?custphone, ?custacc, ?m, ?custcomm), "
					+ "orders(?orderkey, ?custkey, ?os, ?tp, ?od, ?op, ?cl, ?sp, ?com3), "
					+ "lineitem(?orderkey, ?pkey, ?suppkey, ?ln, ?q, ?ep, ?d, ?tax, ?rf, ?ls, ?sd, ?cd, ?rd, ?si, ?sm, ?com4)."; 
			break;
		case 4://4 Joins
			query = "ans(?suppacc, ?suppname, ?nationname, ?partkey, ?partmfgr, ?suppaddress, ?suppphone, ?suppcomm) :- "
					+ "part(?partkey, ?name1, ?partmfgr, ?brand, ?type, ?size, ?container, ?rp, ?com1), "
					+ "partsupp(?partkey, ?suppkey, ?aq, ?suppcost, ?com2), "
					+ "supplier(?suppkey, ?suppname, ?suppaddress, ?nationkey, ?suppphone, ?suppacc, ?suppcomm), "
					+ "nation(?nationkey, ?nationname, ?regionkey, ?comm4), "
					+ "region(?regionkey, ?regname, ?com5).";
			break;
		case 5://5 Joins
			query = "ans(?name1, ?name2) :- "
					+ "part(?pkey, ?name1, ?mfgr, ?brand, ?type, ?size, ?container, ?rp, ?com1), "
					+ "partsupp(?pkey, ?suppkey, ?aq, ?suppcost, ?com2), "
					+ "supplier(?suppkey, ?name3, ?add, ?nation, ?p, ?acc, ?com3), "
					+ "partsupp(?pkey2, ?suppkey, ?aq2, ?suppcost2, ?com4), "
					+ "part(?pkey2, ?name2, ?mfgr2, ?brand2, ?type2, ?size2, ?container2, ?rp2, ?com5), "
					+ "lineitem(?o, ?pk, ?suppkey, ?ln, ?q, ?ep, ?d, ?tax, ?rf, ?ls, ?sd, ?cd, ?rd, ?si, ?sm, ?com6).";
			break;
		case 6://6 Joins
			query = "ans(?nationname) :- customer(?custkey, ?custname, ?a, ?nationkey, ?p, ?ac, ?m, ?com1), "
					+ "orders(?orderkey, ?custkey, ?os, ?tp, ?od, ?op, ?cl, ?sp, ?com2), "
					+ "lineitem(?orderkey, ?pkey, ?suppkey, ?ln, ?q, ?ep, ?d, ?tax, ?rf, ?ls, ?sd, ?cd, ?rd, ?si, ?sm, ?com3), "
					+ "supplier(?suppkey, ?suppname, ?add, ?nationkey, ?p, ?acc, ?comm4), "
					+ "nation(?nationkey, ?nationname, ?regionkey, ?comm5), "
					+ "region(?regionkey, ?regname, ?com6).";
			break;
		case 7://7 Joins
			query = "ans(?od, ?ep, ?na2) :- part(?pkey, ?name1, ?mfgr, ?brand, ?type, ?size, ?container, ?rp, ?com1), "
					+ "lineitem(?orderkey, ?pkey, ?suppkey, ?ln, ?q, ?ep, ?d, ?tax, ?rf, ?ls, ?sd, ?cd, ?rd, ?si, ?sm, ?com2), "
					+ "orders(?orderkey, ?custkey, ?os, ?tp, ?od, ?op, ?cl, ?sp, ?com3), "
					+ "customer(?custkey, ?custname, ?a, ?nationkey, ?p, ?ac, ?m, ?com4), "
					+ "nation(?nationkey, ?na, ?regionkey, ?comm5), "
					+ "region(?regionkey, ?regname, ?com6), "
					+ "supplier(?suppkey, ?suppname, ?add, ?nationkey2, ?p, ?acc, ?comm7), "
					+ "nation(?nationkey2, ?na2, ?regionkey2, ?comm8).";
			break;
		default:
			query = "";
        	break;
		}	
		
		if (query.equals("")) 
		{
			System.out.println(qNumber + " is an illegal number! number must be between 1 and 7");
		}
		
		return query;
	}
}
