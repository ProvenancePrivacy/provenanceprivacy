package irisConn;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import ProvenancePrivacy.TestClass;

public class TPCHConvertTBL 
{
	
	/*************************************************************************************************************/
	/** Title: convertDB																				
	/** Description: convert the tables of the TPC-H DB in tbl format to an iris DB				 
	/*************************************************************************************************************/	
	
	public static void convertDB(int rowLim, int qNumber, String outputFile, String dbFilesPath)
	{
		new File(outputFile).delete();
		
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		if (7 == qNumber) 
		{
			makeResultsMap7(map);
		}
		
		if (4 == qNumber || 5 == qNumber || 7 == qNumber || 8 == qNumber || 11 == qNumber || 12 == qNumber || 15 == qNumber) 
		{
			TPCHConvertTBL.convertTbl(dbFilesPath + "part.tbl", "part", rowLim, qNumber, map, outputFile);
		}
		if (1 == qNumber || 3 == qNumber || 6 == qNumber || 7 == qNumber || 10 == qNumber || 13 == qNumber || 14 == qNumber || 15 == qNumber || 16 == qNumber) 
		{
			TPCHConvertTBL.convertTbl(dbFilesPath + "orders.tbl", "orders", rowLim, qNumber, map, outputFile);
		}
		if (1 == qNumber || 3 == qNumber || 6 == qNumber || 7 == qNumber || 13 == qNumber ) 
		{
			TPCHConvertTBL.convertTbl(dbFilesPath + "customer.tbl", "customer", rowLim, qNumber, map, outputFile);
		}
		if (2 == qNumber || (4 <= qNumber && 13 >= qNumber) || 15 == qNumber || 16 == qNumber) 
		{
			TPCHConvertTBL.convertTbl(dbFilesPath + "supplier.tbl", "supplier", rowLim, qNumber, map, outputFile);
		}
		if ((2 <= qNumber && 4 >= qNumber) || 6 == qNumber || 7 == qNumber || 9 == qNumber || (11 <= qNumber && 13 >= qNumber) || 15 == qNumber || 16 == qNumber) 
		{
			TPCHConvertTBL.convertTbl(dbFilesPath + "nation.tbl", "nation", rowLim, qNumber, map, outputFile);
		}
		if ((5 <= qNumber && 8 >= qNumber) || 1 == qNumber || 3 == qNumber || 10 == qNumber || (13 <= qNumber && 16 >= qNumber)) 
		{
			TPCHConvertTBL.convertTbl(dbFilesPath + "lineitem.tbl", "lineitem", rowLim, qNumber, map, outputFile);
		}
		if (4 == qNumber || 6 == qNumber || 7 == qNumber || 12 == qNumber) 
		{
			TPCHConvertTBL.convertTbl(dbFilesPath + "region.tbl", "region", rowLim, qNumber, map, outputFile);
		}
		if (4 == qNumber || 5 == qNumber || 8 == qNumber || 11 == qNumber || 12 == qNumber || 15 == qNumber) 
		{
			TPCHConvertTBL.convertTbl(dbFilesPath + "partsupp.tbl", "partsupp", rowLim, qNumber, map, outputFile);
		}
		
		writeQuery (qNumber, outputFile);
	}
	
	/*************************************************************************************************************/
	/** Title: convertTable																				
	/** Description: convert a table in tbl format to an iris DB and writes it to a file				 
	/*************************************************************************************************************/	
	
	private static void convertTbl (String file, String predicate, int lineLimit, int qNum, Map<String, List<String>> map,
			String outputFile)
	{
		boolean isFirstLine = true;
		boolean isLineLimit = (-1 != lineLimit) ? true : false;//check if line limit exists: -1 means no!
		try
		{
		    String line, fact;
		    int i = -2;
		    BufferedReader br = new BufferedReader(new FileReader(file)); 
		    Writer writer = new BufferedWriter(new FileWriter(outputFile, true));//append to file
		    while ((line = br.readLine()) != null && i < lineLimit) 
		    {
		    	fact = convertRow(predicate, line, isFirstLine);
		    	if (6 == qNum) 
		    	{
					if (isTupleForQuery6(fact, predicate))
						writer.write(fact + "\n");
				}
		    	else if (7 == qNum)
		    	{
		    		if (isTupleForQuery7(fact, predicate, map))
						writer.write(fact + "\n");
		    	}
		    	else
		    	{
		    		writer.write(fact + "\n");
		    	}
		    	
		    	if (isLineLimit) i++;//there is a line limit, so i has to be increased
		    	isFirstLine = false;
		    }
		    
		    br.close();
		    writer.close();
		}
		
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	
	/*************************************************************************************************************/
	/** Title: writeQuery																				
	/** Description: write the TPC-H query to a file to a file				 
	/*************************************************************************************************************/	
	
	private static void writeQuery (int qNumber, String outputFile)
	{
		try
		{
		    Writer writer = new BufferedWriter(new FileWriter(outputFile, true));//append to file
		    writer.write(converedQueries(qNumber));
		    writer.close();
		}
		
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	
	/*************************************************************************************************************/
	/** Title: convertRow																				
	/** Description: convert a row of a tbl file to an iris fact				 
	/*************************************************************************************************************/	
	
	private static String convertRow (String predicate, String row, boolean isFirstLine)
	{
		String retVal = predicate + "(";
		String [] arr = row.split("\\|");
		
		for (String val : arr) 
		{
			val = val.replace(",", " ");
			retVal += "'" + val + "'" + ", ";
		}
		
		if (isFirstLine)
		{
			int attrnum = arr.length;
			//System.out.println("num of attr. of " + predicate + " is " + attrnum);
		}
		
		retVal = retVal.substring(0, retVal.length() - 2);//remove extra ", " at the end
		retVal += ").";
		return retVal;
	}
	
	
	/*************************************************************************************************************/
	/** Title: makeResultsMap																				
	/** Description: make map of results to check rows before writing for query 7 				 
	/*************************************************************************************************************/	
	
	private static void makeResultsMap7(Map<String, List<String>> map)
	{
		String [] tables = new String [] {"part", "lineitem", "orders", "customer", "supplier"};
		ParseCSVResultsQ8 parser = new ParseCSVResultsQ8();
		parser.makeMapOfResults(map, tables);
	}
	
	/*************************************************************************************************************/
	/** Title: chooseTuplesForQuery7																				
	/** Description: allow only tuples that derive a fact in query 7				 
	/*************************************************************************************************************/	
	
	private static boolean isTupleForQuery7(String row, String pred, Map<String, List<String>> map)
	{
		if (false == map.containsKey(pred))
			return true;
		
		for (String val : map.get(pred)) 
		{
			if (row.contains(val)) 
				return true;
		}
		
		return false;
	}
	
	
	/*************************************************************************************************************/
	/** Title: chooseTuplesForQuery6																				
	/** Description: allow only tuples that derive a fact in query 6				 
	/*************************************************************************************************************/	
	
	private static boolean isTupleForQuery6(String row, String pred)
	{
		//Random rd = new Random();
		//if(rd.nextDouble() < 0.0001)
		//{
		//	return true;
		//}
	      
		String [] keys = null;
		if (pred.equals("supplier")) 
		{
			keys = new String [] {"9867", "7155", "823", "2669", "8384", "5787", "1796", "978", 
					"2829", "4777", "1096", "5291", "477", "2404", "4675", "6047", "8734", "8626", "7318", "4715", "5645", "9363", "8701", "5569", "7613"};
		}
	
		else if (pred.equals("orders")) 
		{
			keys = new String [] {"440964", "1168417", "905633", "3868359", "212870", "1978756", "729504", "835718", 
					"1967460", "144610", "430243", "2942627", "3580034", "3640352", "943270", "1478917", "4683938", "1046082", "3795685", "5066818", "1131267",
					"663840", "3211909", "2654469"};
		}
		
		else if (pred.equals("customer")) 
		{
			keys = new String [] {"Customer#000000047", "Customer#000000025", "Customer#000000127", "Customer#000000005", "Customer#000000001", "Customer#000000011", 
					"Customer#000000004", "Customer#000000044", "Customer#000000100", "Customer#000000014", "Customer#000000067", "Customer#000000002", 
					"Customer#000000062", "Customer#000000010", "Customer#000000028", "Customer#000000043", "Customer#000000016", "Customer#000000035", 
					"Customer#000000089", "Customer#000000029", "Customer#000000020", "Customer#000000046", "Customer#000000052", "Customer#000000007", "Customer#000000365"};
		}
		
		else if (pred.equals("lineitem")) 
		{
			keys = new String [] {"62661.39", "60557.50", "58503.06", "49549.36", "48649.68", "47668.53", "44419.75", "44344.57", 
					"34136.64", "32181.63", "31511.60", "30941.67", "24419.99", "22701.80", "22270.47", "20364.89", "17117.30", 
					"11661.10", "10528.80", "9073.68", "8417.12", "7363.40", "7231.08", "5530.25", "2108.18"};
		}
		
		else if (pred.equals("nation")) 
		{
			keys = new String [] {"BRAZIL", "JAPAN", "VIETNAM", "CANADA", "MOROCCO", "UNITED KINGDOM", "EGYPT", "SAUDI ARABIA", 
					"ARGENTINA", "INDONESIA", "JORDAN", "GERMANY", "ETHIOPIA", "INDIA", "ROMANIA", "IRAN", "PERU", 
					"KENYA", "ALGERIA", "RUSSIA", "FRANCE", "IRAQ", "CHINA", "UNITED STATES"};
		}
		
		else
			return true;
		
		for (String string : keys) 
		{
			if (row.contains(string))
				return true;
		}
		
		return false;
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: converedQueries																				
	/** Description: choose a converted query to check in an experiment				 
	/*************************************************************************************************************/	
	
	public static String converedQueries(int qNumber)
	{
		String query = "";
		switch(qNumber)
		{
		case 1://2 Joins TPC-H num: 3, Other paper num: Q1
			query = "ans(?c, ?od, ?sd) :- customer(?c, ?n, ?a, ?nation, ?p, ?ac, ?m, ?com1), orders(?o, ?c, ?os, ?tp, ?od, ?op, ?cl, ?sp, ?com2), lineitem(?o, ?pk, ?sk, ?ln, ?q, ?ep, ?d, ?tax, ?rf, ?ls, ?sd, ?cd, ?rd, ?si, ?sm, ?com3).";
			break;
		case 2://2 Joins Other paper num: Q2
			query = "ans(?s, ?name, ?s2, ?name2) :- supplier(?s, ?name, ?add, ?nation, ?p, ?acc, ?comm1), nation(?nation, ?na, ?r, ?comm2), supplier(?s2, ?name2, ?add2, ?nation, ?p2, ?acc2, ?comm3).";
			break;
		case 3://3 Joins TPC-H num: 10
			query = "ans(?custkey, ?custname, ?custacc, ?nationname, ?custadd, ?custphone, ?custcomm) :- "
					+ "nation(?nationkey, ?nationname, ?regionkey, ?comm1), "
					+ "customer(?custkey, ?custname, ?custadd, ?nationkey, ?custphone, ?custacc, ?m, ?custcomm), "
					+ "orders(?orderkey, ?custkey, ?os, ?tp, ?od, ?op, ?cl, ?sp, ?com3), "
					+ "lineitem(?orderkey, ?pkey, ?suppkey, ?ln, ?q, ?ep, ?d, ?tax, ?rf, ?ls, ?sd, ?cd, ?rd, ?si, ?sm, ?com4)."; 
			break;
		case 4://4 Joins TPC-H num: 2
			query = "ans(?suppacc, ?suppname, ?nationname, ?partkey, ?partmfgr, ?suppaddress, ?suppphone, ?suppcomm) :- "
					+ "part(?partkey, ?name1, ?partmfgr, ?brand, ?type, ?size, ?container, ?rp, ?com1), "
					+ "partsupp(?partkey, ?suppkey, ?aq, ?suppcost, ?com2), "
					+ "supplier(?suppkey, ?suppname, ?suppaddress, ?nationkey, ?suppphone, ?suppacc, ?suppcomm), "
					+ "nation(?nationkey, ?nationname, ?regionkey, ?comm4), "
					+ "region(?regionkey, ?regname, ?com5).";
			break;
		case 5://5 Joins, 1 Self-Join twice TPC-H num: ?, Other paper num: Q3
			query = "ans(?name1, ?name2) :- "
					+ "part(?pkey, ?name1, ?mfgr, ?brand, ?type, ?size, ?container, ?rp, ?com1), "
					+ "partsupp(?pkey, ?suppkey, ?aq, ?suppcost, ?com2), "
					+ "supplier(?suppkey, ?name3, ?add, ?nation, ?p, ?acc, ?com3), "
					+ "partsupp(?pkey2, ?suppkey, ?aq2, ?suppcost2, ?com4), "
					+ "part(?pkey2, ?name2, ?mfgr2, ?brand2, ?type2, ?size2, ?container2, ?rp2, ?com5), "
					+ "lineitem(?o, ?pk, ?suppkey, ?ln, ?q, ?ep, ?d, ?tax, ?rf, ?ls, ?sd, ?cd, ?rd, ?si, ?sm, ?com6).";
			break;
		case 6://6 Joins TPC-H num: 5  
			query = "ans(?nationname) :- ";
			if (ProvenancePrivacy.TestClass.QueryComplexity >= 6)
				query += "customer(?custkey, ?custname, ?a, ?nationkey, ?custphone, ?ac, ?m, ?com1), ";
			query += "lineitem(?orderkey, ?pkey, ?suppkey, ?ln, ?q, ?ep, ?d, ?tax, ?rf, ?ls, ?sd, ?cd, ?rd, ?si, ?sm, ?com3), ";
			if (ProvenancePrivacy.TestClass.QueryComplexity >= 4)
				query += "orders(?orderkey, ?custkey, ?os, ?tp, ?od, ?op, ?cl, ?sp, ?com2), ";
			if (ProvenancePrivacy.TestClass.QueryComplexity >= 5)
				query += "supplier(?suppkey, ?suppname, ?add, ?nationkey, ?suppphone, ?acc, ?comm4), ";
			query += "nation(?nationkey, ?nationname, ?regionkey, ?comm5), ";
			query += "region(?regionkey, ?regname, ?com6).";
			break;
		case 7://7 Joins TPC-H num: 8
			query = "ans(?na2) :- ";
			if (ProvenancePrivacy.TestClass.QueryComplexity >= 8)
				query += "part(?pkey, ?name1, ?mfgr, ?brand, ?type, ?size, ?container, ?rp, ?com1), ";
			query += "lineitem(?orderkey, ?pkey, ?suppkey, ?ln, ?q, ?ep, ?d, ?tax, ?rf, ?ls, ?sd, ?cd, ?rd, ?si, ?sm, ?com2), ";
			if (ProvenancePrivacy.TestClass.QueryComplexity >= 4)
				query += "orders(?orderkey, ?custkey, ?os, ?tp, ?od, ?op, ?cl, ?sp, ?com3), ";
			if (ProvenancePrivacy.TestClass.QueryComplexity >= 5)
				query += "customer(?custkey, ?custname, ?a, ?nationkey, ?custphone, ?ac, ?m, ?com4), ";
			if (ProvenancePrivacy.TestClass.QueryComplexity >= 6)
				query += "nation(?nationkey, ?na, ?regionkey, ?comm5), ";
			if (ProvenancePrivacy.TestClass.QueryComplexity >= 7)
				query += "region(?regionkey, ?regname, ?com6), ";
			query += "supplier(?suppkey, ?suppname, ?add, ?nationkey2, ?suppphone, ?acc, ?comm7), ";
			query += "nation(?nationkey2, ?na2, ?regionkey2, ?comm8).";
			break;
		case 10://Other paper num: Q4
			query = "ans(?name1, ?name2) :- "
					+ "supplier(?suppkey, ?name1, ?add, ?nation, ?p, ?acc, ?com1), "
					+ "lineitem(?orderkey, ?pk, ?suppkey, ?ln, ?q, ?ep, ?d, ?tax, ?rf, ?ls, ?sd, ?cd, ?rd, ?si, ?sm, ?com2), "
					+ "orders(?orderkey, ?custkey, ?os, ?tp, ?od, ?op, ?cl, ?sp, ?com3), "
					+ "lineitem(?orderkey, ?pk2, ?suppkey2, ?ln2, ?q2, ?ep2, ?d2, ?tax2, ?rf2, ?ls2, ?sd2, ?cd2, ?rd2, ?si2, ?sm2, ?com4), "
					+ "supplier(?suppkey2, ?name2, ?add2, ?nation2, ?p2, ?acc2, ?com5).";
			break;
		case 11://Other paper num: Q5
			if (ProvenancePrivacy.TestClass.QueryComplexity >= 4)
				query = "ans(?name1, ?name2) :- ";
			else
				query = "ans(?name1) :- ";
			query += "partsupp(?pkey2, ?suppkey, ?aq1, ?suppcost1, ?com2), ";
			if (ProvenancePrivacy.TestClass.QueryComplexity >= 5)
				query += "partsupp(?pkey2, ?suppkey2, ?aq2, ?suppcost2, ?com4), ";
			if (ProvenancePrivacy.TestClass.QueryComplexity >= 4)
				query += "supplier(?suppkey2, ?name2, ?add2, ?nationkey, ?p2, ?acc2, ?com5), ";
			if (ProvenancePrivacy.TestClass.QueryComplexity >= 6)
				query += "nation(?nationkey, ?na2, ?regionkey2, ?comm6), ";
			if (ProvenancePrivacy.TestClass.QueryComplexity >= 7)
				query += "part(?pkey2, ?partname, ?mfgr2, ?brand2, ?type2, ?size2, ?container2, ?rp2, ?com3), ";
			query += "supplier(?suppkey, ?name1, ?add, ?nationkey, ?p, ?acc, ?com1).";
			break;
		case 12://Other paper num: Q6
			query = "ans(?name1, ?name2) :- "
					+ "supplier(?suppkey, ?name1, ?add, ?nationkey1, ?p, ?acc, ?com1), "
					+ "partsupp(?pkey2, ?suppkey, ?aq1, ?suppcost1, ?com2), "
					+ "part(?pkey2, ?partname, ?mfgr2, ?brand2, ?type2, ?size2, ?container2, ?rp2, ?com3), "
					+ "partsupp(?pkey2, ?suppkey2, ?aq2, ?suppcost2, ?com4), "
					+ "supplier(?suppkey2, ?name2, ?add2, ?nationkey2, ?p2, ?acc2, ?com5), "
					+ "nation(?nationkey1, ?na1, ?regionkey, ?comm6), "
					+ "region(?regionkey, ?regname, ?com7), "
					+ "nation(?nationkey2, ?na2, ?regionkey, ?comm8).";
			break;
		case 13://TPC-H num: 7
			query = "ans(?suppnation) :- ";
			query += "supplier(?suppkey, ?name1, ?add, ?nationkey1, ?p, ?acc, ?com1), ";
			query += "lineitem(?orderkey, ?pk, ?suppkey, ?ln, ?q, ?ep, ?d, ?tax, ?rf, ?ls, ?sd, ?cd, ?rd, ?si, ?sm, ?com2), ";
			if (ProvenancePrivacy.TestClass.QueryComplexity >= 4)
				query += "orders(?orderkey, ?custkey, ?os, ?tp, ?od, ?op, ?cl, ?sp, ?com3), ";
			if (ProvenancePrivacy.TestClass.QueryComplexity >= 5)
				query += "customer(?custkey, ?custname, ?a, ?nationkey2, ?custphone, ?ac, ?m, ?com4), ";
			if (ProvenancePrivacy.TestClass.QueryComplexity >= 6)
				query += "nation(?nationkey2, ?custnation, ?regionkey2, ?comm5), ";
			query += "nation(?nationkey1, ?suppnation, ?regionkey, ?comm4).";
			break;
		case 14://TPC-H num: 4
			query = "ans(?rd) :- "
					+ "lineitem(?orderkey, ?pk, ?suppkey, ?ln, ?q, ?ep, ?d, ?tax, ?rf, ?ls, ?sd, ?cd, ?rd, ?si, ?sm, ?com2), "
					+ "orders(?orderkey, ?custkey, ?os, ?tp, ?od, ?op, ?cl, ?sp, ?com3).";
			break;
		case 15://TPC-H num: 9
			query = "ans(?suppnation) :- ";
			if (ProvenancePrivacy.TestClass.QueryComplexity >= 6)
				query += "part(?pkey, ?name1, ?mfgr, ?brand, ?type, ?size, ?container, ?rp, ?com1), ";
			query += "supplier(?suppkey, ?suppname, ?add, ?nationkey1, ?p, ?acc, ?com2), ";
			query += "lineitem(?orderkey, ?pkey, ?suppkey, ?ln, ?q, ?ep, ?d, ?tax, ?rf, ?ls, ?sd, ?cd, ?rd, ?si, ?sm, ?com3), ";
			if (ProvenancePrivacy.TestClass.QueryComplexity >= 4)
				query += "partsupp(?pkey, ?suppkey, ?aq2, ?suppcost2, ?com5), ";
			if (ProvenancePrivacy.TestClass.QueryComplexity >= 5)
				query += "orders(?orderkey, ?custkey, ?os, ?tp, ?od, ?op, ?cl, ?sp, ?com4), ";
			query += "nation(?nationkey1, ?suppnation, ?regionkey, ?com6).";
			break;
		case 16://TPC-H num: 21
			query = "ans(?suppnation) :- ";
				query += "supplier(?suppkey, ?suppname, ?add, ?nationkey1, ?p, ?acc, ?com1), ";
				query += "lineitem(?orderkey, ?pkey, ?suppkey, ?ln, ?q, ?ep, ?d, ?tax, ?rf, ?ls, ?sd, ?cd, ?rd, ?si, ?sm, ?com2), ";
				if (ProvenancePrivacy.TestClass.QueryComplexity >= 4)
					query += "lineitem(?orderkey, ?pkey2, ?suppkey2, ?ln2, ?q2, ?ep2, ?d2, ?tax2, ?rf2, ?ls2, ?sd2, ?cd2, ?rd2, ?si2, ?sm2, ?com3), ";
				if (ProvenancePrivacy.TestClass.QueryComplexity >= 5)	
					query += "lineitem(?orderkey, ?pkey3, ?suppkey3, ?ln3, ?q3, ?ep3, ?d3, ?tax3, ?rf3, ?ls3, ?sd3, ?cd3, ?rd3, ?si3, ?sm3, ?com4), ";
				if (ProvenancePrivacy.TestClass.QueryComplexity >= 6)
					query += "orders(?orderkey, ?custkey, ?os, ?tp, ?od, ?op, ?cl, ?sp, ?com5), ";
				query += "nation(?nationkey1, ?suppnation, ?regionkey, ?com6).";
			break;
		default:
			query = "";
        	break;
		}	
		
		if (query.equals("")) 
		{
			System.out.println(qNumber + " is an illegal number! number must be between 1 and 7");
			System.exit(-1);
		}
		
		return query;
	}
}
