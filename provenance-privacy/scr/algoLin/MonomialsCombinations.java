package algoLin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import BasicsQBP.Atom;
import PEI.Monomial;


public class MonomialsCombinations 
{
	/*************************************************************************************************************/
	/** Title: allCombos																				
	/** Description: Finds all combinations of a monomial 	 
	/*************************************************************************************************************/
	
	public static List<Monomial> allCombos (Monomial monom, int size)
	{
		//expand the monomial
		Monomial exMonom = new Monomial();
		expandMonomial(monom, size, exMonom);
		
		//generate all subsets of the correct size
		List<List<Atom>> subsets = allCombosOfSize(exMonom.getTuples(), size);
		
		//lose duplicates
		Set<List<Atom>> hs = new HashSet<List<Atom>>(subsets);
		
		return convertToMonoms(hs);
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: getSubsets																				
	/** Description: Generates all subsets of a certain size 	 
	/*************************************************************************************************************/
	
	private static List<List<Atom>> allCombosOfSize(List<Atom> superSet, int k) 
	{
	    List<List<Atom>> res = new ArrayList<>();
	    getSubsets(superSet, k, 0, new ArrayList<Atom>(), res);
	    return res;
	}
	
	
	/*************************************************************************************************************/
	/** Title: getSubsets																				
	/** Description: Finds all combinations of a monomial with non-identical atoms	 
	/*************************************************************************************************************/
	
	private static void getSubsets(List<Atom> superSet, int k, int idx, List<Atom> current, List<List<Atom>> solution) 
	{
	    //successful stop clause
	    if (current.size() == k) 
	    {
	        solution.add(new ArrayList<Atom>(current));
	        return;
	    }
	    
	    //unseccessful stop clause
	    if (idx == superSet.size()) 
	    	return;
	    Atom x = superSet.get(idx);
	    current.add(x);
	    
	    //"guess" x is in the subset
	    getSubsets(superSet, k, idx+1, current, solution);
	    current.remove(x);
	    
	    //"guess" x is not in the subset
	    getSubsets(superSet, k, idx+1, current, solution);
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: expandMonomial																				
	/** Description: Expands the monomial using duplication of identical atoms	 
	/*************************************************************************************************************/
	
	private static void expandMonomial(Monomial monom, int size, Monomial retVal) 
	{
		for (Atom atm : monom.getTuples()) 
		{
			for (int i = 0; i < size; i++) 
			{
				retVal.addTuple(atm);
			}
		}
	}
	
	
	/*************************************************************************************************************/
	/** Title: convertToMonoms																				
	/** Description: Convert all lists to Monomials	 
	/*************************************************************************************************************/
	
	private static List<Monomial> convertToMonoms (Set<List<Atom>> subsets)
	{
		List<Monomial> retVal = new ArrayList<Monomial>();
		for (List<Atom> list : subsets) 
		{
			retVal.add( new Monomial(list) );
		}
		
		return retVal;
	}
}
