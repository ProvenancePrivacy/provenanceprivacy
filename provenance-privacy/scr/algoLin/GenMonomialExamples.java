package algoLin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import BasicsQBP.Atom;
import PEI.Monomial;
import PEI.MonomialExample;

public class GenMonomialExamples 
{
	
	/*************************************************************************************************************/
	/** Title: monomialExampleCombos																				
	/** Description: Create all monomial example combos that contain all the provenance 	 
	/*************************************************************************************************************/
	
	public static List<MonomialExample> monomialExampleCombos(List<Monomial> monoms, Monomial allProv, Atom outputTuple)
	{
		//get all combinations of monomials that contain all the Lin(X) provenance
		List<MonomialExample> examples = new ArrayList<MonomialExample>();
		List<List<Monomial>> subsets = genMonomialsubsets(monoms, allProv);
		siftMonomials(subsets, allProv);
		
		//generate monomial examples from each legal combination of monomials 
		for (List<Monomial> list : subsets) 
		{
			examples.add( genMonomExample(list, outputTuple) );
		}
		
		return examples;
	}
	
	
	/*************************************************************************************************************/
	/** Title: genMonomialsubsets																				
	/** Description: Generate all subsets of monomials				 
	/*************************************************************************************************************/
	
	private static List<List<Monomial>> genMonomialsubsets(List<Monomial> monoms, Monomial allProv) 
	{
        List<List<Monomial>> res = new ArrayList<List<Monomial>>();
        if(0 == monoms.size())
        {
            res.add(new ArrayList<Monomial>());
            return res;
        }
        
        Monomial head = monoms.get(0);
        //List<Monomial> rest = new ArrayList<Monomial>(monoms.subList(1, monoms.size()));
        List<Monomial> rest = monoms.subList(1, monoms.size());
        
        for(List<Monomial> list : genMonomialsubsets(rest, allProv))
        {
            List<Monomial> newList = new LinkedList<Monomial>();
            newList.add(head);
            newList.addAll(list);
            res.add(list);
            res.add(newList);
        }
        
        return res;
    }
	
	
	
	/*************************************************************************************************************/
	/** Title: siftMonomials																				
	/** Description: 				 
	/*************************************************************************************************************/
	
	private static void siftMonomials (List<List<Monomial>> subsets, Monomial allProv)
	{
		List<List<Monomial>> illegal = new ArrayList<List<Monomial>> ();
		for (List<Monomial> list : subsets) 
		{
			if (false == isContainingAllProv(list, allProv)) 
				illegal.add(list);
		}
		
		subsets.removeAll(illegal);
	}
	
	
	/*************************************************************************************************************/
	/** Title: isContainingAllProv																				
	/** Description: Check that all the provenance is contained in the monoms				 
	/*************************************************************************************************************/
	
	private static boolean isContainingAllProv(List<Monomial> monoms, Monomial allProv) 
	{
		//join all monoms into one set
		Set<Atom> hs = new HashSet<Atom>();
		for (Monomial monomial : monoms) 
		{
			hs.addAll(monomial.getTuples());
		}
		
		//check that all the provenance is contained in the set		
		return hs.containsAll(allProv.getTuples());
	}
	
	
	/*************************************************************************************************************/
	/** Title: genMonomExample																				
	/** Description: Generate a monomial example				 
	/*************************************************************************************************************/

	private static MonomialExample genMonomExample(List<Monomial> list, Atom ouputTuple)
	{
		MonomialExample retVal = new MonomialExample();
		for (Monomial monomial : list) 
		{
			retVal.addExample(new Atom(ouputTuple), new Monomial(monomial));
		}
		
		return retVal;
	}
}
