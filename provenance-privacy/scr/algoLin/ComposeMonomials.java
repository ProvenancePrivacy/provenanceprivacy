package algoLin;

import heuristicTools.AtomArraySum;
import heuristicTools.CoveredAttributesTools;
import heuristicTools.SpreadOutSetFinder;

import java.util.ArrayList;
import java.util.List;

import BasicsQBP.Atom;
import PEI.Monomial;
import PEI.MonomialExample;

public class ComposeMonomials 
{

	/*************************************************************************************************************/
	/** Title: findMonomials																				
	/** Description: composes the monomials out of the sets				 
	/*************************************************************************************************************/	
	
	public static MonomialExample findMonomials (MonomialExample ex, int size)
	{
		MonomialExample retVal = new MonomialExample();
		List<List<AtomArraySum>> maps = new ArrayList<List<AtomArraySum>>();
		List<AtomArraySum> spreadOut = SpreadOutSetFinder.findSpreadOutSet(ex, maps);
		List<AtomArraySum> mustHaves = new ArrayList<AtomArraySum>();
		
		//see what tuples must appear in any monom
		for (AtomArraySum aas : spreadOut) 
		{
			if (aas.isInTheCover())
				mustHaves.add(aas);
		}
		
		//find the tuples needed for the cover in all the monomials
		for (int i = 0; i < maps.size(); i++)
		{
			fitCover(maps.get(i), mustHaves);
		}
		
		for (int i = 0; i < maps.size(); i++)
		{
			convertToMonomials(maps.get(i), ex.getOutputTuple(i), retVal, size);
		}
		
		return retVal;
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: fitCover																				
	/** Description: set tuples that cover the same attr. as the tuples in the most spread out monomial				 
	/*************************************************************************************************************/	
	
	private static void fitCover (List<AtomArraySum> lst, List<AtomArraySum> mustHaves)
	{
		for (AtomArraySum covering : mustHaves) 
		{
			for (AtomArraySum aas : lst) 
			{
				if (CoveredAttributesTools.coverSameAttr(covering, aas))
				{
					aas.setInTheCover(true);
					break;
				}
			}
		}
	}
	
	
	/*************************************************************************************************************/
	/** Title: convertToMonomials																				
	/** Description: set tuples that cover the same attr. as the tuples in the most spread out monomial				 
	/*************************************************************************************************************/	
	
	private static void convertToMonomials (List<AtomArraySum> lst, Atom out, MonomialExample ex, int numOfNonCovering)
	{
		List<Atom> mustHaves = new ArrayList<Atom>();
		for (AtomArraySum aas : lst) 
		{
			if (true == aas.isInTheCover())
				mustHaves.add(aas.getTuple());
		}
		
		List<Atom> nonCovering = new ArrayList<Atom>();
		for (AtomArraySum aas : lst) 
		{
			if (false == aas.isInTheCover())
			{
				nonCovering.add(aas.getTuple());
			}
			
			if (nonCovering.size() == numOfNonCovering) 
			{
				Monomial m = new Monomial();
				m.getTuples().addAll( new ArrayList<Atom>(nonCovering) );
				m.getTuples().addAll(mustHaves);
				ex.addExample(out, m);
				nonCovering.clear(); // empty list to start new monom
			}
		}
	}
}
