package convertEgToMonomialEg;

import algoMinimal.Unification;
import BasicsQBP.Atom;
import BasicsQBP.Constant;
import BasicsQBP.Rule;
import PEI.Example;
import PEI.Monomial;
import PEI.MonomialExample;
import PEI.PolynomialProv;
import PEI.Provenance;

public class TestClass 
{
	public static void main (String [] args)
	{
		System.out.println(checkConverter());
	}
	
	
	private static MonomialExample checkConverter ()
	{
		Atom head_a = new Atom("head", new Constant("a"), new Constant("b"));
		Atom head_b = new Atom("head", new Constant("b"), new Constant("c"));
		Atom head_c = new Atom("head", new Constant("g"), new Constant("h"));
		Atom atom_a = new Atom("route", new Constant("a"), new Constant("b"));
		Atom atom_b = new Atom("route", new Constant("b"), new Constant("c"));
		Atom atom_c = new Atom("route", new Constant("b"), new Constant("d"));
		Atom atom_d = new Atom("route", new Constant("c"), new Constant("d"));
		
		Atom atom_e = new Atom("route", new Constant("a"), new Constant("b"));
		Atom atom_f = new Atom("route", new Constant("b"), new Constant("d"));
		
		Example ex = new Example();
		ex.addExample(head_a, new PolynomialProv("N", new Monomial(atom_a, atom_b), new Monomial(atom_c, atom_d)));
		ex.addExample(head_b, new PolynomialProv("N", new Monomial(atom_c, atom_d)));
		System.out.println(ex);
		return Converter.convertPolynomial(ex); 
	}
}
