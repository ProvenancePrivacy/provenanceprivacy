package convertEgToMonomialEg;

import java.util.Map;

import BasicsQBP.Atom;
import PEI.Example;
import PEI.Monomial;
import PEI.MonomialExample;
import PEI.PolynomialProv;
import PEI.Provenance;

public class Converter 
{
	/*************************************************************************************************************/
	/** Title: convert																				
	/** Description: Takes an N[X] or TRIO[X] example and converts it to a MonomialExample	 
	/*************************************************************************************************************/
	
	public static MonomialExample convertPolynomial(Example ex)
	{
		MonomialExample retVal = new MonomialExample();
		for (Map.Entry<Atom, Provenance> entry : ex.getPei().entrySet()) 
		{
			PolynomialProv prov = ((PolynomialProv)entry.getValue());
			if ( prov.getMonoms().size() > 1 ) 
			{
				for (Monomial monom : prov.getMonoms()) 
				{
					retVal.addExample(entry.getKey(), monom);
				}
			}
			
			else
			{
				retVal.addExample(entry.getKey(), prov.getMonoms().get(0));
			}
		}
		
		return retVal;
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: convert																				
	/** Description: Takes an Lin[X] example and converts it to a MonomialExample	 
	/*************************************************************************************************************/
	
	public static MonomialExample convertLin(Example ex)
	{
		MonomialExample retVal = new MonomialExample();
		for (Map.Entry<Atom, Provenance> entry : ex.getPei().entrySet()) 
		{
			retVal.addExample(entry.getKey(), (Monomial) entry.getValue());
		}
		
		return retVal;
	}
}
