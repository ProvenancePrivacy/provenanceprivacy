package algoNatural;

import java.util.ArrayList;
import java.util.List;

import BasicsQBP.Atom;
import BasicsQBP.Proton;

public class HeadEdge extends Edge
{
	private List<List<Proton>> attrVectors;

	/*************************************************************************************************************/
	/** Title: HeadEdge																				
	/** Description: Constructor to unify the two heads				 
	/*************************************************************************************************************/

	public HeadEdge (Atom from, Atom to)
	{
		super(from, to);
		attrVectors = new ArrayList<List<Proton>>();
		getAttr(attrVectors);
	}

	
	
	public List<List<Proton>> getAttrVectors() {
		return attrVectors;
	}

	
	
	public void setAttrVectors(List<List<Proton>> lst) {
		this.attrVectors = lst;
	}
	
	
}
