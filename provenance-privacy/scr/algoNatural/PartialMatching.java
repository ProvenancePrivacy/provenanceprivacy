package algoNatural;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import algoTrio.FullExpansion;
import BasicsQBP.Atom;
import BasicsQBP.Proton;
import PEI.Monomial;
import PEI.MonomialExample;

public class PartialMatching 
{
	/*************************************************************************************************************/
	/** Title: match																				
	/** Description: Finds the partial matchings in the graph of the first two rows 				 
	/*************************************************************************************************************/

	public static Set<List<BodyEdge>> match (MonomialExample monomEx) throws Exception
	{
		if (monomEx.getMonoms().size() < 2) 
		{
			throw new Exception("PartialMatching::match: the example has less than two rows!");
		}
		
		diversify(monomEx);
		
		MonomialExample monomExTwoRows = new MonomialExample();
		
		//Set the full expansion of the first two rows
		monomExTwoRows.addExample(monomEx.getOutputTuple(0), monomEx.getMonomial(0));
		monomExTwoRows.addExample(monomEx.getOutputTuple(1), monomEx.getMonomial(1));
		giveNewNames(monomExTwoRows);
		monomExTwoRows = FullExpansion.expand(monomExTwoRows);
		
		//Create the head edge containing all the vectors of attributes
		Atom first = monomExTwoRows.getOutputTuple(0);
		Atom second = monomExTwoRows.getOutputTuple(1);
		HeadEdge headEdge = new HeadEdge(first, second);
		
		List<BodyEdge> graph = new ArrayList<BodyEdge>();
		createGraph(monomExTwoRows, headEdge, graph);
		List<List<BodyEdge>> subsets = genSubsets(graph, monomExTwoRows);
		findMatchings(headEdge, monomEx.getMonomial(0).size(), subsets);
		giveOldNames(subsets);
		
		return removeDuplicateMatchings(subsets);
	}
	
	
	/*************************************************************************************************************/
	/** Title: diversify																				
	/** Description: switch rows in the example to have two different output tuples in rows 0 and 1 for a more general query				 
	/*************************************************************************************************************/

	private static void diversify (MonomialExample monomEx)
	{
		Atom tempA;
		Monomial tempM;
		if (monomEx.getOutputTuple(0).equals(monomEx.getOutputTuple(1))) 
		{
			for (int i = 0; i < monomEx.rowNumber(); i++) 
			{
				if (false == monomEx.getOutputTuple(0).equals(monomEx.getOutputTuple(i))) 
				{
					tempA = monomEx.getOutputTuple(1);
					tempM = monomEx.getMonomial(1);
					monomEx.setOutputTupleAt(1,  monomEx.getOutputTuple(i) );
					monomEx.setMonomialAt(1,  monomEx.getMonomial(i) );
					monomEx.setOutputTupleAt(i,  tempA );
					monomEx.setMonomialAt(i,  tempM );
				}
			}
		}
	}
	
	
	/*************************************************************************************************************/
	/** Title: createGraph																				
	/** Description: Create the graph from which a matching can be found				 
	/*************************************************************************************************************/

	private static void createGraph (MonomialExample monomExTwoRows, HeadEdge headEdge, List<BodyEdge> graph)
	{
		Atom first, second;
		String origNameFirst, origNameSecond;
		for (int i = 0; i < monomExTwoRows.getMonomial(0).getTuples().size(); i++) 
		{//Create the head edge containing all the vectors of attributes
			first = monomExTwoRows.getMonomial(0).getTuples().get(i);
			second = monomExTwoRows.getMonomial(1).getTuples().get(i);
			
			//check that both names are the same to make an edge that makes sense
			origNameFirst = first.getName().split("\\_")[0];
			origNameSecond = second.getName().split("\\_")[0];
			if (origNameFirst.equals(origNameSecond)) 
				graph.add( new BodyEdge(first, second, headEdge.getAttrVectors()) );
		}
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: genSubsets																				
	/** Description: Generate all subsets of edges of the graph (Could be more efficient...)				 
	/*************************************************************************************************************/
	
	private static List<List<BodyEdge>> genSubsets(List<BodyEdge> graph, MonomialExample monomExTwoRows) 
	{
        List<List<BodyEdge>> res = new ArrayList<List<BodyEdge>>();
        if(0 == graph.size())
        {
            res.add(new ArrayList<BodyEdge>());
            return res;
        }
        
        BodyEdge head = graph.get(0);
        List<BodyEdge> rest = new ArrayList<BodyEdge>(graph.subList(1, graph.size()));
        
        for(List<BodyEdge> list : genSubsets(rest, monomExTwoRows))
        {
            List<BodyEdge> newList = new LinkedList<BodyEdge>();
            newList.add(head);
            newList.addAll(list);
            if (isLegalMatching(list)) 
            {
            	res.add(list);
			}
            if (isLegalMatching(newList)) 
            {
            	res.add(newList);
			}
        }
        
        return res;
    }
	
	
	
	/*************************************************************************************************************/
	/** Title: isLegalMatching																				
	/** Description: checks of a subset has an edges with common end-points 				 
	/*************************************************************************************************************/
	
	private static boolean isLegalMatching (List<BodyEdge> subset)
	{	
		HashSet<Atom> hsFroms = new HashSet<Atom>();
		HashSet<Atom> hsTos = new HashSet<Atom>();
		for (BodyEdge e : subset) 
		{	
			hsFroms.add(e.getFrom());
			hsTos.add(e.getTo());
		}
	
		return (hsFroms.size() == subset.size()) && (hsTos.size() == subset.size());
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: findMatchings																				
	/** Description: Find subsets of edges of the graph that cover all attr. of the head 				 
	/*************************************************************************************************************/
	
	private static void findMatchings(HeadEdge headEdge, int monomSize, List<List<BodyEdge>> subsets)
	{
		List<List<BodyEdge>> removeLst = new ArrayList<List<BodyEdge>>();
		for (List<BodyEdge> list : subsets) //Iterate over all subsets
		{
			//Assemble the indices covered by each subset  
			List<List<Proton>> curAttr = new ArrayList<List<Proton>>();
			
			for (BodyEdge edge : list) 
			{
				curAttr.addAll(edge.getAttrCovered());
			}
			
			if (false == curAttr.containsAll(headEdge.getAttrVectors()) || list.isEmpty() 
					|| monomSize > list.size()) //If the head attr. aren't covered, remove the subset  
			{
				removeLst.add(list);
			}
		}
		
		subsets.removeAll(removeLst);
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: removeDuplicateMatchings																				
	/** Description: Finds and removes duplicate matchings 				 
	/*************************************************************************************************************/
	
	private static Set<List<BodyEdge>> removeDuplicateMatchings(List<List<BodyEdge>> subsets)
	{
		int beforeSize, afterSize;
		Set<Set<BodyEdge>> hsSubsets = new HashSet<Set<BodyEdge>>();
		Set<List<BodyEdge>> retVal = new HashSet<List<BodyEdge>>();
		for (List<BodyEdge> list : subsets) 
		{
			Set<BodyEdge> hsMatching = new HashSet<BodyEdge>(list);//to recognize same matching in different order
			beforeSize = hsSubsets.size();
			hsSubsets.add(hsMatching);
			afterSize = hsSubsets.size();
			
			if (beforeSize < afterSize)//unique matching inserted 
			{
				retVal.add(list);
			}
		}
		
		return retVal;
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: giveNewNames																				
	/** Description: Give each atom a unique name in order to check matchings 				 
	/*************************************************************************************************************/
	
	private static void giveNewNames (MonomialExample monomExTwoRows)
	{
		Atom first, second;
		int j = 0;
		for (int i = 0; i < monomExTwoRows.getMonomial(0).getTuples().size(); i++) 
		{
			first = monomExTwoRows.getMonomial(0).getTuples().get(i);
			second = monomExTwoRows.getMonomial(1).getTuples().get(i);
			first.setName( first.getName() + "_" + j );
			second.setName( second.getName() + "_" + (++j) );
			++j;
		}
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: giveOldNames																				
	/** Description: Give each atom its old name 				 
	/*************************************************************************************************************/
	
	private static void giveOldNames (List<List<BodyEdge>> subsets)
	{
		for (List<BodyEdge> list : subsets) 
		{
			for (BodyEdge bodyEdge : list) 
			{
				bodyEdge.getFrom().setName( bodyEdge.getFrom().getName().split("_")[0] );
				bodyEdge.getTo().setName( bodyEdge.getTo().getName().split("_")[0] );
			}
		}
	}
}
