package algoNatural;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import BasicsQBP.Atom;
import BasicsQBP.Rule;
import PEI.Monomial;
import PEI.MonomialExample;

public class ConsistentCheck 
{
	/*************************************************************************************************************/
	/** Title: isQueryConsistent																				
	/** Description: Check if the query is consistent with the rest of the example 				 
	/*************************************************************************************************************/

	public static boolean isQueryConsistent (MonomialExample me, Rule q, int startAtRow)
	{
		for (int i = startAtRow; i < me.rowNumber(); i++) 
		{
			Rule query = new Rule(q);
			setMonomUntaken(me.getMonomial(i));
			if (false == isQueryConsistentRow(me.getOutputTuple(i), me.getMonomial(i), query)) 
			{
				return false;
			}
		}
		
		return true;
	}
	
	
	/*************************************************************************************************************/
	/** Title: isQueryConsistentRow																				
	/** Description: Check if the query is consistent with the a single row of the example 				 
	/*************************************************************************************************************/

	private static boolean isQueryConsistentRow(Atom head, Monomial body, Rule query)
	{
		List<Atom> monomLst = new ArrayList<Atom>();
		for (Atom atm : body.getTuples()) 
		{
			atm.isFullyInst();
			monomLst.add(atm);
		}
		
		query.SwapToInstAtomAndPartiallyInst(query.getHead(), head); // Inst. the head of the query with the output tuple
		
		List<Atom> instAtoms = new ArrayList<Atom>();// so that the body could contain identical tuples
		Set<Atom> bodyAtoms = new HashSet<Atom>();
		removeFullyInst(monomLst, query, instAtoms);
		
		return fitsProv(monomLst, query, instAtoms, bodyAtoms);
	}
	
	
	/*************************************************************************************************************/
	/** Title: fitsProv																				
	/** Description: isQueryConsistentRow() helper: remove fully inst. atoms from the query 				 
	/*************************************************************************************************************/

	private static void removeFullyInst(List<Atom> monomLst, Rule query, List<Atom> instAtoms)
	{
		for (Atom atm : query.getBody().getAtoms()) 
		{
			if (true == atm.isFullyInst())//remove fully inst. atoms 
			{
				monomLst.remove(atm);
				instAtoms.add(atm);
			}
		}
		query.getBody().getAtoms().removeAll(instAtoms);
		instAtoms.clear();
	}
	
	
	/*************************************************************************************************************/
	/** Title: fitsProv																				
	/** Description: isQueryConsistentRow() helper: check if the query is consistent the provenance of a monomial 				 
	/*************************************************************************************************************/

	private static boolean fitsProv(List<Atom> monomLst, Rule query, List<Atom> instAtoms, Set<Atom> bodyAtoms)
	{
		boolean atLeastOneMatch;
		for (Atom atm : query.getBody().getAtoms()) //make sure the provenance in the row fits the query
		{
			atLeastOneMatch = false;
			for (Atom monomAtom : monomLst)
			{
				if (atm.FittsPartialInst(monomAtom) && !monomAtom.isTaken()) 
				{
					atLeastOneMatch = true;
					instAtoms.add(monomAtom);
					bodyAtoms.add(atm);
					monomAtom.setTaken(true);
					query.SwapToInstAtomAndPartiallyInst(atm, monomAtom);
					break; // only one atom to fit the tuple
				}
			}
			
			if (false == atLeastOneMatch)//didn't find any tuple to match an atom from the body 
				return false;
		}
		
		monomLst.removeAll(instAtoms);
		
		return monomLst.isEmpty() && (instAtoms.size() == bodyAtoms.size());
	}
	
	
	/*************************************************************************************************************/
	/** Title: setMonomUntaken																				
	/** Description: sets all the tuples in the monom as untaken 				 
	/*************************************************************************************************************/

	private static void setMonomUntaken(Monomial m)
	{
		for (Atom t : m.getTuples()) 
		{
			t.setTaken(false);
		}
	}
}
