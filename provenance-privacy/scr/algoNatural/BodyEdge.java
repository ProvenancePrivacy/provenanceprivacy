package algoNatural;

import java.util.ArrayList;
import java.util.List;

import BasicsQBP.Atom;
import BasicsQBP.Proton;

public class BodyEdge extends Edge
{

	private List<List<Proton>> attrCovered;
	
	/*************************************************************************************************************/
	/** Title: Edge																				
	/** Description: Constructor for two atoms from the monomials				 
	/*************************************************************************************************************/

	public BodyEdge (Atom from, Atom to, List<List<Proton>> headAttr)
	{
		super(from, to);
		this.attrCovered = new ArrayList<List<Proton>>();
		updateAttr(headAttr);
	}

	

	public List<List<Proton>> getAttrCovered() {
		return attrCovered;
	}


	public void setAttrCovered(List<List<Proton>> attrCovered) {
		this.attrCovered = attrCovered;
	}
	
	
	/*************************************************************************************************************/
	/** Title: updateAttr																				
	/** Description: Updates the attrCovered field by finding the head attr. covered by the column of atoms				 
	/*************************************************************************************************************/

	private void updateAttr (List<List<Proton>> headAttr)
	{
		List<List<Proton>> lst = new ArrayList<List<Proton>>();
		getAttr(lst);
		
		//Compare with the list of params for the head
		for (List<Proton> list : headAttr) //for every head vector
		{
			if (true == lst.contains(list)) 
			{
				this.attrCovered.add(list);
			}
		}
	}
}
