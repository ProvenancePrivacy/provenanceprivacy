package algoNatural;

import java.util.ArrayList;
import java.util.List;

import algoMinimal.Unification;
import BasicsQBP.Atom;
import BasicsQBP.Proton;

public class Edge 
{
	private Atom from;
	
	private Atom to;
	
	
	/*************************************************************************************************************/
	/** Title: Edge																				
	/** Description: Generic constructor				 
	/*************************************************************************************************************/

	public Edge (Atom from, Atom to)
	{
		this.to = to;
		this.from = from;
	}	
	

	public Atom getFrom() {
		return from;
	}


	public void setFrom(Atom from) {
		this.from = from;
	}


	public Atom getTo() {
		return to;
	}


	public void setTo(Atom to) {
		this.to = to;
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: getAttr																				
	/** Description: find the attr. vectors of the column of atoms				 
	/*************************************************************************************************************/

	protected void getAttr (List<List<Proton>> lst)
	{
		//Create the list of parameters of the first atom
		for (Proton prt : this.from.getParams()) 
		{
			List<Proton> params = new ArrayList<Proton>();
			params.add( prt );
			lst.add( params );
		}
		
		Unification.unifyTwoRows(lst, this.to.getParams());
	}


	@Override
	public String toString() {
		return "(" + from + ", " + to + ")";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edge other = (Edge) obj;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		return true;
	}
	
	
	
}
