package algoNatural;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import algoMinimal.Unification;
import BasicsQBP.Atom;
import BasicsQBP.Proton;
import BasicsQBP.Rule;
import BasicsQBP.Var;
import PEI.Monomial;
import PEI.MonomialExample;

public class MostGeneralQuery 
{
	/*************************************************************************************************************/
	/** Title: genMGQueries																				
	/** Description: Given a partial setting, generates the most general query that fits this setting 				 
	/*************************************************************************************************************/

	public static List<Rule> genMGQueries(List<BodyEdge> pm, MonomialExample monomEx)
	{
		//Set the full expansion of the first two rows
		MonomialExample monomExTwoRows = new MonomialExample();
		monomExTwoRows.addExample(monomEx.getOutputTuple(0), monomEx.getMonomial(0));
		monomExTwoRows.addExample(monomEx.getOutputTuple(1), monomEx.getMonomial(1));
		
		MonomialExample finalMonom = new MonomialExample();
		ConvertMatchingToSetting(pm, monomExTwoRows, finalMonom);
		
		return genQueriesFromMatching(finalMonom);
	}
	
	
	/*************************************************************************************************************/
	/** Title: ConvertMatchingToSetting																				
	/** Description: convert partial matching to a partial setting 				 
	/*************************************************************************************************************/

	private static void ConvertMatchingToSetting(List<BodyEdge> pm, MonomialExample monomEx, MonomialExample finalMonom)
	{
		//Initialize example
		finalMonom.addExample(monomEx.getOutputTuple(0), new Monomial());
		finalMonom.addExample(monomEx.getOutputTuple(1), new Monomial());
		
		for (BodyEdge bodyEdge : pm) 
		{
			finalMonom.getMonomial(0).addTuple(bodyEdge.getFrom());
			finalMonom.getMonomial(1).addTuple(bodyEdge.getTo());
		}
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: genMapOfIndices																				
	/** Description: makes a map of the indices where each proton of the head appears in the body 				 
	/*************************************************************************************************************/	
	
	private static void genMapOfIndices(Rule miniQuery, Map<Proton, List<int []>> maps)
	{
		//get the indices of each proton of the head
		for (Proton p : miniQuery.getHead().getParams()) 
		{
			for (int i = 0; i < miniQuery.getBody().getAtoms().size(); i++) 
			{
				//Check if the atom at idx i contains the same proton as the proton in the head
				if (true == miniQuery.getBody().getAtoms().get(i).getParams().contains(p)) 
				{
					if (false == maps.containsKey(p)) //if list uninitialized
						maps.put(p, new ArrayList<int []>());
					
					int [] arr = new int [] {i, miniQuery.getBody().getAtoms().get(i).getParams().indexOf(p)}; // arr = [atomIdx, protonIdx]
					maps.get(p).add(arr); // maps = {p1 : [[1,2], [3,4]], p2 : [[5,6], [7,8]]...}
				}
			}
		}
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: generatePermutations																				
	/** Description: Gen. all perms of a most general query 				 
	/*************************************************************************************************************/

	private static void generatePermutations(List<List<int []>> Lists, List<List<int []>> result, int depth, List<int []> current)
	{
	    if(depth == Lists.size())
	    {
	       result.add(current);
	       return;
	     }

	    for(int i = 0; i < Lists.get(depth).size(); ++i)
	    {
	    	List<int []> arr = new ArrayList<int []>(current);
	    	arr.add(Lists.get(depth).get(i));
	        generatePermutations(Lists, result, depth + 1, arr);
	    }
	}
	
	
	/*************************************************************************************************************/
	/** Title: genQueriesFromMatching																				
	/** Description: Convert partial matching to a partial setting 				 
	/*************************************************************************************************************/

	private static List<Rule> genQueriesFromMatching(MonomialExample finalMonom)
	{
		List<Rule> retVal = new ArrayList<Rule>();
		Rule miniQuery = Unification.findMinimalQueryForSetting(finalMonom);
		Map<Proton, List<int []>> maps = new HashMap<Proton, List<int []>>();
		
		genMapOfIndices(miniQuery, maps);
		
		//add the minimal query just to check (not part of the algorithm)
		retVal.add(miniQuery);
		
		//generate all variations of a most general query
		List<List<int []>> perms = new ArrayList<List<int []>>();
		List<List<int []>> vals = new ArrayList<List<int []>>(maps.values());
		generatePermutations(vals, perms, 0, new ArrayList<int []>());//perms = [ (first variation)[ [1,2], [3,4] ], (second variation)[ [4,5], [6,7] ] ]
		
		//make an unsafe most general query
		Rule q = makeUnsafeGeneralQuery(miniQuery);
		
		int numOfUniqueHeadVars = perms.get(0).size();//number of unique head attributes
		for (List<int []> variation : perms) 
		{
			Rule queryCopy = new Rule(q);
			for (int i = 0; i < numOfUniqueHeadVars; i++)
			{
				int [] indices = variation.get(i);
				queryCopy.getBody().getAtoms().get(indices[0]).getParams().set(indices[1], q.getHead().getParams().get(i));
			}
			
			retVal.add(queryCopy);
		}
		
		return retVal;
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: makeUnsafeGeneralQuery																				
	/** Description: makes the most general query, where every variable in the body appears exactly once and no head 
	/** variable appears in the body
	/*************************************************************************************************************/

	private static Rule makeUnsafeGeneralQuery (Rule miniQuery)
	{
		Rule q = new Rule (miniQuery);
		char varName = 'o';
		for (Atom atm : q.getBody().getAtoms()) 
		{
			for (int i = 0; i < atm.getParams().size(); i++) 
			{
				atm.getParams().set(i, new Var(String.valueOf(varName)));
				
				varName = (char) (varName+1);//need to make sure var name is unique! currently not the case
				if (varName > 122)
					varName = 'A';
			}
		}
		
		//need to make sure var name is unique! currently not the case
		for (int i = 0; i < q.getHead().getParams().size(); i++) 
		{
			q.getHead().getParams().set(i, new Var(String.valueOf(varName)));
			
			varName = (char) (varName+1);
			if (varName > 122)
				varName = 'A';
		}
		
		return q;
	}
}
