package algoNatural;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import BasicsQBP.Atom;
import BasicsQBP.Proton;
import BasicsQBP.Rule;
import BasicsQBP.Var;
import PEI.MonomialExample;

public class CompareVarsInQuery 
{
	/*************************************************************************************************************/
	/** Title: getMinimalQuery																				
	/** Description: compare vars in a query and check at each stage if it is still consistent with the example 	 
	/*************************************************************************************************************/
	
	public static Rule getMinimalQuery (MonomialExample ex, Rule query)
	{
		returnToOriginalName(ex);
		List<Set<Proton>> varSubsets = new ArrayList<Set<Proton>>();
		List<Proton> allVars = new ArrayList<Proton>();
		
		collectVars(query, allVars);// collect all vars in a list
		generatePairsVars(allVars, varSubsets);// generate sets of pairs of vars
		
		
		//return the query added last, i.e. has the largest set of equal variables
		return tryPossibilities(varSubsets, allVars, ex, query);
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: tryPossibilities																				
	/** Description: try all possibilities to equate as many vars as possible using a greedy method 	 
	/*************************************************************************************************************/
	
	private static Rule tryPossibilities (List<Set<Proton>> varSubsets, List<Proton> allVars, MonomialExample ex, Rule query)
	{
		Proton p;
		List<Proton> ignoreList = new ArrayList<Proton>();

		for (Set<Proton> list : varSubsets) 
		{
			if (false == listContainsIgnoreProton(list, ignoreList)) 
			{
				Rule copy = new Rule (query);
				p = equateVars(copy, list);
				if (true == ConsistentCheck.isQueryConsistent(ex, copy, 0)) 
				{
					updateIgnoreLst(p, list, ignoreList);
					query = copy;
				}
			}
		}
		
		return query;
	}
	
		
	/*************************************************************************************************************/
	/** Title: listContainsIgnoreProton 																				
	/** Description: the set contains a proton already given a different name, so no point in equating to it 	 
	/*************************************************************************************************************/

	private static boolean listContainsIgnoreProton (Set<Proton> lst, List<Proton> ignoreList)
	{
		Set<Proton> temp = new HashSet<>(lst);
		temp.retainAll(ignoreList);
		return temp.size() > 0;
		/*for (Proton proton : lst) 
		{
			if (ignoreList.contains(proton))
				return true;
		}
		
		return false;*/
	}
		
	
	/*************************************************************************************************************/
	/** Title: updateIgnoreLst 																				
	/** Description: add a new proton to ignore list  	 
	/*************************************************************************************************************/
	
	private static void updateIgnoreLst (Proton p, Set<Proton> lst, List<Proton> ignoreList)
	{
		for (Proton pr : lst) 
		{
			if (!pr.equals(p))
				ignoreList.add(pr);
		}
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: equateVars																				
	/** Description: turn all vars in lst to one var in the body of query 	 
	/*************************************************************************************************************/
	
	private static Proton equateVars (Rule query, Set<Proton> lst)
	{
		Proton p = lst.iterator().next();
		String varName = p.getName();
		//in body
		for (Atom a : query.getBody().getAtoms()) 
		{
			for (int i = 0; i < a.getParams().size(); i++)
			{
				if (lst.contains( a.getParams().get(i) )) 
				{
					a.getParams().set(i, new Var(varName));
				}
			}
		}
		
		//in head
		for (int i = 0; i < query.getHead().getParams().size(); i++)
		{
			if (lst.contains( query.getHead().getParams().get(i) )) 
			{
				query.getHead().getParams().set(i, new Var(varName));
			}
		}
		
		return p;
	}
	
	
	/*************************************************************************************************************/
	/** Title: collectVars																				
	/** Description: generate a list of all variables of the query 	 
	/*************************************************************************************************************/
	
	private static void collectVars (Rule query, List<Proton> allVars)
	{
		//collect all vars of the body of the rule
		for (Atom a : query.getBody().getAtoms()) 
		{
			allVars.addAll( a.getParams() );
		}
		
		allVars.addAll( query.getHead().getParams() );
	}
	
	
	/*************************************************************************************************************/
	/** Title: generatePairsVars																				
	/** Description: generate a list of all variables of the query 	 
	/*************************************************************************************************************/
	
	private static void generatePairsVars (List<Proton> allVars, List<Set<Proton>> varSubsets)
	{
		//collect all vars of the body of the rule
		for (Proton p1 : allVars) 
		{
			for (Proton p2 : allVars) 
			{
				if (false == p1.equals(p2)) 
				{
					Set<Proton> pair = new HashSet<Proton>();
					pair.add(p1); 
					pair.add(p2);
					if (false == varSubsets.contains(pair)) 
					{
						varSubsets.add( pair );
					}
				}
			}
		}
	}
	
	
	/*************************************************************************************************************/
	/** Title: returnToOriginalName																				
	/** Description: returns the names of the atoms into their original name 	 
	/*************************************************************************************************************/
	
	private static void returnToOriginalName (MonomialExample me)
	{
		for (int i = 0; i < 2; i++) 
		{
			for (Atom tuple : me.getMonomial(i).getTuples()) 
			{
				tuple.setName( tuple.getName().split("\\_")[0] );
			}
		}
	}
	
	
	/*************************************************************************************************************/
	/** Title: getSubsets																				
	/** Description: Generate all subsets all subsets of variables of the query of size k containing needed protons 			 
	/*************************************************************************************************************/
	
	private static List<Set<Proton>> getSubsets(List<Proton> superSet, int k, Set<Proton> needed) 
	{
	    List<Set<Proton>> res = new ArrayList<>();
	    getSubsetsHelper(superSet, k, 0, new HashSet<Proton>(), res, needed);
	    return res;
	}
	
	
	/*************************************************************************************************************/
	/** Title: getSubsetsHelper																				
	/** Description: The recursive method for getSubsets 				 
	/*************************************************************************************************************/
	
	private static void getSubsetsHelper(List<Proton> superSet, int k, int idx, Set<Proton> current, List<Set<Proton>> solution, Set<Proton> needed) 
	{
	    //successful stop clause
		//added the second part of the condition to check if the current list is contained in the subset found
	    if (current.size() == k) 
	    {
	    	if (current.containsAll(needed)) 
	    	{
	    		solution.add(new HashSet<>(current));
			}
	    	
	        return;
	    }
	    
	    //unseccessful stop clause
	    if (idx == superSet.size()) 
	    	return;
	    
	    Proton x = superSet.get(idx);
	    current.add(x);
	    
	    //"guess" x is in the subset
	    getSubsetsHelper(superSet, k, idx+1, current, solution, needed);
	    current.remove(x);
	    
	    //"guess" x is not in the subset
	    getSubsetsHelper(superSet, k, idx+1, current, solution, needed);
	}
}
