package algoNatural;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import convertEgToMonomialEg.Converter;
import BasicsQBP.Rule;
import PEI.Example;
import PEI.MonomialExample;

public class FindConsQueryNatural 
{
	/*************************************************************************************************************/
	/** Title: findQuery																				
	/** Description: input an example with N(X) provenance and output a consistent query 	 
	/*************************************************************************************************************/
	
	public static Rule findQuery (Example ex) throws Exception
	{
		MonomialExample monomEx = Converter.convertPolynomial(ex);
		return findMostSpecificQuery(monomEx);
	}
	
	
	/*************************************************************************************************************/
	/** Title: findMostSpecificQuery																				
	/** Description: uses a heuristic approach to find the most specific query out of all consistent queries found  	 
	/*************************************************************************************************************/
	
	public static Rule findMostSpecificQuery (MonomialExample monomEx) throws Exception
	{
		List<Rule> queries = findQueries(monomEx);
		List<Rule> minQueries = new ArrayList<Rule>();
		for (Rule rule : queries) 
		{
			minQueries.add( CompareVarsInQuery.getMinimalQuery(monomEx, rule) );
		}
		
		Rule retVal = null;
		int curWeight, weight = -100;
		for (Rule query : minQueries) 
		{
			curWeight = query.numOfConsts() - query.numOfUniqueVars();
			if (weight < curWeight) 
			{
				retVal = query;
				weight = curWeight;
			}
		}
		
		return retVal;
	}
	
	
	/*************************************************************************************************************/
	/** Title: findQueries																				
	/** Description: input an example with N(X) provenance and output all consistent queries 	 
	/*************************************************************************************************************/
	
	public static List<Rule> findQueries (MonomialExample monomEx) throws Exception
	{
		List<Rule> retVal = new ArrayList<Rule>();
		
		//Find matchings of the first two rows
		Set<List<BodyEdge>> matchings = PartialMatching.match(monomEx);
		
		//Generate all most general queries
		List<Rule> mostGenQueries = new ArrayList<Rule>();
		for (List<BodyEdge> matching : matchings) 
		{
			mostGenQueries.addAll( MostGeneralQuery.genMGQueries(matching, monomEx) );
		}
		
		//Check for consistent queries
		for (Rule rule : mostGenQueries) 
		{
			if (true == ConsistentCheck.isQueryConsistent(monomEx, rule, 2)) 
			{
				retVal.add(rule);
			}
		}
		
		return retVal;
	}
}
