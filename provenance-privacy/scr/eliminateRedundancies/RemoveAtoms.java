package eliminateRedundancies;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import BasicsQBP.Atom;
import BasicsQBP.Proton;
import BasicsQBP.Rule;
import BasicsQBP.Var;

public class RemoveAtoms 
{
	/*************************************************************************************************************/
	/** Title: clean																				
	/** Description: removes atoms whose variables appear only in them 	 
	/*************************************************************************************************************/
	
	public static void clean (Rule query)
	{
		
		//gen list of all vars in the rule
		List<Proton> vars = new ArrayList<Proton>();
		for (Atom atm : query.getBody().getAtoms()) 
		{
			for (Proton proton : atm.getParams()) 
			{
				if (proton instanceof Var) 
					vars.add(proton);
			}
			
		}
		
		//for each atom, check how many vars it has that appear only once in the query
		List<Atom> remove = new ArrayList<Atom>();
		int oneTimeVars;
		for (Atom atm : query.getBody().getAtoms()) 
		{
			oneTimeVars = 0;
			for (Proton proton : atm.getParams()) 
			{
				if (proton instanceof Var && 1 == Collections.frequency(vars, proton)) 
				{
					oneTimeVars++;
				}
			}
			
			if (atm.getParams().size() == oneTimeVars)//if all vars are appear only in this atom 
				remove.add(atm);
		}
		
		query.getBody().getAtoms().removeAll(remove);
	}
	
}
