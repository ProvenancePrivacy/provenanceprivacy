package algoTrio;

import heuristicTools.CoveredAttributesTools;
import BasicsQBP.Atom;
import PEI.Monomial;
import PEI.MonomialExample;

public class FullExpansion 
{
	
	/*************************************************************************************************************/
	/** Title: expand																				
	/** Description: Takes a MonomialExample and makes a MonomialExample which is the full expansion of the original 	 
	/*************************************************************************************************************/
	
	public static MonomialExample expand (MonomialExample ex)
	{
		MonomialExample retVal = new MonomialExample ();
		int monomLength = CoveredAttributesTools.aggregateMonomialsSize(ex); // Size of the monomials after the expansion
		int power = monomLength;
		
		for (int i = 0; i < ex.getTuples().size(); i++) 
		{
			power = (int) ((double) power / ex.getMonomial(i).getTuples().size() );
			retVal.addExample( ex.getOutputTuple(i), expandMonomial(power, monomLength, ex.getMonomial(i)) );
		}
		
		return retVal;
	}
	
	
	/*************************************************************************************************************/
	/** Title: expandMonomial																				
	/** Description: expands a single monomial (orig) to be in size endSize according to the power param 	 
	/*************************************************************************************************************/
	
	private static Monomial expandMonomial (int power, int endSize, Monomial orig)
	{
		Monomial retVal = new Monomial ();
		while (endSize > 0)
		{
			for (Atom tuple : orig.getTuples()) 
			{
				expandMonomialHelper(power, tuple, retVal);
				endSize -= power;
			}
		}
		
		return retVal;
	}
	
	
	/*************************************************************************************************************/
	/** Title: expandMonomialHelper																				
	/** Description: raises a single variable to the power  	 
	/*************************************************************************************************************/
	
	private static void expandMonomialHelper (int power, Atom atm, Monomial monom)
	{
		for (int i = 0; i < power; i++) 
		{
			monom.addTuple(new Atom (atm));
		}
	}
}
