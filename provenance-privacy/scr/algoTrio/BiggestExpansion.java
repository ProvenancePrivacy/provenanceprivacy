package algoTrio;

import heuristicTools.AtomArraySum;
import heuristicTools.CoveredAttributesTools;

import java.util.ArrayList;
import java.util.List;

import BasicsQBP.Atom;
import PEI.Monomial;
import PEI.MonomialExample;

public class BiggestExpansion 
{
	/*************************************************************************************************************/
	/** Title: expandByBiggestMonomial																				
	/** Description: expand according to the biggest monom. 
	/** ASSUMPTIOM: NO NEED TO EXPAND THE BIGGEST MONOMIAL ITSELF
	/*************************************************************************************************************/	
	
	public static void expandByBiggestMonomial (MonomialExample monomEx)
	{
		List<Integer> remainingAttr = new ArrayList<Integer>();
		fillInRange(remainingAttr, monomEx.getOutputTuple(0).getParams().size());
		List<List<AtomArraySum>> maps = new ArrayList<List<AtomArraySum>>();
		for (int i = 0; i < monomEx.rowNumber(); i++) 
		{
			List<AtomArraySum> attr = new ArrayList<AtomArraySum>();
			maps.add(attr);
			CoveredAttributesTools.coveredAttributesPerRow(i, monomEx, attr);
		}
		List<AtomArraySum> biggest = findBiggest(maps);
		
		for (int i = 0; i < monomEx.rowNumber(); i++) 
		{
			if (biggest != maps.get(i))
				monomEx.getMonoms().set(i, expand(biggest, maps.get(i), remainingAttr) );
		}
		
		assert (true == sanityCheck(monomEx, remainingAttr));
	}
	
	
	/*************************************************************************************************************/
	/** Title: findBiggest																				
	/** Description: finds the biggest monomial on the example table				 
	/*************************************************************************************************************/	
	
	private static List<AtomArraySum> findBiggest (List<List<AtomArraySum>> maps)
	{
		int maxSize = 0;
		List<AtomArraySum> retVal = null;
		for (List<AtomArraySum> monom : maps) 
		{
			if (monom.size() > maxSize)
			{
				retVal = monom;
				maxSize = monom.size();
			}
		}
		
		return retVal;
	}
	
	
	/*************************************************************************************************************/
	/** Title: expand																				
	/** Description: creates an expanded monomial according to the heuristics				 
	/*************************************************************************************************************/	
	
	private static Monomial expand (List<AtomArraySum> biggest, List<AtomArraySum> cur, List<Integer> remainingAttr)
	{
		Monomial retVal = new Monomial();
		AtomArraySum candid;
		List<AtomArraySum> candidates = new ArrayList<AtomArraySum>();
		
		for (AtomArraySum atom : biggest) 
		{
			if (atom.isCoveringSomething())
			{
				candid = findCandidate(cur, atom, remainingAttr);
				if (candid != null) candidates.add( candid );
			}
		}
		
		for (AtomArraySum atom : candidates) 
		{
			for (int i = 0; i < atom.getExponent(); i++) 
			{
				retVal.addTuple( new Atom (atom.getTuple()) );
			}
		}
		
		for (AtomArraySum atomArraySum : cur) 
		{
			if (false == retVal.getTuples().contains(atomArraySum.getTuple())) 
			{
				retVal.addTuple(atomArraySum.getTuple());
			}
		}
		
		return retVal;
	}
	
	/*************************************************************************************************************/
	/** Title: findCandidate																				
	/** Description: find a tuple to expand according to the attributes covered  				 
	/*************************************************************************************************************/	
	
	/*private static AtomArraySum findCandidate (List<AtomArraySum> cur, AtomArraySum tuple, List<Integer> remainingAttr)
	{
		//didn't find any candidates to cover the attribute - all attributes are covered
		if (remainingAttr.isEmpty()) 
		{
			for (AtomArraySum candid : cur) 
			{
				if (isCoverSameAttributeSameIndex(candid.getAttrCovered(), tuple.getAttrCovered())) 
				{
					if (candid.isInTheCover() == false)
					{
						candid.setInTheCover(true);
						return candid;
					}
					else
						candid.setExponent(candid.getExponent() + 1);
				}
			}
		}

		for (AtomArraySum candid : cur) 
		{
			if (candid.isInTheCover() == false && 
					isCoverSameAttributeSameIndex(candid.getAttrCovered(), tuple.getAttrCovered(), remainingAttr)) 
			{
				candid.setInTheCover(true);
				return candid;
			}
		}
		
		//didn't find any fresh candidates to cover the attribute - need to duplicate an existing tuple in the cover
		for (AtomArraySum candid : cur) 
		{
			if (isCoverSameAttributeSameIndex(candid.getAttrCovered(), tuple.getAttrCovered(), remainingAttr)) 
			{
				candid.setExponent(candid.getExponent() + 1);
			}
		}
		
		//didn't find any fresh candidates to cover the attribute - need to duplicate an existing tuple without expecting anything
		for (AtomArraySum candid : cur) 
		{
			if (isCoverSameAttributeSameIndex(candid.getAttrCovered(), tuple.getAttrCovered(), remainingAttr)) 
			{
				candid.setExponent(candid.getExponent() + 1);
			}
		}
		
		return null;
	}*/
	
	
	private static AtomArraySum findCandidate (List<AtomArraySum> cur, AtomArraySum tuple, List<Integer> remainingAttr)
	{
		//didn't find any candidates to cover the attribute - all attributes are covered
		if (!remainingAttr.isEmpty()) 
		{
			for (AtomArraySum candid : cur) 
			{
				if (candid.isInTheCover() == false && 
						isCoverSameAttributeSameIndex(candid.getAttrCovered(), tuple.getAttrCovered(), remainingAttr)) 
				{
					candid.setInTheCover(true);
					return candid;
				}
			}
			
			//didn't find any fresh candidates to cover the attribute - need to duplicate an existing tuple in the cover
			for (AtomArraySum candid : cur) 
			{
				if (isCoverSameAttributeSameIndex(candid.getAttrCovered(), tuple.getAttrCovered(), remainingAttr)) 
				{
					candid.setExponent(candid.getExponent() + 1);
				}
			}
		}

		else
		{
			for (AtomArraySum candid : cur) 
			{
				if (isCoverSameAttributeSameIndex(candid.getAttrCovered(), tuple.getAttrCovered())) 
				{
					if (candid.isInTheCover() == false)
					{
						candid.setInTheCover(true);
						return candid;
					}
					else
					{
						candid.setExponent(candid.getExponent() + 1);
						return null;
					}
				}
			}
			
			//didn't find any fresh candidates to cover the attribute - need to duplicate an existing tuple without expecting anything
			cur.get(0).setExponent(cur.get(0).getExponent() + 1);
		}
		
		return null;
	}
	
	/*************************************************************************************************************/
	/** Title: checkCoverSameAttributeSameIndex																				
	/** Description: check if two tuples have a common attribute that they cover 				 
	/*************************************************************************************************************/	
	
	private static boolean isCoverSameAttributeSameIndex (int [] arr1, int [] arr2, List<Integer> remainingAttr)
	{
		for (int i : remainingAttr) 
		{
			if (arr1[i] != -1 && arr1[i] == arr2[i])
			{
				remainingAttr.remove(new Integer(i));
				return true;
			}
		}
		
		return false;
	}
	
	
	/*************************************************************************************************************/
	/** Title: checkCoverSameAttributeSameIndex																				
	/** Description: check if two tuples have a common attribute that they cover. No dependency on output attr. 				 
	/*************************************************************************************************************/	
	
	private static boolean isCoverSameAttributeSameIndex (int [] arr1, int [] arr2)
	{
		for (int i = 0; i < arr2.length; i++)
		{
			if (arr1[i] != -1 && arr1[i] == arr2[i])
			{
				return true;
			}
		}
		
		return false;
	}
	
	
	/*************************************************************************************************************/
	/** Title: fillInRange																				
	/** Description: fills a List<Integer> with the numbers in range [0,max) 				 
	/*************************************************************************************************************/	
	
	private static void fillInRange(List<Integer> list, int max) 
	{
        for (int i = 0; i < max; i++)
            list.add(i);
    }
	
	
	/*************************************************************************************************************/
	/** Title: sanityCheck																				
	/** Description:
	/*************************************************************************************************************/	
	
	private static boolean sanityCheck (MonomialExample ex, List<Integer> remainingAttr)
	{
		//check all monomials of the same size
		int size = ex.getMonomial(0).size();
		for (Monomial monomial : ex.getMonoms()) 
		{
			if (size != monomial.size()) 
			{
				System.out.println("BiggestExpansion::sanityCheck: monomial " + monomial + " is not of size " + size + "\n");
				return false;
			}
		}
		
		if (false == remainingAttr.isEmpty()) 
		{
			System.out.println("BiggestExpansion::sanityCheck: Attributes " + remainingAttr + " are not covered\n");
			return false;
		}
		
		return true;
	}
}
