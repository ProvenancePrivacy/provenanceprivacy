package BasicsQBP;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class Body 
{
	
	List<Atom> atoms = new ArrayList<Atom>();
	
	public Body () {}
	
	
	public Body (List<Atom> iatoms) 
	{
		for (Atom atom : iatoms) 
		{
			this.atoms.add( new Atom(atom) );
		}
	}
	
	
	public Body (Atom ... iatoms) 
	{
		for (Atom atom : iatoms) 
		{
			this.atoms.add(atom);
		}
	}
	

	/*************************************************************************************************************/
	/** Title: Body																				
	/** Description: Copy constructor				
	/*************************************************************************************************************/
	
	public Body (Body other)
	{
		for (Atom atom : other.getAtoms()) 
		{
			this.atoms.add(atom);
		}
	}


	public List<Atom> getAtoms() 
	{
		return atoms;
	}


	public void setAtoms(List<Atom> atoms) 
	{
		this.atoms = atoms;
	}
	
	
	public void addAtom(Atom atom) 
	{
		this.atoms.add( atom );
	}

	
	
	/*************************************************************************************************************/
	/** Title: LegalDerivation																	
	/** Description: Checks if TopK of each body atom is Updated  						
	/*************************************************************************************************************/
	
	public boolean LegalDerivation()
	{
		boolean retVal = true;
		for (Atom atom : atoms) 
		{
			if (null == atom) 
			{
				retVal = false;
				break;
			}
		}
		
		return retVal;
	}
	
	
	
	
	/*************************************************************************************************************/
	/** Title: HasNonFact																	
	/** Description: Checks if body Has Atom that is not fact  						
	/*************************************************************************************************************/
	
	public boolean HasNonFact ()
	{
		boolean retVal = false;
		for (Atom atom : this.atoms) 
		{
			if (false == atom.isTaken()) 
			{
				retVal = true;
				break;
			}
		}
		
		return retVal;
	}
	
	
	/*************************************************************************************************************/
	/** Title: IdenticalAtoms																	
	/** Description: Returns true iff this has the same atoms as other  						
	/*************************************************************************************************************/
	
	public boolean IdenticalAtoms (Body other)
	{
		boolean retVal = ( this.atoms.size() == other.getAtoms().size() );
		if (true == retVal) 
		{
			for (int i = 0; i < this.atoms.size(); i++) 
			{
				if (false == this.atoms.get(i).equals(other.getAtoms().get(i)))
				{
					retVal = false;
					break;
				}
			}
		}
		
		return retVal;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((atoms == null) ? 0 : atoms.hashCode());
		/*result = prime * result + derivedInlevel;
		result = prime * result
				+ ((ruleUsed == null) ? 0 : ruleUsed.hashCode());*/
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Body other = (Body) obj;
		if (atoms == null) {
			if (other.atoms != null)
				return false;
		} else if (!atoms.equals(other.atoms))
			return false;
		/*if (derivedInlevel != other.derivedInlevel)
			return false;*/
		/*if (ruleUsed == null) {
			if (other.ruleUsed != null)
				return false;
		} else if (!ruleUsed.equals(other.ruleUsed))
			return false;*/
		return true;
	}

	
	public String toString()
	{
		return this.atoms.toString();
	}

}
