package ProvenancePrivacy;

import BasicsQBP.Atom;
import BasicsQBP.Constant;
import BasicsQBP.Proton;
import BasicsQBP.Var;
import ProvenancePrivacy.AbstractionTree.TreeNode;
import BasicsQBP.Rule;
import PEI.MonomialExample;
import algoNatural.CompareVarsInQuery;
import algoNatural.FindConsQueryNatural;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

public class PrivacyCount
{
	public static HashMap<Rule, Boolean> connectedCache = new HashMap<>();
	public static HashMap<MonomialExample, List<Rule>> consistentCache = new HashMap<>();
	
	/*************************************************************************************************************/
	/** Title: CalculatePrivacy																				
	/** Description: calculate the privacy of a given example
	/*************************************************************************************************************/	
    public static int CalculatePrivacy(String absExample, TreeNode<String> absTree) throws Exception
    {
        return GetPrivacyQueries(absExample, absTree).size();
    }

    public static int consistentCacheCount = 0;
    public static int connectedCacheCount = 0;
    
    /*************************************************************************************************************/
	/** Title: GetPrivacyQueries																				
	/** Description: get the inclusion minimal connected queries of a given example
	/*************************************************************************************************************/	
    public static List<Rule> GetPrivacyQueries(String absExample, TreeNode<String> absTree) throws Exception
    {
    	Abstraction.monExampleToStrExample.clear();
    	
    	List<Rule> goodQueries = new ArrayList<>();
    	HashSet<String> goodConcretizations = new HashSet<>();
    	
    	String[] rows = absExample.split("\n");
    	
    	goodConcretizations.add(rows[0] + "\n");
    	
    	for (int i = 1; i < rows.length; i++)
    	{
    		goodQueries.clear();
    		
    		// get concretization set
    		List<MonomialExample> monomialsExamples = new ArrayList<>();
    		for (String concretization : goodConcretizations)
    		{
    			String currentExample = concretization + rows[i] + "\n";
    			monomialsExamples.addAll(Abstraction.GetConcretizationSet(currentExample, absTree));
    		}
    		
            HashMap<Rule, List<MonomialExample>> queryToMonExample = new HashMap<>();
            for (MonomialExample me : monomialsExamples)
            {
            	List<Rule> out = new ArrayList<>();
            	
            	// get consistent queries
            	if (TestClass.ShouldUseConsistentCache && consistentCache.containsKey(me))
        		{
            		consistentCacheCount++;
            		// get consistent queries from cache
            		out = consistentCache.get(me);
        		}
            	else
            	{
            		out = FindConsQueryNatural.findQueries(me);
            		
            		if (TestClass.ShouldUseConsistentCache)
            		{
            			consistentCache.put(me, out);
            		}
            	}
            	
            	// save for each query which example created it
            	for (Rule q : out)
            	{
            		if (!queryToMonExample.containsKey(q))
            		{
            			queryToMonExample.put(q, new ArrayList<MonomialExample>());
            		}
            		
            		queryToMonExample.get(q).add(me);
            	}
            	
            	goodQueries.addAll(out);
            }
            if (goodQueries.size() < TestClass.PrivacyThreshold)
            {
            	return new ArrayList<Rule>();
            }
            
            // remove duplicates
            goodQueries = RemoveDuplicates(goodQueries);
            if (goodQueries.size() < TestClass.PrivacyThreshold)
            {
            	return new ArrayList<Rule>();
            }
            
            // remove not connected
            goodQueries = RemoveDisconnected(goodQueries);
            if (goodQueries.size() < TestClass.PrivacyThreshold)
            {
            	return new ArrayList<Rule>();
            }
            
            // save good concretizations for next round
            goodConcretizations.clear();
    		for (Rule q : goodQueries)
    		{
    			List<MonomialExample> temp = queryToMonExample.get(q);
    			for (MonomialExample monEx : temp)
    			{
    				String origConcretization = Abstraction.monExampleToStrExample.get(monEx);
    				goodConcretizations.add(origConcretization);
    			}
    		}
    		
    		// remove not minimal
    		goodQueries = RemoveNotMinimal(goodQueries);
    		if (goodQueries.size() < TestClass.PrivacyThreshold)
            {
            	return new ArrayList<Rule>();
            }
    		
    		if (TestClass.ShoudLogOutputCIMConcretizations)
    		{
	    		for (Rule q : goodQueries)
	    		{
	    			if (goodQueries.size() >= TestClass.PrivacyThreshold && TestClass.ExampleRows == i + 1)
					{
						Utils.LogRawText("######### query #########\n" + q);
					}
	    			MonomialExample first = queryToMonExample.get(q).get(0);
	    			if (goodQueries.size() >= TestClass.PrivacyThreshold && TestClass.ExampleRows == i + 1)
					{
						Utils.LogRawText("minimal query: " + CompareVarsInQuery.getMinimalQuery(first, q) + "\n");
						Utils.LogRawText("concretization:\n" + Abstraction.monExampleToStrExample.get(first));
					}
	    		}
    		}
    	}
        
        return goodQueries;
    }

    /*************************************************************************************************************/
	/** Title: RemoveNotMinimal																				
	/** Description: remove queries that are not minimal w.r.t. each others
	/*************************************************************************************************************/	
    private static List<Rule> RemoveNotMinimal(List<Rule> queries)
    {
        queries.sort(new Comparator<Rule>()
        {
            @Override
            public int compare(Rule o1, Rule o2)
            {
                return o2.numOfConsts() - o1.numOfConsts();
            }
        });

        int firstIndex = 0;
        while (firstIndex < queries.size() - 1)
        {
        	Rule firstQuery = queries.get(firstIndex);
        	List<Atom> firstAtoms = firstQuery.getBody().getAtoms();
        	
            int secondIndex = firstIndex + 1;
            while (secondIndex < queries.size())
            {
            	Rule secondQuery = queries.get(secondIndex);
                List<Atom> secondAtoms = secondQuery.getBody().getAtoms();
                
                if (firstAtoms.size() != secondAtoms.size())
                {
                    continue;
                }

                boolean isContained = true;
                for (int k = 0; k < firstAtoms.size(); k++)
                {
                    if (!firstAtoms.get(k).getName().equals(secondAtoms.get(k).getName()))
                    {
                        isContained = false;
                        break;
                    }

                    Vector<Proton> protons1 = firstAtoms.get(k).getParams();
                    Vector<Proton> protons2 = secondAtoms.get(k).getParams();
                    for (int l = 0; l < protons1.size(); l++)
                    {
                        Proton firstProton = protons1.get(l);
                        Proton secondProton = protons2.get(l);
                        if (!((firstProton instanceof Var && secondProton instanceof Var)
                                || (firstProton instanceof Constant && secondProton instanceof Constant
                                    && ((Constant)firstProton).equals((Constant)secondProton))
                                || (firstProton instanceof Constant && secondProton instanceof Var)))
                        {
                            isContained = false;
                            break;
                        }
                    }

                    if (!isContained)
                    {
                        break;
                    }
                }
                
                if (isContained)
                {
                    queries.remove(queries.remove(secondIndex));
                }
                else
                {
                    secondIndex++;
                }
            }
            firstIndex++;
        }

        return queries; // to do - implement this
    }
    
    /*************************************************************************************************************/
	/** Title: IsQueryConnected																				
	/** Description: check if a query is connected
	/*************************************************************************************************************/
    private static boolean IsQueryConnected(Rule query)
    {
    	if (TestClass.ShouldUseConnectedCache && connectedCache.containsKey(query))
    	{
    		connectedCacheCount++;
    		return connectedCache.get(query);
    	}
    	
    	List<Atom> connectedAtoms = new ArrayList<>();
        connectedAtoms.add(query.getHead());
        List<Var> allVars = new ArrayList<>();
        for (Proton p : query.getHead().getParams())
        {
            if (p instanceof Var)
            {
                allVars.add((Var)p);
            }
        }
        List<Atom> allAtoms = new ArrayList<>();
        allAtoms.addAll(query.getBody().getAtoms());
        boolean wasFound;
        do
        {
            wasFound = false;
            for (Atom current : allAtoms)
            {
                if (connectedAtoms.contains(current))
                {
                    continue;
                }
                boolean shouldAdd = false;
                for (Proton p : current.getParams())
                {
                    if (p instanceof Var && allVars.contains(p))
                    {
                        shouldAdd = true;
                        break;
                    }
                }
                if (shouldAdd)
                {
                    connectedAtoms.add(current);
                    for (Proton p : current.getParams())
                    {
                        if (p instanceof Var)
                        {
                            allVars.add((Var)p);
                        }
                    }
                    wasFound = true;
                }
            }
        } while (wasFound);
        
        boolean isConnected = connectedAtoms.size() == query.getBody().getAtoms().size() + 1;
        		
		if (TestClass.ShouldUseConnectedCache)
    	{
    		connectedCache.put(query, isConnected);
    	}

        return isConnected;
    }

    /*************************************************************************************************************/
	/** Title: RemoveDisconnected																				
	/** Description: remove queries that are not connected
	/*************************************************************************************************************/	
    private static List<Rule> RemoveDisconnected(List<Rule> queries)
    {
    	List<Rule> connectedQueries = new ArrayList<>();
        for (Rule query : queries)
        {
            if (IsQueryConnected(query))
            {
                connectedQueries.add(query);
            }
        }

        return connectedQueries;
    }

    /*************************************************************************************************************/
	/** Title: RemoveDuplicates																				
	/** Description: remove duplicates queries
	/*************************************************************************************************************/	
    private static List<Rule> RemoveDuplicates(List<Rule> queries)
    {
        return queries.stream().map(Rule::new).distinct().collect(Collectors.toList());
    }
}