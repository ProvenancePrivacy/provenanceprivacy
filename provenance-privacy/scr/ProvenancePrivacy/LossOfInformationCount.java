package ProvenancePrivacy;

import ProvenancePrivacy.AbstractionTree.TreeNode;

public class LossOfInformationCount
{
	public enum LOI_METHOD
	{
	  ARBITRARY_WEIGHTS,
	  LEAVES_COUNT,
	  ENTROPY,
	}
	
	/*************************************************************************************************************/
	/** Title: CalculateLossOfInfo																				
	/** Description: calculates the loss of information via the input method
	/*************************************************************************************************************/	
	public static double CalculateLossOfInfo(String origExample, String absExample, TreeNode<String> absTree,
			LOI_METHOD loiMethod) throws Exception
	{
		switch(loiMethod)
		{
		  case ARBITRARY_WEIGHTS:
			  return ArbitraryWeightsLossOfInfo(origExample, absExample, absTree);
		  case LEAVES_COUNT:
			  return LeavesCountLossOfInfo(absExample, absTree);
		  case ENTROPY:
			  return EntropyLossOfInfo(absExample, absTree);
		  default:
			  throw new Exception("LOI_METHOD not found");
		}
	}
	
	/*************************************************************************************************************/
	/** Title: ArbitraryWeightsLossOfInfo																				
	/** Description: calculates the loss of information via arbitrary weights method
	/*************************************************************************************************************/	
    private static double ArbitraryWeightsLossOfInfo(String origExample, String absExample, TreeNode<String> absTree)
    {
        long lossOfInformation = 1;

        String[] parts = absExample.split("!");
        for (int i = 1; i < parts.length - 1; i += 2)
        {
            String abstractedValue = parts[i];

            origExample = origExample.substring(parts[i-1].length());
            int endOfValue = Integer.min(origExample.indexOf("|"), origExample.indexOf("\n"));
            if (endOfValue == -1)
            {
                endOfValue = origExample.trim().length();
            }
            String originalValue = origExample.substring(0, endOfValue);
            origExample = origExample.substring(originalValue.length());

            TreeNode<String> originalNode = absTree.findTreeNode(originalValue);
            TreeNode<String> abstractedNode = absTree.findTreeNode(abstractedValue);

            long currentLossOfInformation = 0;
            while (!originalNode.data.equals(abstractedNode.data))
            {
                currentLossOfInformation += originalNode.goToParentWeight;
                originalNode = originalNode.parent;
            }

            if (currentLossOfInformation > 0)
            {
                lossOfInformation *= currentLossOfInformation;
            }
        }

        return lossOfInformation;
    }
    
    /*************************************************************************************************************/
	/** Title: LeavesCountLossOfInfo																				
	/** Description: calculates the loss of information via leaves count method
	/*************************************************************************************************************/	
    private static double LeavesCountLossOfInfo(String absExample, TreeNode<String> absTree)
    {
        return Abstraction.GetConcretizationSetSize(absExample, absTree);
    }
    
    /*************************************************************************************************************/
	/** Title: EntropyLossOfInfo																				
	/** Description: calculates the loss of information via entropy method
	/*************************************************************************************************************/	
    private static double EntropyLossOfInfo(String absExample, TreeNode<String> absTree)
    {
        return Math.log(Abstraction.GetConcretizationSetSize(absExample, absTree));
    }
}
