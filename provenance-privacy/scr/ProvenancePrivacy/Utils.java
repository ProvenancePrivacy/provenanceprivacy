package ProvenancePrivacy;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Stream;

import BasicsQBP.Rule;
import PEI.MonomialExample;
import algoNatural.FindConsQueryNatural;
import irisConn.ParsingTools;

public class Utils
{
	/******************************/
	/** Title: exceptions utils
	/******************************/
	public static void ExceptionHandler(String operation, Exception ex)
    {
		System.out.println("Error in " + operation);
        ex.printStackTrace();
        java.awt.Toolkit.getDefaultToolkit().beep();
        System.exit(-1);
    }
	
	/******************************/
	/** Title: files utils
	/******************************/
    public static String ReadFile(String filePath)
    {
        StringBuilder contentBuilder = new StringBuilder();
        try (Stream<String> stream = Files.lines( Paths.get(filePath), StandardCharsets.UTF_8))
        {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        }
        catch (IOException e)
        {
        	ExceptionHandler("reading text from file:" + filePath, e);
        }
        return contentBuilder.toString();
    }
    
    public static void AppendToFile(String data, String filePath)
    {
    	Path path = Paths.get(filePath);
    	try
    	{
			Files.write(path, (data + "\n").getBytes(), StandardOpenOption.APPEND);
		}
    	catch (IOException e)
    	{
			ExceptionHandler("appending text to file:" + filePath, e);
		}
    }
    
    public static void CreateFile(String filePath)
    {
    	try
    	{
            File file = new File(filePath);
            file.createNewFile();
        }
    	catch(Exception e)
    	{
    		ExceptionHandler("creating file:" + filePath, e);
        }
    }
    
    public static int CountNumberOfLines(String filename)
    {
    	int count = 0;
    	boolean empty = true;
        try
        {
        	InputStream is = new BufferedInputStream(new FileInputStream(filename));
            byte[] c = new byte[1024];
            int readChars = 0;
            while ((readChars = is.read(c)) != -1)
            {
                empty = false;
                for (int i = 0; i < readChars; ++i)
                {
                    if (c[i] == '\n')
                    {
                        ++count;
                    }
                }
            }
        }
    	catch(Exception e)
    	{
            ExceptionHandler("counting lines of file: " + filename, e);
        }
        
        return (count == 0 && !empty) ? 1 : count;
    }
    
    public static void PrepareOutputDirectories()
    {
    	new File(TestClass.OsBasePath).mkdir();
    	new File(TestClass.BaseDirectory).mkdir();
    	new File(TestClass.OutputsDirectory).mkdir();
    	new File(TestClass.WorkingDirectory).mkdir();
    	new File(TestClass.LogsDirectory).mkdir();
    	new File(TestClass.TpchDBTablesPath).mkdir();
    	new File(TestClass.AbsCacheDirectory).mkdir();
    }
    
    public static void ReplaceInFile(String filePath, String oldStr, String newStr) throws IOException
    {
		Path path = Paths.get(filePath);
		Charset charset = StandardCharsets.UTF_8;
		String content = new String(Files.readAllBytes(path), charset);
		content = content.replaceAll(oldStr, newStr);
		Files.write(path, content.getBytes(charset));
    }
    
    /******************************/
	/** Title: log utils
	/******************************/
    
    private static int qIndex = 1;
    private static int aIndex = 1;
    private static String logInternalPath = "";
    
    public static String GetLogFileName()
    {
    	if (logInternalPath != "")
    	{
    		return logInternalPath;
    	}
    	
    	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy-HH-mm-ss");  
    	LocalDateTime now = LocalDateTime.now();
    	logInternalPath = TestClass.LogsDirectory + dtf.format(now) + "-PRIVACY-" + TestClass.PrivacyThreshold + ".txt";
	   	
    	CreateFile(logInternalPath);
    	
    	System.out.println("[INFO] log file path is: " + logInternalPath);
    	
	   	return logInternalPath;
    }
    
    public static void LogRawText(String message)
    {
    	if (TestClass.ShouldSaveLogsToFile)
    	{
    		AppendToFile(message, GetLogFileName());
    	}
    	
    	if (TestClass.ShouldPrintLogsToScreen)
		{
    		System.out.println(message);
		}
    }
    
    public static void LogMessage(String message)
    {
    	LogRawText("[INFO] " + message + " (" + GetTimeMeasure() + ")");
    }
    
    public static void LogOriginalQuery(String originalExample, int privacyThreshold) throws Exception
    {
		MonomialExample me = ParsingTools.makeExample(originalExample);
		String output = "***** INPUT *****\n";
		output += "* Original Example Output:\n" + originalExample.trim() + "\n";
		output += "* Privacy threshold is " + privacyThreshold + "\n";
		output += "* LOI method is " + TestClass.LossOfInfoMethod + "\n";
		output += "* Original Query:\n(Q0) " + FindConsQueryNatural.findMostSpecificQuery(me) + "\n";
		LogRawText(output);
	}
    
    public static void LogAbstractionOptionDetails(String abstraction, double lossOfInfo, List<Rule> potentialQueries, 
    		int privacy)
    {
    	String output = "\n***** ABSTRACTION OPTION (A" + aIndex++ + ") *****\n";
    	output += "* current abstraction:\n" + abstraction.trim() + "\n";
    	output += "* current privacy is " + privacy + "\n";
    	output += "* current lossOfInfo is " + lossOfInfo + "\n";
    	output += "* potential queries:\n";
		for (Rule q : potentialQueries)
		{
			output += "(Q" + qIndex++ + ") " + q + "\n";
		}
		LogRawText(output);
	}
    
    public static void LogOutputDetails(double optimalLossOfInfo, int optimalPrivacy)
    {
    	String output = "\n***** OUTPUT *****\n";
    	output += "* Optimal privacy is " + optimalPrivacy + "\n";
    	output += "* Optimal loss of information is " + optimalLossOfInfo + "\n";
    	LogRawText(output);
	}
    
    public static void LogQueries(List<Rule> queries, String queriesMean)
    {
    	if (TestClass.ShoudLogAllPrivacyQueries)
    	{
	    	LogRawText(" >> " + queriesMean + " queries:\n");
	        int index = 1;
	        for (Rule q : queries)
	        {
	            LogRawText("(" + queriesMean + " Q" + index++ + ") " + q + "\n");
	        }
    	}
	}
    
    /******************************/
	/** Title: time utils
	/******************************/
    
    private static long totalStartTime = System.currentTimeMillis();
    
    public static void ResetTotalTimeMeasure()
    {
    	LogMessage("Reset Total Time Measure");
    	totalStartTime = System.currentTimeMillis();
    }
    
    public static String GetTimeMeasure()
    {
		return "Time: " + java.time.LocalTime.now() + " | Elapsed: " +
			((double)(System.currentTimeMillis()-totalStartTime))/1000 + "s";
    }
    
    private static long lastStartTime = System.currentTimeMillis();
    
    public static void StartTimeMeasure()
    {
    	lastStartTime = System.currentTimeMillis();
    }
    
    public static boolean StopTimeMeasure(String operation)
    {
    	double elapsed = System.currentTimeMillis()-lastStartTime;
    	boolean isLong = elapsed >= 3;
    	if (TestClass.ShouldLogInternalTimeMeasures && isLong)
    	{
    		System.out.println("[Time] " + operation + " elapsed time: " + (double)elapsed/1000 + "s");
    	}
    	
    	return isLong;
    }
    
    public static long GetLastTimeMeasure()
    {
    	return System.currentTimeMillis()-lastStartTime;
    }
    
    private static long experimentStartTime = System.currentTimeMillis();
    private static long totalPausedTime = 0;
    private static long pausedTime = System.currentTimeMillis();
    
    public static void PauseTotalTimeMeasure()
    {
    	pausedTime = System.currentTimeMillis();
    }
    
    public static void ResumeTotalTimeMeasure()
    {
    	totalPausedTime += System.currentTimeMillis() - pausedTime;
    }
    
    public static void StartExperimentTime()
    {
    	totalPausedTime = 0;
    	experimentStartTime = System.currentTimeMillis();
    }
    
    public static long GetExperimentTime()
    {
    	return System.currentTimeMillis() - experimentStartTime - totalPausedTime;
    }
    
    /******************************/
	/** Title: serialization utils
	/******************************/
    
    private static byte[] createChecksum(String filename) throws Exception {
        InputStream fis =  new FileInputStream(filename);

        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance("MD5");
        int numRead;

        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);

        fis.close();
        return complete.digest();
    }

    public static String getMD5Checksum(String filename)
    {
    	byte[] b = {0};
    	try
    	{
    		b = createChecksum(filename);
    	}
    	catch (Exception e)
    	{
            ExceptionHandler("getting MD5 of file:" + filename, e);
    	}
    	
        String result = "";
        for (int i = 0; i < b.length; i++)
        {
            result += Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
        }
        return result.toUpperCase();
    }
    
    private static String absPath = "";
    
    private static String GetPath(String origExample)
    {
    	if ("" != absPath)
    	{
    		return absPath;
    	}
    	
    	String treeMD5 = getMD5Checksum(TestClass.AbstractionTreePath);
    	String exampleMD5 = getMD5Checksum(TestClass.OriginalExamplePath);
    	String dbMD5 = getMD5Checksum(TestClass.IrisDBPath);
    	absPath = TestClass.AbsCacheDirectory + treeMD5 + "_" + exampleMD5 + "_" + dbMD5 + ".cache";
    	return absPath;
    }
    
    public static void SerializeAbstractions(List<String> allAbstractions, String origExample) {
		try
        {
	        FileOutputStream fout = new FileOutputStream(GetPath(origExample));
	        ObjectOutputStream oos = new ObjectOutputStream(fout);
	        oos.writeObject(allAbstractions);
	    }
		catch (Exception e)
		{
			ExceptionHandler("SerializeAbstractions", e);
		}
	}

	public static List<String> DeserializeAbstractions(String origExample)
	{
    	try
    	{
    		FileInputStream streamIn = new FileInputStream(GetPath(origExample));
    	    ObjectInputStream objectinputstream = new ObjectInputStream(streamIn);
    		return (List<String>) objectinputstream.readObject();
    	}
    	catch (Exception e)
    	{
    		ExceptionHandler("DeserializeAbstractions", e);
    	}
		return null;
	}
	
	public static Boolean AbstractionsAlreadyCalculated(String origExample)
	{
		return new File(GetPath(origExample)).isFile();
	}
	
	/******************************/
	/** Title: python runner utils
	/******************************/
	
	public static void CreateTreeViaPythonScript(String scriptPath) throws Exception
	{
    	Process process = Runtime.getRuntime().exec(new String[]{"python", scriptPath,
    			TestClass.IrisDBPath, TestClass.AbstractionTreePath, String.valueOf(TestClass.AbstractionTreeLevels),
    			TestClass.AbstractionTreeTable, String.valueOf(TestClass.AbstractionTreeSizeLimit), TestClass.OriginalExamplePath});
	    InputStream stdout = process.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(stdout, StandardCharsets.UTF_8));
        if (!reader.readLine().equals("done"))
        {
        	throw new Exception("Creating tree via python script " + scriptPath + " failed!");
        }
        LogMessage("creating tree using " + scriptPath + " has finished");
	}
	
	/******************************/
	/** Title: string utils
	/******************************/
	
	public static int CountSubstrings(String str, String subStr)
    {
    	int lastIndex = 0;
    	int count = 0;

    	while(lastIndex != -1)
    	{
    	    lastIndex = str.indexOf(subStr, lastIndex);

    	    if(lastIndex != -1)
    	    {
    	        count++;
    	        lastIndex += 1;
    	    }
    	}
    	
    	return count;
    }
}