package ProvenancePrivacy;

import java.io.File;

import ProvenancePrivacy.LossOfInformationCount.LOI_METHOD;
import ProvenancePrivacy.AbstractionTree.TreeNode;

public class TestClass
{
	// experiments settings
	public static final boolean ShouldRunExperiments = true;
	public static final boolean ShouldCreateFirstNewDB = true;
	public static final boolean ShouldCreateFirstQuery = true;
	public static final boolean ShouldCreateFirstTree = true;
	public static final int RunTimes = 10;
	
	public static final int TreeSizeStart = 10000;
	public static final int TreeSizeEnd = 10000;
	public static final int TreeSizeInterval = 100000;
	
	public static final int PrivacyStart = 2;
	public static final int PrivacyEnd = 20;
	public static final int PrivacyInterval = 3;
	
	public static final int QComplexStart = 8;
	public static final int QComplexEnd = 8;
	public static final int QComplexInterval = 1;
	
	public static final int TreeLevelsStart = 5;
	public static final int TreeLevelsEnd = 5;
	public static final int TreeLevelsInterval = 1;
	
	public static final int ExampleRowsStart = 2;
	public static final int ExampleRowsEnd = 2;
	public static final int ExampleRowsInterval = 1;
	
	public static final int TimeoutHardCoded = 3600;
	public static final int TimeoutMultiply = 100;
	public static final boolean IsEdgesCountTests = true;
	
	public static final boolean PrintExpLogs = true;
	public static final boolean PrintCacheUsage = true;
	
	// environment settings
	public static final boolean ShouldCreateNewEnvironment = true && !ShouldRunExperiments;
	public static final boolean ShouldCreateNewDB = true;
	public static final boolean ShouldCreateNewExample = true;
	public static final boolean ShouldCreateNewTree = true;
	public static final int QueryNumber = 13; // all the queries are in file TPCHConvertTBL.java
	public static final int DBRowsLimit = -1; // set to -1 for unlimited
	public static int ExampleRows = 2;
	public static final int ProvenanceType = 0; // 0 for N(x), 1 for Trio(x), 2 for Lin(x)
	public static final String AbstractionTreeTable = "lineitem"; // table names separated with ","
	public static int AbstractionTreeLevels = 5;
	public static int AbstractionTreeSizeLimit = 10000;
	public static int QueryComplexity = 8;
	
	// optimal abstraction settings
	public static final boolean ShouldFindOptimalAbstraction = true && !ShouldRunExperiments;
	public static final LOI_METHOD LossOfInfoMethod = LOI_METHOD.ENTROPY;
	public static int PrivacyThreshold = 2;
	
	// optimizations settings
	public static final boolean ShouldUseConsistentCache = true;
	public static final boolean ShouldUseConnectedCache = true;
	public static final boolean ShouldUseDisconnectedConcretizationsCache = true;
	public static final boolean ShouldFilterDisconnectedConcretizations = true;
	public static final boolean ShouldSortAbstractions = true;
	
	public static final boolean ShouldRandomShuffleAbstractions = false;
	
	// paths settings
	public static final String OsBasePath = System.getProperty("os.name").contains("Mac") ? "/Temp" : "C:\\Temp";
	public static final String BaseDirectory = OsBasePath + File.separator + "ProvenancePrivecy" + File.separator;
	
	public static final String TpchDBTablesPath = BaseDirectory + "TPCH_DB" + File.separator;
	public static final String ScriptsPath = System.getProperty("user.dir") + File.separator + "scripts" + File.separator;
	public static final String AbsCacheDirectory = BaseDirectory + "CACHE" + File.separator;
	
	public static final String OutputsDirectory = BaseDirectory + "OUT" + File.separator;
	public static final String WorkingDirectory = OutputsDirectory + "Q" + QueryNumber + File.separator;
	public static final String LogsDirectory = WorkingDirectory + "LOGS" + File.separator;
	
	public static final String IrisDBPath = WorkingDirectory + "TPCH-DB.iris";
	public static final String OriginalExamplePath = WorkingDirectory + "OriginalExample.txt";
	public static final String AbstractionTreePath = WorkingDirectory + "AbstractionTree.txt";
	public static final String ProvenanceFactsPath = WorkingDirectory + "ProvenanceFactsPath.iris";
	
	// log settings
	public static final boolean ShouldPrintLogsToScreen = true && !ShouldRunExperiments;
	public static final boolean ShouldSaveLogsToFile = true && !ShouldRunExperiments;
	public static final boolean ShoudLogAllPrivacyQueries = false && !ShouldRunExperiments;
	public static final boolean ShouldLogInternalTimeMeasures = false && !ShouldRunExperiments;
	public static final boolean ShoudLogOutputCIMConcretizations = true && !ShouldRunExperiments;
	
	// abstraction settings
	public static final boolean ShouldUseAbstractionCache = false;
	public static final int AbstractionsLimitNumber = -1; // set to -1 for unlimited
	
    public static void main(String[] args)
    {
		Utils.PrepareOutputDirectories();
		Utils.LogMessage("QueryNumber: " + QueryNumber + ", DBRowsLimit: " + DBRowsLimit + ", ExampleRows: "
				+ ExampleRows + ", AbstractionTreeType: " + Abstraction.GetAbstractionTreeType());
    	Utils.LogMessage("Start");
    	
    	try
        {
    		if (ShouldRunExperiments)
    		{
    			Runner.CreateEnvironment(ShouldCreateFirstNewDB, ShouldCreateFirstQuery, ShouldCreateFirstTree);
    			ExperimentsRunner.RunPriacyExperiment();
    		}
    		
	    	if (ShouldCreateNewEnvironment)
	    	{
	    		Runner.CreateEnvironment(ShouldCreateNewDB, ShouldCreateNewExample, ShouldCreateNewTree);
	    	}
	    	
	    	if (ShouldFindOptimalAbstraction)
	    	{
		        String originalExample = Utils.ReadFile(OriginalExamplePath);
		        TreeNode<String> abstractionTree = Abstraction.CreateAbstractionTree(AbstractionTreePath);
		        
		        Utils.StartExperimentTime();
		        Runner.CalculateOptimalAbstraction(originalExample, abstractionTree, PrivacyThreshold);
		        Utils.LogRawText("[EXP TIME] Experiment Time: " + (double)Utils.GetExperimentTime()/1000 + "s");
	    	}
    	}
        catch (Exception e)
        {
        	Utils.ExceptionHandler("MAIN", e);
        }
    	
    	Utils.LogMessage("Done");
    	java.awt.Toolkit.getDefaultToolkit().beep();
    }
}