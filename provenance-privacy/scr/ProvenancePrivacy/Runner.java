package ProvenancePrivacy;

import java.io.File;

import Experiments.QueryByProvenanceExp;
import ProvenancePrivacy.AbstractionTree.TreeNode;

public class Runner
{
	public static void CalculateOptimalAbstraction(String originalExample, TreeNode<String> abstractionTree,
			int privacyThreshold) throws Exception
	{
		Utils.LogMessage("Finding optimal abstraction...");
		
		String optimalAbstraction = OptimalAbstraction.findOptimalAbstraction(originalExample, 
				abstractionTree, privacyThreshold);
		
		Utils.LogRawText(" >> Optimal abstraction:\n" + optimalAbstraction);
	}
	
	public static void CreateEnvironment(boolean shouldCreateNewDB, boolean shouldCreateNewExample,
			boolean shouldCreateNewTree) throws Exception
	{
		Utils.LogMessage("Creating new environment...");
		
		if (shouldCreateNewDB)
		{
			// create db and query
			Utils.LogMessage("Preparing DB and query...");
			irisConn.TPCHConvertTBL.convertDB(TestClass.DBRowsLimit, TestClass.QueryNumber,
					TestClass.IrisDBPath, TestClass.TpchDBTablesPath);
			Utils.LogRawText("db number of lines is: " + (Utils.CountNumberOfLines(TestClass.IrisDBPath) - 1));
			
			// replace '!' with '#' in DB
			Utils.ReplaceInFile(TestClass.IrisDBPath, "!", "###");
		}
		
		if (shouldCreateNewExample)
		{
			// create example from query
			Utils.LogMessage("Creating example...");
			QueryByProvenanceExp.runIRIS_prov(TestClass.ExampleRows, TestClass.ProvenanceType, 
					TestClass.QueryNumber, TestClass.OriginalExamplePath, 
					TestClass.ProvenanceFactsPath, TestClass.IrisDBPath);
			new File(TestClass.ProvenanceFactsPath).delete();
			Utils.LogRawText("example number of lines is: " + (Utils.CountNumberOfLines(TestClass.OriginalExamplePath)));
		}
		
		if (shouldCreateNewTree)
		{
			// generate abstraction tree
			Utils.LogMessage("Generating abstraction tree...");

			Utils.CreateTreeViaPythonScript(TestClass.ScriptsPath + "TPCHTreeGenerator.py");

			Utils.LogRawText("abstraction tree number of lines is: " +
					Utils.CountNumberOfLines(TestClass.AbstractionTreePath));
		}
		
		Utils.LogMessage("The environment is ready!");
	}
}