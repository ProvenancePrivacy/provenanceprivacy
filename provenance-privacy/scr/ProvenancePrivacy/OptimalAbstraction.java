package ProvenancePrivacy;

import ProvenancePrivacy.AbstractionTree.TreeNode;
import java.util.*;
import BasicsQBP.Rule;

public class OptimalAbstraction
{
	public static double optLOI = 0;
    /*************************************************************************************************************/
	/** Title: findOptimalAbstraction																				
	/** Description: returns the optimal abstraction (over the privacy threshold and minimize the loss of info)		 
	/*************************************************************************************************************/	
    public static String findOptimalAbstraction(String origExample, TreeNode<String> absTree, 
    		int privacyThreshold) throws Exception
    {
    	PrivacyCount.connectedCache.clear();
    	PrivacyCount.consistentCache.clear();
    	Abstraction.connectedExCache.clear();
    	Abstraction.monExampleToStrExample.clear();
    	
    	Utils.LogOriginalQuery(origExample, privacyThreshold);
    	
        String optimalAbstraction = "";
        double optimalLossOfInfo = Double.MAX_VALUE;
        int optimalPrivacy = 0;
        
        Utils.StartTimeMeasure();
        List<String> allAbstractions = getAllAbstractions(origExample, absTree);
        Utils.StopTimeMeasure("getAllAbstractions");
        Utils.LogMessage("total number of abstractions: " + allAbstractions.size());
        
        int abstractionIndex = 0;
        int printIndex = 0;
        for (String abstraction : allAbstractions)
        {
        	abstractionIndex++;
        	
        	double lossOfInfo = LossOfInformationCount.CalculateLossOfInfo(origExample, abstraction, absTree, TestClass.LossOfInfoMethod);
        	if (lossOfInfo > optimalLossOfInfo)
            {
                continue;
            }
            
        	if (++printIndex % 20 == 0)
        	{
        		Utils.LogMessage("scanned " + abstractionIndex + "\\" + allAbstractions.size() + " abstractions");
        		Utils.LogRawText("[EXP TIME] LOI: " + lossOfInfo + ", Current experiment Time: " + (double)Utils.GetExperimentTime()/1000 + "s");
        	}

            List<Rule> potentialQueries = PrivacyCount.GetPrivacyQueries(abstraction, absTree);
            int privacy = potentialQueries.size();

            if (privacy < privacyThreshold)
            {
                continue;
            }

            Utils.LogAbstractionOptionDetails(abstraction, lossOfInfo, potentialQueries, privacy);
            
            Utils.LogRawText("[EXP TIME] Current experiment Time: " + (double)Utils.GetExperimentTime()/1000 + "s");

            if (privacy > optimalPrivacy || lossOfInfo < optimalLossOfInfo)
            {
                optimalAbstraction = abstraction;
                optimalLossOfInfo = lossOfInfo;
                optimalPrivacy = privacy;
            }
        }

        Utils.LogOutputDetails(optimalLossOfInfo, optimalPrivacy);
        
        if (0 == optimalPrivacy)
        {
        	throw new Exception("Privacy is 0");
        }
        
        System.out.println("privacy: " + optimalPrivacy);
        if (TestClass.PrintExpLogs)
		{
        	System.out.println("\toptimal abstraction:\n\t\t" + optimalAbstraction.trim().replace("\n", "\n\t\t"));
		}
        
        optLOI = optimalLossOfInfo;
        return optimalAbstraction;
    }

	/*************************************************************************************************************/
	/** Title: getAllAbstractions																				
	/** Description: return all the abstractions of a given example and abstraction tree	 
	/*************************************************************************************************************/	
    private static List<String> getAllAbstractions(String origExample, TreeNode<String> absTree)
    {
    	if (TestClass.ShouldUseAbstractionCache && Utils.AbstractionsAlreadyCalculated(origExample))
    	{
    		List<String> cacheAbstractions = Utils.DeserializeAbstractions(origExample);
    		if (cacheAbstractions != null)
    		{
    			Utils.LogMessage("loaded abstractions from cache");
    			return cacheAbstractions;
    		}
    	}
        
    	List<String> allAbstractions = new ArrayList<>();
        allAbstractions.add(origExample);

        Queue<String> waiting = new LinkedList<>();
        waiting.add(origExample);

        while (!waiting.isEmpty())
        {
            String current = waiting.poll();

            int atomStartIndex = current.indexOf("|") + 1;;
            while (atomStartIndex < current.length())
            {
                int atomEndIndex = current.indexOf("|", atomStartIndex);
                String atomData = "";
                if (atomEndIndex != -1)
                {
                    atomData = current.substring(atomStartIndex, atomEndIndex).split("\n")[0];
                }
                else
                {
                    atomData = current.trim().substring(atomStartIndex);
                    atomEndIndex = current.length();
                }

                int realAtomEnd = atomStartIndex + atomData.length();
                if (atomData.contains("!")) {
                    atomData = atomData.substring(1, atomData.length() - 1);
                }

                TreeNode<String> atomNode = absTree.findTreeNode(atomData);
                if (atomNode == null || atomNode.isRoot())
                {
                    atomStartIndex = atomEndIndex + 1;
                    continue;
                }

                String newExample = current.substring(0, atomStartIndex)
                        + "!" + atomNode.parent.data + "!" + current.substring(realAtomEnd);
                if (!allAbstractions.contains(newExample))
                {
                    allAbstractions.add(newExample);
                    waiting.add(newExample);
                }

                atomStartIndex = atomEndIndex + 1;

                if (-1 != TestClass.AbstractionsLimitNumber && allAbstractions.size() >= TestClass.AbstractionsLimitNumber)
                {
                    Utils.SerializeAbstractions(allAbstractions, origExample);
                    return allAbstractions;
                }
            }
        }
        
        if (TestClass.ShouldSortAbstractions)
        {
        	allAbstractions = sortAbstractionsByLOI(allAbstractions, origExample, absTree);
        }
        
        if (TestClass.ShouldRandomShuffleAbstractions)
        {
        	Collections.shuffle(allAbstractions);
        }
        
        if (TestClass.ShouldUseAbstractionCache)
        {
        	Utils.SerializeAbstractions(allAbstractions, origExample);
        }
        
        return allAbstractions;
    }
    
    /*************************************************************************************************************/
	/** Title: sortAbstractionsByLOI																				
	/** Description: sort all abstraction by LOI from lower to higher
	/*************************************************************************************************************/	
    private static List<String> sortAbstractionsByLOI(List<String> allAbstractions, String origExample, 
    		TreeNode<String> absTree)
    {
	    Utils.StartTimeMeasure();
	    allAbstractions.sort(new Comparator<String>()
	    {
	        @Override
	        public int compare(String s1, String s2)
	        {
	            try
	            {
	            	
	            	if (Utils.CountSubstrings(s1, "!") > Utils.CountSubstrings(s2, "!"))
	            	{
	            		return 1;
	            	}
	            	
	            	if (Utils.CountSubstrings(s1, "!") < Utils.CountSubstrings(s2, "!"))
	            	{
	            		return -1;
	            	}
	            	
	            	
	            	double loi = LossOfInformationCount.CalculateLossOfInfo(origExample, s1, absTree, TestClass.LossOfInfoMethod)
							- LossOfInformationCount.CalculateLossOfInfo(origExample, s2, absTree, TestClass.LossOfInfoMethod);
					if (0 < loi)
					{
						return 1;
					}
					else if (0 > loi)
					{
						return -1;
					}
					else
					{
						return 0;
					}
				}
	            catch (Exception e)
	            {
		            Utils.ExceptionHandler("comparing abstraction via LOI", e);
				}
	            return 0;
	        }
	    });
	    Utils.StopTimeMeasure("sort all abstractions");
	    
	    return allAbstractions;
    }
}