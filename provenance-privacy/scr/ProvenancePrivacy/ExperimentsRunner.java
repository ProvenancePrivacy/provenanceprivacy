package ProvenancePrivacy;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import ProvenancePrivacy.AbstractionTree.TreeNode;

public class ExperimentsRunner
{
	public static void RunPriacyExperiment() throws Exception
	{
		System.out.println(
				"[*] QueryNumber: " + TestClass.QueryNumber 
				+ ", DBRowsLimit: " + TestClass.DBRowsLimit
				+ ", AbstractionTreeType: " + Abstraction.GetAbstractionTreeType()
			);
		
		System.out.println("[*] Starting All Experiments....\n");
        for (int privacy = TestClass.PrivacyStart; privacy <= TestClass.PrivacyEnd; privacy += TestClass.PrivacyInterval)
        {
        	TestClass.PrivacyThreshold = privacy;
        	for (int treeSize = TestClass.TreeSizeStart; treeSize <= TestClass.TreeSizeEnd; treeSize += TestClass.TreeSizeInterval)
        	{
        		TestClass.AbstractionTreeSizeLimit = treeSize;
        		for (int qComplex = TestClass.QComplexStart; qComplex <= TestClass.QComplexEnd; qComplex += TestClass.QComplexInterval)
            	{
        			TestClass.QueryComplexity = qComplex;
        			for (int levels = TestClass.TreeLevelsStart; levels <= TestClass.TreeLevelsEnd; levels += TestClass.TreeLevelsInterval)
                	{
        				TestClass.AbstractionTreeLevels = levels;
        				for (int rows = TestClass.ExampleRowsStart; rows <= TestClass.ExampleRowsEnd; rows += TestClass.ExampleRowsInterval)
                    	{
        					TestClass.ExampleRows = rows;
        					
        					RunExperimentCategory();
                    	}
                	}
            	}
        	}
        }
        System.out.println("\n[*] Finished All experiments");
	}
	
	private static void RunExperimentCategory() throws Exception
	{
		System.out.println("********* NEW EXPERIMENT CATEGORY *********\n["
    			+ "QCOMPLEX: " + TestClass.QueryComplexity
    			+ ", TREE SIZE: " + TestClass.AbstractionTreeSizeLimit
    			+ ", TREE LEVELS: " + TestClass.AbstractionTreeLevels
    			+ ", PRIVACY: " + TestClass.PrivacyThreshold
    			+ ", EX ROWS: " + TestClass.ExampleRows
    			+ "]");
    	
    	int timeout = TestClass.TimeoutHardCoded != -1 ? 
    			TestClass.TimeoutHardCoded : TestClass.PrivacyThreshold * TestClass.ExampleRows * TestClass.TimeoutMultiply;
    	
    	double sum = 0;
    	for (int run = 0; run < TestClass.RunTimes; run++)
        {
    		Abstraction.connectedChacheCount = 0;
    		PrivacyCount.consistentCacheCount = 0;
    		PrivacyCount.connectedCacheCount = 0;
    		
    		// run experiment
    		long output = RunExperiment(timeout);
    		
    		if (TestClass.PrintCacheUsage)
    		{
    			System.out.println("* 1 Conc Conncted Cache: " + Abstraction.connectedChacheCount);
    			System.out.println("* 2 Query Consistent Cache: " + PrivacyCount.consistentCacheCount);
    			System.out.println("* 3 Query Connected Cache: " + PrivacyCount.connectedCacheCount);
    		}
    		if (!TestClass.IsEdgesCountTests && output < 50)
    		{
    			System.out.println("\tTime too short...");
    			run--;
    			continue;
    		}
    		if (TestClass.IsEdgesCountTests && output == 0)
    		{
    			if (TestClass.PrintExpLogs)
    			{
    				System.out.println("\tEdges Count not found...");
    			}
    			run--;
    			continue;
    		}
    		System.out.println("* CURRENT LOI: " + OptimalAbstraction.optLOI);
    		sum += OptimalAbstraction.optLOI;// output;
    	}
    	double avg = (double)sum / (double)TestClass.RunTimes;
    	
    	if (TestClass.IsEdgesCountTests)
    	{
    		System.out.println("[+] AVERAGE EDGES: " + ((double)avg) + "\n");
    	}
    	else
    	{
    		System.out.println("[+] AVERAGE TIME: " + ((double)avg/1000) + "s\n");
    	}
    	java.awt.Toolkit.getDefaultToolkit().beep();
	}
	
    private static long RunExperiment(int timeout) throws Exception
    {
        while (true)
        {
        	if (TestClass.PrintExpLogs)
			{
        		System.out.println(">> Starting new experiment");
        		System.out.println("\tcreating environment...");
			}
        	Runner.CreateEnvironment(TestClass.ShouldCreateNewDB, TestClass.ShouldCreateNewExample, TestClass.ShouldCreateNewTree);
        	if (TestClass.PrintExpLogs)
			{
        		System.out.println("\tdb size: " + Utils.CountNumberOfLines(TestClass.IrisDBPath)
        		+ ", ex size: " + Utils.CountNumberOfLines(TestClass.OriginalExamplePath)
        		+ ", abstree size: " + Utils.CountNumberOfLines(TestClass.AbstractionTreePath));
			}
        	ExecutorService executor = Executors.newSingleThreadExecutor();
            Future<Long> future = executor.submit(TestClass.IsEdgesCountTests 
            		? new CountEdgesForOptimalPrivacyTask() : new CalculateTimeForOptimalPrivacyTask());
            
	        try
	        {
	        	if (TestClass.PrintExpLogs)
    			{
	        		System.out.println("\trunning experiment... (timeout " + timeout + "s)");
    			}
	        	long output = future.get(timeout, TimeUnit.SECONDS);
	        	
	        	if (output == -1)
	        	{
	            	System.out.println("\tTrying again...");
	            	future.cancel(true);
	        		continue;
	        	}
	        	
	        	if (TestClass.IsEdgesCountTests)
	        	{
	        		System.out.println("Exp Edges: " + output);
	        	}
	        	else
	        	{
	        		System.out.println("Exp Time: " + ((double)output/1000) + "s");
	        	}
	            return output;
	        }
	        catch (TimeoutException e)
	        {
	        	System.out.println("\tTimeout! Trying again...");
	            future.cancel(true);
	        }
	        finally
	        {
	        	executor.shutdownNow();
	        }
        }
    }
}

class CountEdgesForOptimalPrivacyTask implements Callable<Long>
{
	public CountEdgesForOptimalPrivacyTask()
	{
	}
	
	private long CountTreeEdges(String abstraction)
	{
		int edgesCount = 0;
		
		String[] parts = abstraction.split("!");
	    Boolean isAbstractedValue = true;
	    for (int i = 0; i < parts.length; i++)
		{
	    	isAbstractedValue = !isAbstractedValue;
	    	if (!isAbstractedValue)
	    	{
	    		continue;
	    	}
	    	
	    	String part = parts[i];
	    	
	    	int removedLevels = ("star" == part) ? 0 : Utils.CountSubstrings(part, "_");
	    	
	    	edgesCount += TestClass.AbstractionTreeLevels - removedLevels;
		}
	    
		return edgesCount;
	}
	
    @Override
    public Long call()
    {
    	try
        { 
        	String originalExample = Utils.ReadFile(TestClass.OriginalExamplePath);
            TreeNode<String> abstractionTree = Abstraction.CreateAbstractionTree(TestClass.AbstractionTreePath);
        	
            long regularStartTime = System.currentTimeMillis();
	        Utils.StartExperimentTime();
	    		
	        String optimalAbstraction = OptimalAbstraction.findOptimalAbstraction(originalExample, 
	        		abstractionTree, TestClass.PrivacyThreshold);
	        
	        if (TestClass.PrintExpLogs)
			{
	        	System.out.println("\tElapsed Time: " + (((double)System.currentTimeMillis()-regularStartTime)/1000));
			}
			return CountTreeEdges(optimalAbstraction);
        }
        catch (Exception e)
        {
        	System.out.println("\tException! Message: " + e.getMessage());
        }
		
		return (long) 0;
    }
}


class CalculateTimeForOptimalPrivacyTask implements Callable<Long>
{
	public CalculateTimeForOptimalPrivacyTask()
	{
	}
	
    @Override
    public Long call()
    {
    	long time = -1;
    	try
        { 
        	String originalExample = Utils.ReadFile(TestClass.OriginalExamplePath);
            TreeNode<String> abstractionTree = Abstraction.CreateAbstractionTree(TestClass.AbstractionTreePath);
        	
            long regularStartTime = System.currentTimeMillis();
	        Utils.StartExperimentTime();
	    		
	        Runner.CalculateOptimalAbstraction(originalExample, abstractionTree, TestClass.PrivacyThreshold);
	        
	        time = Utils.GetExperimentTime();
	        
	        System.out.println("\tElapsed Time: " + (((double)System.currentTimeMillis()-regularStartTime)/1000));
        }
        catch (Exception e)
        {
        	System.out.println("\tException! Message: " + e.getMessage());
        }
		
		return time;
    }
}