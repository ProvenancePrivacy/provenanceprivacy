package ProvenancePrivacy;

import irisConn.ParsingTools;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import BasicsQBP.Constant;
import BasicsQBP.Atom;
import BasicsQBP.Proton;
import BasicsQBP.Rule;
import BasicsQBP.Var;
import PEI.MonomialExample;
import ProvenancePrivacy.AbstractionTree.TreeNode;

public class Abstraction
{
	public static String GetAbstractionTreeType()
	{
		return TestClass.AbstractionTreeTable + "-" + TestClass.AbstractionTreeLevels + "-levels";
	}
	
	/*************************************************************************************************************/
	/** Title: CreateAbstractionTree																				
	/** Description: given an abstraction tree file, it creates an abstraction tree
	/*************************************************************************************************************/	
    public static TreeNode<String> CreateAbstractionTree(String treeFilePath)
    {
        String edgeSeparator = "->";
        String treeData = Utils.ReadFile(treeFilePath);

        String rootData = treeData.split(edgeSeparator)[0];
        TreeNode<String> abstractionTree = new TreeNode<String>(rootData);

        for (String edge : treeData.split("\n"))
        {
            String source = edge.split(edgeSeparator)[0];
            double weight = Double.parseDouble(edge.split(edgeSeparator)[1]);
            String target = edge.split(edgeSeparator)[2];

            abstractionTree.findTreeNode(source).addChild(target, weight);
        }

        return abstractionTree;
    }
    
    /*************************************************************************************************************/
	/** Title: GetConcretizationSetSize																				
	/** Description: get the size of the concretization set of a given abstracted example without calculate it
	/*************************************************************************************************************/	
    public static long GetConcretizationSetSize(String absExample, TreeNode<String> absTree)
    {
    	long size = 1;

        String[] parts = absExample.split("!");
        for (int i = 1; i < parts.length - 1; i += 2)
        {
            String abstractedValue = parts[i];
            
            TreeNode<String> found = absTree.findTreeNode(abstractedValue);

            size *= found.getAllLeaves().size();
        }

        return size;
    }
    
    
    // cache for disconnected concretizations
	public static HashMap<MonomialExample, Boolean> connectedExCache = new HashMap<>();
    
	public static int connectedChacheCount = 0;
	
	/*************************************************************************************************************/
	/** Title: IsExampleConnected																				
	/** Description: check weather a concretization is connected or not
	/*************************************************************************************************************/	
    private static boolean IsExampleConnected(MonomialExample example)
    {
    	if (!TestClass.ShouldFilterDisconnectedConcretizations)
		{
    		return true;
		}
    	
    	if (TestClass.ShouldUseDisconnectedConcretizationsCache && connectedExCache.containsKey(example))
    	{
    		connectedChacheCount++;
    		return connectedExCache.get(example);
    	}
    	
    	for (int i = 0; i < example.getTuples().size(); i++)
    	{
    		List<Atom> connectedAtoms = new ArrayList<>();
            connectedAtoms.add(example.getOutputTuple(i));
            List<Constant> allConstants = new ArrayList<>();
            for (Proton p : example.getOutputTuple(i).getParams())
            {
                if (p instanceof Constant)
                {
                	allConstants.add((Constant)p);
                }
            }
            List<Atom> allAtoms = new ArrayList<>();
            allAtoms.addAll(example.getMonomial(i).getTuples());
            boolean wasFound;
            do
            {
                wasFound = false;
                for (Atom current : allAtoms)
                {
                    if (connectedAtoms.contains(current))
                    {
                        continue;
                    }
                    boolean shouldAdd = false;
                    for (Proton p : current.getParams())
                    {
                        if (p instanceof Constant && allConstants.contains(p))
                        {
                            shouldAdd = true;
                            break;
                        }
                    }
                    if (shouldAdd)
                    {
                        connectedAtoms.add(current);
                        for (Proton p : current.getParams())
                        {
                            if (p instanceof Constant)
                            {
                            	allConstants.add((Constant)p);
                            }
                        }
                        wasFound = true;
                    }
                }
            } while (wasFound);
            
            if (connectedAtoms.size() != example.getMonomial(i).getTuples().size() + 1)
            {
            	// not connected
            	if (TestClass.ShouldUseDisconnectedConcretizationsCache)
            	{
            		connectedExCache.put(example, false);
            	}
            	return false;
            }
    	}
    	
    	// connected
    	if (TestClass.ShouldUseDisconnectedConcretizationsCache)
    	{
    		connectedExCache.put(example, true);
    	}
    	return true;
    }
    
    public static HashMap<MonomialExample, String> monExampleToStrExample = new HashMap<>();
    private static double concLeftSum = 0;
    private static double concLeftCount = 0;
    
    /*************************************************************************************************************/
	/** Title: GetConcretizationSet																				
	/** Description: get the concretization set of a given abstracted example
	/*************************************************************************************************************/	
    public static List<MonomialExample> GetConcretizationSet(String absExample, TreeNode<String> absTree) throws Exception
    {
        List<String> allExamples = new ArrayList<String>();
        allExamples.add(absExample);

        String[] parts = absExample.split("!");
        for (int i = 1; i < parts.length - 1; i += 2)
        {
            String abstractedValue = parts[i];

            List<String> curExample = new ArrayList<String>();

            TreeNode<String> found = absTree.findTreeNode(abstractedValue);
            for (TreeNode<String> leaf : found.getAllLeaves())
            {
                for (String currentTextualExample : allExamples)
                {
                    String replaceRegex = "!" + abstractedValue + "!";
                    curExample.add(currentTextualExample.replaceFirst(replaceRegex, leaf.data));
                }
            }

            allExamples = curExample;
        }

        List<MonomialExample> monomialsExamples = new ArrayList<>();
        for (String example : allExamples)
        {
        	MonomialExample cur = ParsingTools.makeExample(example);
            monomialsExamples.add(cur);
            monExampleToStrExample.put(cur, example);
        }
        
        List<MonomialExample> monomialsExamplesOut = new ArrayList<>();
        for (MonomialExample curEx : monomialsExamples)
        {
    		if (IsExampleConnected(curEx))
        	{	
    			monomialsExamplesOut.add(curEx);
        	}
        }
        
        double currentLeft = ((double)monomialsExamplesOut.size()/(double)monomialsExamples.size());
        //System.out.println("* Current: " + currentLeft);
        
        concLeftSum += currentLeft;
        concLeftCount++;
        //System.out.println("* Average: " + concLeftSum / concLeftCount);

        return monomialsExamplesOut;
    }
}
