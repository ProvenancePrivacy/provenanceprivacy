package heuristicTools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import BasicsQBP.Atom;
import BasicsQBP.Proton;
import PEI.Monomial;
import PEI.MonomialExample;

public class CoveredAttributesTools 
{
	/*************************************************************************************************************/
	/** Title: findSpreadOutMonomial																				
	/** Description: Finds the monomial with the most tuples needed to cover the head attributes				 
	/*************************************************************************************************************/	
	
	/*public static Monomial findSpreadOutMonomial (MonomialExample ex)
	{
		List<List<AtomArraySum>> maps = new ArrayList<List<AtomArraySum>>();
		for (int i = 0; i < ex.rowNumber(); i++) 
		{
			List<AtomArraySum> attr = new ArrayList<AtomArraySum>();
			maps.add(attr);
			coveredAttributesPerRow(i, ex, attr);
		}
		
		List<AtomArraySum> aas = findSpreadOutMonomial(ex, maps);
		return convertToMonom(aas);
	}*/
	
	
	/*************************************************************************************************************/
	/** Title: coveredAttributesPerRow																				
	/** Description: makes a map of the indices that each tuple in the monom covers in the head per row				 
	/*************************************************************************************************************/	
	
	public static void coveredAttributesPerRow(int idx, MonomialExample ex, List<AtomArraySum> maps)
	{
		List<Proton> attr = ex.getOutputTuple(idx).getParams();
		for (Atom tuple : ex.getMonomial(idx).getTuples()) 
		{
			AtomArraySum cur = new AtomArraySum(attr.size(), tuple);
			for (Proton proton : tuple.getParams()) 
			{
				if (attr.contains(proton)) 
				{
					cur.getAttrCovered()[attr.indexOf(proton)] = cur.getTuple().getParams().indexOf(proton);
				}
			}
			
			cur.calcSum();
			maps.add(cur);
		}
	}
	
	
	/*************************************************************************************************************/
	/** Title: coverSameAttr																				
	/** Description: check if tuple B covers the same attributes as tuple A				 
	/*************************************************************************************************************/	
	
	public static boolean coverSameAttr(AtomArraySum tupleA, AtomArraySum tupleB)
	{
		int [] A = tupleA.getAttrCovered();
		int [] B = tupleB.getAttrCovered();
		for (int i = 0; i < A.length; i++)
		{
			if (A[i] != -1 && A[i] != B[i])
			{
				return false;
			}
		}
		
		return true;
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: aggregateMonomialsSize																				
	/** Description: multiply the sizes of all monomials in the example 	 
	/*************************************************************************************************************/
	
	public static int aggregateMonomialsSize (MonomialExample ex)
	{
		int retVal = 1;
		for (Monomial monom : ex.getMonoms()) 
		{
			retVal *= monom.getTuples().size();
		}
		
		return retVal;
	}
	
	
	/*************************************************************************************************************/
	/** Title: allMonomsSameSize																				
	/** Description: returns true iff all monoms in the example are of the same size				 
	/*************************************************************************************************************/	
	
	public static boolean allMonomsSameSize(MonomialExample ex)
	{
		int size = ex.getMonomial(0).size();
		for (Monomial m : ex.getMonoms()) 
		{
			if (m.size() != size) 
				return false;
		}
		
		return true;
	}
	
	
	/*************************************************************************************************************/
	/** Title: findSpreadOutMonomial																				
	/** Description: Finds the monomial with the most tuples needed to cover the head attributes				 
	/*************************************************************************************************************/	
	
	/*private static List<AtomArraySum> findSpreadOutMonomial(MonomialExample ex, List<List<AtomArraySum>> maps)
	{
		List<AtomArraySum> curMonom;
		List<AtomArraySum> spreadOutMonom = null;
		int atomsCovered, max = 0;
		for (int i = 0; i < ex.rowNumber(); i++) 
		{
			curMonom = maps.get(i);
			atomsCovered = checkNumCoveringAtoms(maps.get(i));
			if (atomsCovered > max) 
			{
				spreadOutMonom = curMonom;
			}
		}
		
		return spreadOutMonom;
	}*/
	
	
	/*************************************************************************************************************/
	/** Title: checkNumCoveringAtoms																				
	/** Description: Finds the minimal number of tuples in the monomial needed to cover all the head attributes				 
	/*************************************************************************************************************/	
	
	/*private static int checkNumCoveringAtoms(List<AtomArraySum> maps)
	{
		int attrNum = maps.get(0).getAttrCovered().length;
		
		//create attribute array to follow the advance of the covering
		boolean [] attrCovered = new boolean [attrNum];
		Arrays.fill(attrCovered, false);
		
		//sort the objects by sum of the array in descending order
		Collections.sort(maps, Collections.reverseOrder());
		
		for (int j = 0; j < maps.size(); j++) // for all tuples in the monomial
		{
			AtomArraySum obj = maps.get(j);
			for (int i = 0; i < attrNum; i++) // all the range of the array of the tuple 
			{
				if (false == attrCovered[i]) // attr. hasn't been covered yet
				{
					//attrCovered[i] = attrCovered[i] || obj.getAttrCovered()[i];
					obj.setInTheCover(true);
				}
			}
			
			if (isAllTrue( attrCovered )) //all attributes are covered
			{
				return j; // return the number of atoms 
			}
		}
		
		return -1;
	}*/
	
	
	
	/*************************************************************************************************************/
	/** Title: isAllTrue																				
	/** Description: checks if all values of a boolean array are 'true'				 
	/*************************************************************************************************************/	
	
	/*private static boolean isAllTrue (boolean [] arr)
	{
		for (boolean val : arr)
		{
			if (!val) return false; //val == false  
		}
		return true;
	}*/
	
	
	/*************************************************************************************************************/
	/** Title: findTuplesToDuplicate																				
	/** Description: 				 
	/*************************************************************************************************************/	
	
	/*private static void findTuplesToDuplicate (MonomialExample ex, List<List<AtomArraySum>> maps, List<AtomArraySum> spreadOut)
	{
		List<AtomArraySum> duplicate = new ArrayList<AtomArraySum>();
		maps.remove(spreadOut);
		for (List<AtomArraySum> list : maps) 
		{
			for (AtomArraySum cur : list) 
			{
				if (true == noTupleCoversCur(cur, spreadOut)) 
				{
					duplicate.add(cur);
				}
			}
		}
	}*/
	
	
	/*************************************************************************************************************/
	/** Title: noTupleCoversCur																				
	/** Description: true iff there is no atom in the most spread out monomial that covers all attributes that cur covers 				 
	/*************************************************************************************************************/	
	
	/*private static boolean noTupleCoversCur (AtomArraySum cur, List<AtomArraySum> spreadOut)
	{
		boolean covered = true;
		int [] track = new int [cur.getAttrCovered().length];
		System.arraycopy( cur.getAttrCovered(), 0, track, 0, cur.getAttrCovered().length );
		
		for (AtomArraySum atomArraySum : spreadOut) 
		{
			for (int i = 0; i < cur.getAttrCovered().length; i++) 
			{
				//if found an attribute that cur covers and atomArraySum does not cover
				if (cur.getAttrCovered()[i] == true && atomArraySum.getAttrCovered()[i] == false)
				{
					covered = false;
					break;
				}
			}
			
			if (covered) 
			{
				return false;
			}
		}
		
		return true;
	}*/
	
	
	/*************************************************************************************************************/
	/** Title: convertToMonom																				
	/** Description: 	 
	/*************************************************************************************************************/	
	
	/*private static Monomial convertToMonom(List<AtomArraySum> maps)
	{
		Monomial monom = new Monomial();
		for (AtomArraySum atomArraySum : maps) 
		{
			monom.addTuple( atomArraySum.getTuple() );
		}
		
		return monom;
	}*/
}
