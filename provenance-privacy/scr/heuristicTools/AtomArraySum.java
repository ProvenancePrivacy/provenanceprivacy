package heuristicTools;

import java.util.Arrays;

import BasicsQBP.Atom;

public class AtomArraySum implements Comparable
{
	private Atom tuple;
	
	private int [] attrCovered;
	
	private int sum;
	
	private boolean inTheCover;
	
	private int exponent;
	
	public AtomArraySum(int numAttr, Atom t)
	{
		attrCovered = new int [numAttr];
		Arrays.fill(attrCovered, -1);
		sum = 0;
		tuple = t;
		exponent = 1;
	}


	public Atom getTuple() {
		return tuple;
	}


	public void setTuple(Atom tuple) {
		this.tuple = tuple;
	}


	public int[] getAttrCovered() {
		return attrCovered;
	}


	public void setAttrCovered(int[] attrCovered) {
		this.attrCovered = attrCovered;
	}


	public int getSum() {
		return sum;
	}


	public void setSum(int sum) {
		this.sum = sum;
	}
	
	
	public void calcSum (){
		for (int val : this.attrCovered) 
		{
			if (val != -1)
				this.sum ++;
		}
	}


	public boolean isInTheCover() {
		return inTheCover;
	}


	public void setInTheCover(boolean inTheCover) {
		this.inTheCover = inTheCover;
	}
	
	
	public boolean isCoveringSomething () {
		for (int i : this.attrCovered) 
		{
			if (i != -1) 
				return true;
		}
		
		return false;
	}

	
	public int getExponent() {
		return exponent;
	}


	public void setExponent(int exponent) {
		this.exponent = exponent;
	}


	@Override
	public int compareTo(Object o) {
		if (false == (o instanceof AtomArraySum))
		      throw new ClassCastException("A AtomArraySum object expected.");
		int otherSum = ((AtomArraySum) o).getSum();  
		return Integer.compare(sum, otherSum);
	}


	@Override
	public String toString() {
		return "[tuple=" + tuple + ", attrCovered="
				+ Arrays.toString(attrCovered) + ", sum=" + sum
				+ ", inTheCover=" + inTheCover + ", exponent=" + exponent + "]";
	}
	
}
