package heuristicTools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import PEI.MonomialExample;

public class SpreadOutSetFinder 
{
	/*************************************************************************************************************/
	/** Title: findSpreadOutSet																				
	/** Description: Finds the set with the most tuples needed to cover the head attributes				 
	/*************************************************************************************************************/	
	
	public static List<AtomArraySum> findSpreadOutSet (MonomialExample ex)
	{
		List<List<AtomArraySum>> maps = new ArrayList<List<AtomArraySum>>();
		for (int i = 0; i < ex.rowNumber(); i++) 
		{
			List<AtomArraySum> attr = new ArrayList<AtomArraySum>();
			maps.add(attr);
			CoveredAttributesTools.coveredAttributesPerRow(i, ex, attr);
		}
		
		List<AtomArraySum> aas = findSpreadOutMonomial(ex, maps);
		return aas;
	}
	
	
	/*************************************************************************************************************/
	/** Title: findSpreadOutSet																				
	/** Description: Different version that also updates the maps list				 
	/*************************************************************************************************************/
	
	public static List<AtomArraySum> findSpreadOutSet (MonomialExample ex, List<List<AtomArraySum>> maps)
	{
		for (int i = 0; i < ex.rowNumber(); i++) 
		{
			List<AtomArraySum> attr = new ArrayList<AtomArraySum>();
			maps.add(attr);
			CoveredAttributesTools.coveredAttributesPerRow(i, ex, attr);
		}
		
		List<AtomArraySum> aas = findSpreadOutMonomial(ex, maps);
		return aas;
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: findSpreadOutMonomial																				
	/** Description: Finds the monomial with the most tuples needed to cover the head attributes				 
	/*************************************************************************************************************/	
	
	private static List<AtomArraySum> findSpreadOutMonomial(MonomialExample ex, List<List<AtomArraySum>> maps)
	{
		List<AtomArraySum> curMonom;
		List<AtomArraySum> spreadOutMonom = null;
		int atomsCovered, max = 0;
		for (int i = 0; i < ex.rowNumber(); i++) 
		{
			curMonom = maps.get(i);
			atomsCovered = checkNumTuplesNeededToCover( curMonom, false );
			if (atomsCovered > max) 
			{
				spreadOutMonom = curMonom;
				max = atomsCovered;
			}
		}
		
		checkNumTuplesNeededToCover( spreadOutMonom, true );
		return spreadOutMonom;
	}
	
	
	/*************************************************************************************************************/
	/** Title: checkNumTuplesNeededToCover																				
	/** Description: Finds the minimal number of tuples in the monomial needed to cover all the head attributes				 
	/*************************************************************************************************************/	
	
	private static int checkNumTuplesNeededToCover(List<AtomArraySum> maps, boolean forSpreadOut)
	{
		int attrNum = maps.get(0).getAttrCovered().length;
		
		//create attribute array to follow the advance of the covering
		int [] attrCovered = new int [attrNum];
		Arrays.fill(attrCovered, -1);
		
		//sort the objects by sum of the array in descending order
		Collections.sort(maps, Collections.reverseOrder());
		
		for (int j = 0; j < maps.size(); j++) // for all tuples in the monomial
		{
			AtomArraySum obj = maps.get(j);
			for (int i = 0; i < attrNum; i++) // all the range of the array of the tuple 
			{
				if (-1 == attrCovered[i]) // attr. hasn't been covered yet
				{
					attrCovered[i] = obj.getAttrCovered()[i];
					if (forSpreadOut) 
						obj.setInTheCover(true);
				}
			}
			
			if (isAllCovered( attrCovered )) //all attributes are covered
			{
				return ++j; // return the number of atoms 
			}
		}
		
		return -1; // no safe query
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: isAllTrue																				
	/** Description: checks if all values of a boolean array are 'true'				 
	/*************************************************************************************************************/	
	
	private static boolean isAllCovered (int [] arr)
	{
		for (int val : arr)
		{
			if (-1 == val) return false;  
		}
		
		return true;
	}
	
}
