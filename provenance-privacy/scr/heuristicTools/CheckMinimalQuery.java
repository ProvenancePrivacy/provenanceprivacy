package heuristicTools;

import eliminateRedundancies.RemoveAtoms;
import BasicsQBP.Rule;

public class CheckMinimalQuery 
{
	/*************************************************************************************************************/
	/** Title: isQueryMinimal																				
	/** Description: check if the input query is minimal	 
	/*************************************************************************************************************/
	
	public static boolean isQueryMinimal (Rule query)
	{
		if(null == query) 
			return false;
		
		Rule orig = new Rule(query);
		RemoveAtoms.clean(query);
		return query.equals(orig);
	}
}
