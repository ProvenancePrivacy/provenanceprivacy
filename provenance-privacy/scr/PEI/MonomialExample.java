package PEI;

import java.util.ArrayList;
import java.util.List;

import BasicsQBP.Atom;
import BasicsQBP.Rule;

public class MonomialExample 
{	
	//the data-structure needs to contain the same output tuple more than once - Map will not work
	private List<Atom> tuples = new ArrayList<Atom>();
	
	private List<Monomial> monoms = new ArrayList<Monomial>();
	
	public MonomialExample () {}
	

	public MonomialExample (MonomialExample me) 
	{
		for (int i = 0; i < me.rowNumber(); i++) 
		{
			this.addExample(new Atom(me.getOutputTuple(i)), new Monomial(me.getMonomial(i)));
		}
	}
	
	
	public void addExample (Atom output, Monomial explain)
	{
		tuples.add(output);
		monoms.add(explain);
	}
	
	
	public void removeExample (Atom output, Monomial explain)
	{
		tuples.remove(output);
		monoms.remove(explain);
	}
	
	
	public Atom getOutputTuple (int idx)
	{
		return tuples.get(idx);
	}
	
	public void setOutputTupleAt (int idx, Atom tuple)
	{
		tuples.set(idx, tuple);
	}
	
	public Monomial getMonomial (int idx)
	{
		return monoms.get(idx);
	}
	
	public void setMonomialAt (int idx, Monomial m)
	{
		monoms.set(idx, m);
	}
	
	
	public List<Atom> getTuples() {
		return tuples;
	}


	public void setTuples(List<Atom> tuples) {
		this.tuples = tuples;
	}


	public List<Monomial> getMonoms() {
		return monoms;
	}


	public void setMonoms(List<Monomial> monoms) {
		this.monoms = monoms;
	}

	
	public int rowNumber()
	{
		return this.tuples.size();
	}
	
	
	@Override
	public String toString ()
	{
		String retVal = "Output | Provenance\n";
		for (int i = 0; i < this.tuples.size(); i++) 
		{
			retVal += this.tuples.get(i) + " | " + this.monoms.get(i) + "\n";
		}
		
		return retVal.substring(0, retVal.length() - 2);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tuples == null) ? 0 : tuples.hashCode());
		result = prime * result + ((monoms == null) ? 0 : monoms.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonomialExample other = (MonomialExample) obj;
		if (monoms == null) {
			if (other.monoms != null)
				return false;
		} else if (!monoms.equals(other.monoms))
			return false;
		if (tuples == null) {
			if (other.tuples != null)
				return false;
		} else if (!tuples.equals(other.tuples))
			return false;
		return true;
	}
}
