package PEI;

import java.util.HashMap;
import java.util.Map;

import BasicsQBP.Atom;

public class Example 
{
	private Map<Atom, Provenance> pei = new HashMap<Atom, Provenance>();
	
	public Example () {}
	
	
	public void addExample (Atom output, Provenance explain)
	{
		pei.put(output, explain);
	}
	
	
	public void removeExample (Atom output)
	{
		pei.remove(output);
	}


	public Map<Atom, Provenance> getPei() {
		return pei;
	}


	public void setPei(Map<Atom, Provenance> pei) {
		this.pei = pei;
	}
	
	
	@Override
	public String toString ()
	{
		String retVal = "";
		for (Map.Entry<Atom, Provenance> entry : this.pei.entrySet()) 
		{
			retVal += entry.getKey() + " | " + entry.getValue() + "\n";
		}
		
		return retVal.substring(0, retVal.length() - 1);
	}
}
