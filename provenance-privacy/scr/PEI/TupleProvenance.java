package PEI;

import java.util.List;

import BasicsQBP.Atom;

public interface TupleProvenance 
{
	void addTuple(Atom tuple);
	
	void removeTuple(Atom tuple);
	
	void setTuples(List<Atom> tuples);
	
	List<Atom> getTuples();
}
