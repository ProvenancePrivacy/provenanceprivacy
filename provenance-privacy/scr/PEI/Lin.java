package PEI;

import BasicsQBP.Atom;

public class Lin extends Monomial implements Provenance
{
	private final String type = "lin";
	
	public Lin() 
	{
		super();
	}
	
	public Lin(Atom ... inTuples)
	{
		super(inTuples);
	}

	
	public String getType() 
	{
		return type;
	}
	
	
	@Override
	public String toString ()
	{
		String retVal = "{";
		for (Atom tuple : this.tuples) 
		{
			retVal += tuple + ", ";
		}
		
		return retVal.substring(0, retVal.length() - 2) + "}";
	}
}
