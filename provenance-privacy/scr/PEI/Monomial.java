package PEI;

import java.util.ArrayList;
import java.util.List;

import BasicsQBP.Atom;

public class Monomial implements TupleProvenance 
{
	protected List<Atom> tuples;
	
	public Monomial() {}
	
	
	
	public Monomial(Atom ... inTuples)
	{
		this.tuples = new ArrayList<Atom>();
		for (Atom atom : inTuples) 
		{
			this.tuples.add(atom);
		}
	}

	public Monomial(List<Atom> inTuples)
	{
		this.tuples = new ArrayList<Atom>();
		for (Atom atom : inTuples) 
		{
			this.tuples.add( atom );
		}
	}
	
	
	public Monomial(Monomial other)
	{
		this.tuples = new ArrayList<Atom>();
		for (Atom atom : other.getTuples()) 
		{
			this.tuples.add( new Atom(atom) );
		}
	}
	
	

	public List<Atom> getTuples() 
	{
		if (null == this.tuples) 
		{
			this.tuples = new ArrayList<Atom>();
		}
		
		return tuples;
	}


	public void setTuples(List<Atom> tuples) 
	{
		this.tuples = tuples;
	}
	
	
	public void addMonom(Monomial monom) 
	{
		if (null == this.tuples) 
		{
			this.tuples = new ArrayList<Atom>();
		}
		this.tuples.addAll(monom.getTuples());
	}
	
	
	
	public void addTuple(Atom tuple) 
	{
		if (null == this.tuples) 
		{
			this.tuples = new ArrayList<Atom>();
		}
		
		this.tuples.add(tuple);
	}
	
	
	public void removeTuple(Atom tuple) 
	{
		if (null != this.tuples) 
		{
			this.tuples.remove(tuple);
		}
	}
	
	
	public int size()
	{
		return tuples.size();
	}
	
	@Override
	public String toString ()
	{
		String retVal = "";
		for (Atom tuple : this.tuples) 
		{
			retVal += tuple + "*";
		}
		
		return retVal.substring(0, retVal.length() - 1);
	}
	
}
