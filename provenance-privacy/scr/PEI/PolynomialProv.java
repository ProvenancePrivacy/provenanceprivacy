package PEI;

import java.util.ArrayList;
import java.util.List;

import BasicsQBP.Atom;

public class PolynomialProv implements Provenance
{
	private List<Monomial> monoms = new ArrayList<Monomial>();
	
	private String type;
	
	public PolynomialProv (String type, Monomial...monomials)
	{
		this.type = type;
		for (Monomial monomial : monomials) 
		{
			this.monoms.add(monomial);
		}
	}

	public List<Monomial> getMonoms() {
		return monoms;
	}

	public void setMonoms(List<Monomial> monoms) {
		this.monoms = monoms;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
	@Override
	public String toString ()
	{
		String retVal = "";
		for (Monomial monom : this.monoms) 
		{
			retVal += monom + " + ";
		}
		
		return retVal.substring(0, retVal.length() - 3);
	}
}
