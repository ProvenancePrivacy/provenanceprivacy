package algoMinimal;

import BasicsQBP.Atom;
import BasicsQBP.Constant;
import BasicsQBP.Rule;
import PEI.Monomial;
import PEI.MonomialExample;

public class TestClass 
{
	public static void main (String [] args)
	{
		System.out.println(checkSimpleQuerySwap());
	}
	
	
	private static Rule checkSimpleQuerySwap ()
	{
		Atom head_a = new Atom("head", new Constant("a"), new Constant("b"));
		Atom head_b = new Atom("head", new Constant("b"), new Constant("c"));
		Atom head_c = new Atom("head", new Constant("g"), new Constant("h"));
		Atom atom_a = new Atom("route", new Constant("a"), new Constant("b"));
		Atom atom_b = new Atom("route", new Constant("b"), new Constant("c"));
		Atom atom_c = new Atom("route", new Constant("b"), new Constant("d"));
		Atom atom_d = new Atom("route", new Constant("c"), new Constant("d"));
		
		Atom atom_e = new Atom("route", new Constant("a"), new Constant("b"));
		Atom atom_f = new Atom("route", new Constant("b"), new Constant("d"));
		Atom atom_g = new Atom("route", new Constant("b"), new Constant("d"));
		Atom atom_h = new Atom("route", new Constant("c"), new Constant("d"));
		
		MonomialExample me  = new MonomialExample ();
		
		me.addExample(head_a, new Monomial(atom_a, atom_c));
		me.addExample(head_b, new Monomial(atom_b, atom_d));
		
		/*
		me.addExample(head_a, new Monomial(atom_a, atom_c, atom_e));
		me.addExample(head_b, new Monomial(atom_b, atom_d, atom_f));
		*/
		//me.addExample(head_c, new Monomial(atom_e, atom_f));
		return Unification.findMinimalQueryForSetting(me);
	}
}
