package algoMinimal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import BasicsQBP.Atom;
import BasicsQBP.Body;
import BasicsQBP.Proton;
import BasicsQBP.Rule;
import BasicsQBP.Var;
import PEI.MonomialExample;

public class Unification 
{
	
	/*************************************************************************************************************/
	/** Title: unifyTwoRows																				
	/** Description: unifies two rows where the first one is the accumulator of the second one. Done for two atoms	 
	/*************************************************************************************************************/
	
	public static void unifyTwoRows(List<List<Proton>> aList, List<Proton> bList)
	{
		for (int i = 0; i < aList.size(); i++) 
		{
			aList.get(i).add( bList.get(i) );
		}
	}
	
	
	/*************************************************************************************************************/
	/** Title: checkSafeQuery																				
	/** Description: checks that the query is safe at each level				 
	/*************************************************************************************************************/
	
	private static boolean checkSafeQuery(List<List<Proton>> headList, List<List<List<Proton>>> atomList)
	{
		boolean vectorFound;
		for (List<Proton> list : headList) //for every head vector
		{
			vectorFound = false;
			for (List<List<Proton>> atomLst : atomList) //check if there is an atom that contains this vector in the body 
			{
				if (true == atomLst.contains(list)) 
				{
					vectorFound = true;
					break;
				}
			}
			if (false == vectorFound) 
			{
				return false;
			}
		}
		
		return true;
	}
	
	/*************************************************************************************************************/
	/** Title: allEqualList																				
	/** Description: Check if all protons in list are equal				 
	/*************************************************************************************************************/
	
	private static boolean allEqualList (List<Proton> list)
	{
		Proton temp = list.get(0);
		for (Proton proton : list) 
		{
			if (false == proton.equals(temp)) 
				return false;
		}
		
		return true;
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: swapInAtom																				
	/** Description: swaps a list of constants in the atom with a variable/constant				 
	/*************************************************************************************************************/
	
	private static boolean swapInAtom(List<Proton> list, List<List<Proton>> atomList, Atom atom, char varName,
			Map<List<Proton>, Proton> vectorsToVars, int i)
	{
		boolean usedVarName = false;
		/*if (atomList.contains(list)) 
		{*/
		if (vectorsToVars.containsKey(list)) 
		{
			atom.getParams().set(i, vectorsToVars.get(list));
		}

		else if (allEqualList(list)) 
		{
			atom.getParams().set(i, list.get(0));
			vectorsToVars.put(list, list.get(0));
		}

		else
		{
			Var v = new Var(String.valueOf(varName));
			atom.getParams().set(atomList.indexOf(list), v);
			vectorsToVars.put(list, v);
			usedVarName = true;
		}
		//}
		return usedVarName;
	}
	
	
	/*************************************************************************************************************/
	/** Title: swapInQuery																				
	/** Description: swaps a list of constants in the query with a variable/constant				 
	/*************************************************************************************************************/
	
	private static void swapInQuery(List<List<List<Proton>>> atomList, Rule query)
	{
		char varName = 'a';
		boolean usedVarName;
		Map<List<Proton>, Proton> vectorsToVars = new HashMap<List<Proton>, Proton>();
		for (int i = 0; i < atomList.get(0).size(); i++) //the arity of the head of the query 
		{
			swapInAtom(atomList.get(0).get(i), atomList.get(0), query.getHead(), varName, vectorsToVars, i);
			varName = (char) (varName+1);
		}
		
		for (int i = 1; i < atomList.size(); i++) 
		{
			Atom curAtom = query.getBody().getAtoms().get(i - 1);
			for (int j = 0; j < atomList.get(i).size(); j++) 
			{
				//Get the j'th var of the i'th atom and compare to the rest of the query
				usedVarName = swapInAtom(atomList.get(i).get(j), atomList.get(i), curAtom, varName, vectorsToVars, j);
				if (usedVarName)
					varName = (char) (varName+1);
				if (varName > 122)
					varName = 'A';
			}
		}
	}	
	
	
	/*************************************************************************************************************/
	/** Title: GenParamList																				
	/** Description: Adds the initial params list of the atom to retVal 				 
	/*************************************************************************************************************/
	
	private static void GenParamList(Atom atm, List<List<List<Proton>>> retVal)
	{
		List<List<Proton>> tmp = new ArrayList<List<Proton>>();
		for (Proton prt : atm.getParams()) 
		{
			List<Proton> params = new ArrayList<Proton>();
			params.add( prt );
			tmp.add( params );
		}

		retVal.add( tmp );
	}
	
	
	/*************************************************************************************************************/
	/** Title: findMinimalQueryForSetting																				
	/** Description: swaps a list of constants in the query with a variable/constant				 
	/*************************************************************************************************************/
	
	public static Rule findMinimalQueryForSetting (MonomialExample ex)
	{
		boolean safe;
		Atom frstAtom = ex.getTuples().get(0);
		Body monom = new Body(ex.getMonoms().get(0).getTuples());
		Rule query = new Rule(frstAtom, monom);
		ex.removeExample(frstAtom, ex.getMonoms().get(0));
		
		//make first list of lists
		List<List<List<Proton>>> frstLst = new ArrayList<List<List<Proton>>>();
		GenParamList(frstAtom, frstLst);
		for (Atom atm : monom.getAtoms()) 
		{
			GenParamList(atm, frstLst);
		}
		
		for (Atom atm : ex.getTuples()) // for each row of output tuples 
		{
			unifyTwoRows(frstLst.get(0), atm.getParams());
		}
		
		for (int i = 0; i < monom.getAtoms().size(); i++) //for each column of tuples 
		{	
			for (Atom atm : ex.getTuples()) // for each row 
			{
				Atom relevant = ex.getMonoms().get(ex.getTuples().indexOf(atm)).getTuples().get(i);
				unifyTwoRows(frstLst.get(i+1), relevant.getParams());
			}
		}
		safe = checkSafeQuery(frstLst.get(0), frstLst.subList(1, frstLst.size()));
		if (false == safe) 
		{
			System.out.println("Unification::findMinimalQuery: The setting is not good - unsafe query");
			System.exit(0);
		}
		
		swapInQuery(frstLst, query);
		return query;
	}
	
}
