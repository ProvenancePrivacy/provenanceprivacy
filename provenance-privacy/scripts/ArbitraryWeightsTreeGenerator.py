import io, random, sys

input_file = sys.argv[1]
output_file = sys.argv[2]

ignore = ["ans", "part", "supplier"]
was = []

with open(input_file) as fp:
	line = fp.readline()
	while line and line.split("(")[0] in ignore:
		line = fp.readline()
		
	with open(output_file, "w") as myfile:
		myfile.write("star->" + str(random.randint(30, 50)) + "->" + line.split("(")[0] + "\n")
	was.append(line.split("(")[0])
	
	while line:
		if line.split("(")[0] not in was:
			with open(output_file, "a") as myfile:
				myfile.write("star->" + str(random.randint(30, 50)) + "->" + line.split("(")[0] + "\n")
			was.append(line.split("(")[0])
			
		with open(output_file, "a") as myfile:
			myfile.write(line.split("(")[0] + "->" + str(random.randint(2, 20)) + "->" + line[:-2] + "\n")
			
		line = fp.readline()
		while line and line.split("(")[0] in ignore:
			line = fp.readline()
		
print("done")