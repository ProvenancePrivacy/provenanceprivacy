import io
import random
import sys
import os

# tree levels size
levels_size = 3

# input arguments
input_file = sys.argv[1]

output_file = sys.argv[2]

tree_level = int(sys.argv[3])

parents = {}
for key in sys.argv[4].split(","):
    parents[key] = []

tree_size_limit = int(sys.argv[5])

example_file = sys.argv[6]


def add_example_atoms():
    tree_data = open(output_file, "r").read()
    ex_data = open(example_file, "r").read()
    for name in parents.keys():
        if not name in ex_data:
            continue
        if not name in tree_data:
            continue

        isFirst = True
        parts = ex_data.split(name)
        for part in parts:
            if isFirst:
                isFirst = False
                continue

            value = name + part
            if "|" in value:
                value = value.split("|")[0]
            if "\n" in value:
                value = value.split("\n")[0]

            if value in tree_data:
                continue

            # add the leaf edge
            rnd_index = random.randint(0, len(parents[name]) - 1)
            add_edge(parents[name][rnd_index], value, random.randint(2, 15))


def add_edge(s, d, w):
    with open(output_file, "a") as myfile:
        myfile.write(str(s) + "->" + str(w) + "->" + str(d) + "\n")


def add_internal_edges(table, old_name, index):
    if 0 == index:
        parents[table].append(old_name)
        return

    for i in range(1, levels_size + 1):
        cur_name = old_name + "_" + str(i)
        if (index == tree_level - 1):
            add_edge("star", cur_name, random.randint(40, 50))
        else:
            add_edge(old_name, cur_name, random.randint(20, 40))
        add_internal_edges(table, cur_name, index - 1)


def main():
    key_was = []
    rows_added = 0

    if os.path.exists(output_file):
        os.remove(output_file)

    db_rows = open(input_file, "r").read().split("\n")
    random.shuffle(db_rows)

    for db_row in db_rows:
        if rows_added >= tree_size_limit:
            break

        name = db_row.split("(")[0]
        if not name in parents.keys():
            continue

        # if first time key appears, add the internal edges of it
        if not name in key_was:
            add_internal_edges(name, name, tree_level - 1)
            key_was.append(name)

        # add the leaf edge
        rnd_index = random.randint(0, len(parents[name]) - 1)
        add_edge(parents[name][rnd_index],
                 db_row[:-1], random.randint(2, 15))

        rows_added += 1

    add_example_atoms()


main()
print("done")
