/*
 * Integrated Rule Inference System (IRIS):
 * An extensible rule inference system for datalog with extensions.
 * 
 * Copyright (C) 2008 Semantic Technology Institute (STI) Innsbruck, 
 * University of Innsbruck, Technikerstrasse 21a, 6020 Innsbruck, Austria.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
 * MA  02110-1301, USA.
 */
package org.deri.iris.rules.compiler;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.deri.iris.ConfigurationWithTopK;
import org.deri.iris.EvaluationException;
import org.deri.iris.RuleUnsafeException;
import org.deri.iris.api.basics.ITuple;
import org.deri.iris.api.terms.IStringTerm;
import org.deri.iris.api.terms.ITerm;
import org.deri.iris.api.terms.IVariable;
import org.deri.iris.factory.Factory;
import org.deri.iris.storage.IRelation;
import org.deri.iris.terms.concrete.IntegerTerm;
import org.deri.iris.utils.TermMatchingAndSubstitution;

import Top1.DerivationTree2;
import Top1.EquationTopK2;
import TopKBasics.KeyMap2;

/**
 * A compiled rule element representing the substitution of variable bindings in to the rule head.
 */
public class HeadSubstituter extends RuleElement
{
	/**
	 * Constructor.
	 * @param variables The variables from the rule body.
	 * @param headTuple The tuple from the rule head.
	 * @throws EvaluationException If unbound variables occur.
	 */
	public HeadSubstituter( List<IVariable> variables, ITuple headTuple, ConfigurationWithTopK configuration ) throws EvaluationException
	{
		assert variables != null;
		assert headTuple != null;
		assert configuration != null;
		
		mConfiguration = configuration;
		
		mHeadTuple = headTuple;

		// Work out the indices of variables in substitution order
		List<IVariable> variablesToSubstitute = TermMatchingAndSubstitution.getVariables( mHeadTuple, false );
		mIndices = new int[ variablesToSubstitute.size() ];
		
		int i = 0;
		for( IVariable variable : variablesToSubstitute )
		{
			int index = variables.indexOf( variable );
			if( index < 0 )
				throw new RuleUnsafeException( "Unbound variable in rule head: " + variable );
			mIndices[ i++ ] = index;
		}
	}
	
	@Override
	public IRelation process(CompiledRuleWithTopK r, IRelation inputRelation, Set<DerivationTree2> treesWaiting )
	{
		assert inputRelation != null;

		IRelation result = mConfiguration.relationFactory.createRelation();
		
		int [][] indices = new int[r.getElements().size() - 2][];
		for( int i = 0; i < r.getElements().size() - 2; ++i )
		{
			RuleElement element = r.getElements().get( i );
			indices[i] = element.getIndices();
		}

		for( int i = 0; i < inputRelation.size(); ++i )
		{
			ITuple inputTuple = inputRelation.get( i );
			
			ITuple outputTuple = TermMatchingAndSubstitution.substituteVariablesInToTuple( mHeadTuple, inputTuple, mIndices );
			
			result.add( outputTuple );
			
			//Amir added
			ITuple headTuple = getHead(r, outputTuple);
			if (false == headTuple.isTop1Found()) 
			{
				getAtomBody(r, inputTuple, indices, treesWaiting, headTuple);
			}
		}
		
		return result;
	}



	@Override
	public IRelation process(IRelation inputRelation )
	{
		assert inputRelation != null;

		IRelation result = mConfiguration.relationFactory.createRelation();

		for( int i = 0; i < inputRelation.size(); ++i )
		{
			ITuple inputTuple = inputRelation.get( i );

			ITuple outputTuple = TermMatchingAndSubstitution.substituteVariablesInToTuple( mHeadTuple, inputTuple, mIndices );

			result.add( outputTuple );
		}

		return result;
	}


	/*************************************************************************************************************/
	/** Title: getHead																					
	/** Description: Get the head of the rule - the fact that is derived. 									
	/*************************************************************************************************************/
	
	public ITuple getHead (CompiledRuleWithTopK r, ITuple tuple)
	{
		ITuple head;
		String name = r.getPredicate().getPredicateSymbol();
		boolean added = KeyMap2.getInstance().StringUpdate( name, tuple );
		
		if ( false == added )
		{
			head = KeyMap2.getInstance().Get(name, tuple);
		}
		
		else
		{
			head = tuple;
		}
		
		head.setCurRuleWeight(r.getWeight());
		head.setPredicate(name);
		return head;
	}
	
	
	/*************************************************************************************************************/
	/** Title: getAtomBody																					
	/** Description: Get the body of the rule and find the top-1 tree candidate for this fact. 									
	/*************************************************************************************************************/
	
	public void getAtomBody (CompiledRuleWithTopK r, ITuple tuple, int [][] indices, Set<DerivationTree2> treesWaiting, ITuple head)
	{
		List<ITuple> body = new ArrayList<ITuple>();
		boolean added = false;

		for( int i = 0; i < r.getElements().size() - 2; ++i )
		{
			RuleElement element = r.getElements().get( i );
			
			if (element instanceof Builtin) 
			{
				continue;
			}

			String name = element.getPredicate().getPredicateSymbol();//.intern();
			ITuple relevantTuple = TupleByIndices(indices[i], tuple, element);
			relevantTuple.setPredicate(name);
			added = KeyMap2.getInstance().StringUpdate(name, relevantTuple);

			//this holds when the atom is already in the keymap or when relevantTuple is the same tuple as head, i.e. a fact derived from itself
			if ( false == added ) //&& relevantTuple.getTree() != null
			{
				relevantTuple = KeyMap2.getInstance().Get(name, relevantTuple);

				if (relevantTuple.equals(head))
				{
					relevantTuple.setFact(true);
				}

				body.add( relevantTuple );

				body.add( KeyMap2.getInstance().Get(name, relevantTuple) );
			}
			
			else //can only happen for facts in the first iteration
			{
				relevantTuple.setFact(true);
				body.add(relevantTuple);
			}
		}
		
		HandleTop1Scenario(head, body, r, treesWaiting);
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: HandleTop1Scenario																					
	/** Description: Uses UpdateTop1WhileSemiNaive method to find the candidate for the top-1 tree and sets it as
	/**  this fact's tree. 									
	/*************************************************************************************************************/
	
	public void HandleTop1Scenario (ITuple ihead, List<ITuple> ibody, CompiledRuleWithTopK r, Set<DerivationTree2> treesWaiting)
	{
		EquationTopK2.UpdateTop1WhileSemiNaive(ihead, ibody);
		ihead.getTree().setRulePointer(r);
//		treesWaiting.add( ihead.getTree() );
	}
	
	
	
	/*************************************************************************************************************/
	/** Title: TupleByIndices																					
	/** Description: Finds the tuple that fits the rule element. Divides it into two cases: 
	/** 1. having a const. value in the tuple (happens when using a pattern) 
	/** 2. there is no const. value in the tuple.
	/*************************************************************************************************************/
	
	public ITuple TupleByIndices (int [] indices, ITuple tuple, RuleElement element)
	{
		ITerm[] terms = new ITerm[element.getView().size()];
		int index = 0;
		
		if (indices.length < element.getView().size()) //there is a const value in the tuple
		{
			for (int i = 0; i < element.getView().size(); ++i) 
			{
				ITerm term = element.getView().get(i);
				if (term instanceof IStringTerm)
				{
					terms[i] = term;//index++
				}
				//ADDED FOR EXPLAIN
				else if (term instanceof IntegerTerm)
				{
					terms[i] = term;
				}
			}
			
			//index = 0;
//			int j = 0;
			for (int i = 0; i < terms.length; ++i)
			{
				if (null == terms[i])
				{
					terms[i] = tuple.get(indices[0]);
//					terms[i] = indices.length > j ? tuple.get(indices[j]) : tuple.get(indices[0]);//change indices[0] to indices[i]. Not sure..
//					j++;
				}
//				else
//				{
//					j = i;
//				}
			}
		}
		
		else
		{
			for (int i : indices) 
			{
				terms[index++] = tuple.get(i);
			}
		}

		return Factory.BASIC.createTuple(terms);
	}
	
	
	
	//Amir added
	/*public String StringTupleByIndices (int [] indices, ITuple tuple)
	{
		String retVal= "(";
		for (int i : indices) 
		{
			retVal += tuple.get(i).toString().intern() + ",";
		}
		
		retVal = retVal.substring(0, retVal.length() - 1) + ")";
		return retVal.intern();
	}*/
	/*
	public List<ITuple> getHeads ()
	{
		return heads;
	}
	
	public List<List<ITuple>> getBodies()
	{
		return bodies;
	}
	
	public void clearHeads ()
	{
		if (heads != null) 
		{
			heads.clear();
		}
	}
	*/
	//Amir added
	public int[] getIndices() 
	{
		return mIndices;
	}
	
	
	public ITuple getView()
	{
		return mHeadTuple;
	}

	/** The rule head tuple. */
	protected final ITuple mHeadTuple;
	
	/** The indices of variables in substitution order. */
	protected final int[] mIndices;
	
	/** The knowledge-base's configuration object. */
	protected final ConfigurationWithTopK mConfiguration;
	
	
	//private List<ITuple> heads;
	//private List<List<ITuple>> bodies;
}
