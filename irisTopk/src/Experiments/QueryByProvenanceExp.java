package Experiments;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.deri.iris.ConfigurationWithTopK;
import org.deri.iris.EvaluationException;
import org.deri.iris.api.basics.IPredicate;
import org.deri.iris.api.basics.IRule;
import org.deri.iris.api.basics.ITuple;
import org.deri.iris.compiler.Parser;
import org.deri.iris.compiler.ParserException;
import org.deri.iris.evaluation.stratifiedbottomup.seminaive.SemiNaiveEvaluatorWithTopK;
import org.deri.iris.facts.FactsWithTopK;
import org.deri.iris.facts.IFacts;
import org.deri.iris.optimisations.magicsets.MagicSets;
import org.deri.iris.optimisations.rulefilter.RuleFilter;
import org.deri.iris.rules.compiler.ICompiledRule;
import org.deri.iris.rules.compiler.RuleCompilerWithTopK;
import org.deri.iris.storage.IRelation;

import TopKBasics.KeyMap2;

public class QueryByProvenanceExp 
{
	
	public static void main (String [] args)
	{
		int qNum = 6;
		int rowNum = 2;
		boolean isLaptop = true;//configure path for files
		int provKind = 0; //0 for N(x), 1 for Trio(x), 2 for Lin(x)
		runIRIS_prov(isLaptop, rowNum, provKind,qNum);
	}
	
	
	public static void run (boolean isLaptop, int provKind, int rowNum, int qNum)//
	{
		runIRIS_prov(isLaptop, rowNum, provKind, qNum);
	}
	
	
	/*************************************************************************************************************/
	/** Title: runIRIS_Prov																				
	/** Description: takes the IRIS file and runs an evaluation of the query on the DB using the top-k mechanism				 
	/*************************************************************************************************************/	
	
	private static void runIRIS_prov (boolean isLaptop, int rowNum, int provKind, int qNum)
	{
		File program;
		KeyMap2.getInstance().Reset();
		String path = "C:\\Temp\\"; 
		deleteFile(path + "OriginalExample.txt");
		deleteFile(path + "ProvenanceFacts.iris");
		program = new File(path + "TPC-H_DB" + ".iris");// Create a Reader on the Datalog program file.
		//program = new File(path + "demoDB.iris");
		Reader reader;
		try 
		{
			reader = new FileReader(program);
			// Parse the Datalog program.
			Parser parser = new Parser();
			parser.parse(reader);

			// Retrieve the facts, rules and queries from the parsed program.
			Map<IPredicate, IRelation> factMap = parser.getFacts();
			List<IRule> rules = parser.getRules();

			// Create a default configuration.
			ConfigurationWithTopK configuration = new ConfigurationWithTopK();

			// Enable Magic Sets together with rule filtering.
			configuration.programOptmimisers.add(new RuleFilter());
			configuration.programOptmimisers.add(new MagicSets());

			// Convert the map from predicate to relation to a IFacts object.
			FactsWithTopK facts = new FactsWithTopK(factMap, configuration.relationFactory);

			int initialSize = 0;
			for( IPredicate predicate : facts.getPredicates() )
				initialSize += facts.get( predicate ).size();
			
			// Evaluate all queries over the knowledge base.
			List<ICompiledRule> cr = compile(rules, facts, configuration);
			SemiNaiveEvaluatorWithTopK sn = new SemiNaiveEvaluatorWithTopK();
			sn.evaluateRules(cr, facts, configuration);
			/*TopKFinder topk = new TopKFinder(cr, 3);
			Map<ITuple, List<DerivationTree2>> trees = topk.Topk("ans");*/
			
			int endSize = 0;
			for( IPredicate predicate : facts.getPredicates() )
			{
				endSize += facts.get( predicate ).size();
			}
			
			System.out.println("number of facts derived: " + (endSize - initialSize));
			
			try
			{
				boolean chooseRow;
				Random r = new Random();
				int rowsWritten = 0;
				Writer writer = new BufferedWriter(new FileWriter(path + "OriginalExample.txt", true));//append to file
				Writer writerForRecall = new BufferedWriter(new FileWriter(path + "ProvenanceFacts.iris", true));//append to file
				
				for( Map.Entry<IPredicate, IRelation> entry : facts.getPredicateRelationMap().entrySet() )
				{
					IRelation relation = entry.getValue();

					for( int t = 0; t < relation.size(); ++t )
					{
						chooseRow = (r.nextDouble() > 0.4) ? true : false;//true;
						
						ITuple tuple = relation.get( t );
						if (tuple.getTree() != null)
						{
							if (chooseRow && rowsWritten < rowNum)
							{
								//create example for learning
								if (0 == provKind)//write N(x) provenance
								{
									writer.write(tuple.getTree() + "\n"); 
								}
								else if (1 == provKind)//write trio(x) provenance
								{
									writer.write(tuple.getTree().toStringProvTRIO() + "\n");
								}
								else//write lin(x) provenance
								{
									Set<String> derivationFacts = new HashSet<>();
									/*for (DerivationTree2 tree : trees.get(tuple)) //collect all facts used in some derivation in a set to remove duplicates
									{
										derivationFacts.addAll(tree.toStringSetLIN());
									}*/									
									
									writer.write(tuple.getPredicate() + tuple);
									for (String strFact : derivationFacts) //write the set of facts
									{
										writer.write("|" + strFact);
									}
									
									writer.write("\n");
								}
								
								// create database for checking precision/recall
								// every line correlates to a provenance row
								writerForRecall.write(tuple.getTree().toStringProv() + "\n");
								rowsWritten++;
							}
						}
					}
				}
				
				System.out.println("wrote " + rowsWritten + " rows");
				writer.close();
				writerForRecall.close();
			}
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		} 
		catch (FileNotFoundException | EvaluationException | ParserException e) 
		{
			e.printStackTrace();
		}
	}
	
	
	/*************************************************************************************************************/
	/** Title: runIRIS_NoProv																				
	/** Description: takes the IRIS file and runs an evaluation of the query on the DB				 
	/*************************************************************************************************************/	
	
	public static FactsWithTopK runIRIS_noProv(String query, String path, boolean isFirstRun)
	{
		try 
		{
			if (false == path.contains("2")) copyFileWithoutQuery(path, query, isFirstRun);
			wrtieQueryToFile(query, path);

			// Create a Reader on the Datalog program file.
			File program = new File(path);
			Reader reader = new FileReader(program);

			// Parse the Datalog program.
			Parser parser = new Parser();
			parser.parse(reader);

			// Retrieve the facts, rules and queries from the parsed program.
			Map<IPredicate, IRelation> factMap = parser.getFacts();
			List<IRule> rules = parser.getRules();

			// Create a default configuration.
			ConfigurationWithTopK configuration = new ConfigurationWithTopK();

			// Enable Magic Sets together with rule filtering.
			configuration.programOptmimisers.add(new RuleFilter());
			configuration.programOptmimisers.add(new MagicSets());

			// Convert the map from predicate to relation to a IFacts object.
			FactsWithTopK facts = new FactsWithTopK(factMap, configuration.relationFactory);

			int initialSize = 0;
			for( IPredicate predicate : facts.getPredicates() )
				initialSize += facts.get( predicate ).size();

			List<ICompiledRule> cr = compile(rules, facts, configuration);
			SemiNaiveEvaluatorWithTopK sn = new SemiNaiveEvaluatorWithTopK();
			sn.evaluateRules2(cr, facts, configuration);

			int endSize = 0;
			for( IPredicate predicate : facts.getPredicates() )
				endSize += facts.get( predicate ).size();

			System.out.println("added facts " + (endSize - initialSize));
			
			return facts;
		}
		
		catch (FileNotFoundException | EvaluationException | ParserException e) 
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	/*************************************************************************************************************/
	/** Title: wrtieQueryToFile																				
	/** Description: writes the formulated query to the IRIS file				 
	/*************************************************************************************************************/	
	
	private static void wrtieQueryToFile (String query, String path)
	{
		Writer writer = null;

		try 
		{
		    writer = new BufferedWriter(new FileWriter(path, true));
		    writer.write("\n" + query);
		    
		} 
		catch (IOException ex) 
		{
			System.out.println("IrisOperation::wrtieQueryToFile: ERROR in writing query " + query + " to file");
		} 
		finally 
		{
		   try {writer.close();} catch (Exception ex) {}
		}
	}
	
	
	/*************************************************************************************************************/
	/** Title: deleteQueryFromFile																				
	/** Description: deletes the query string from file				 
	/*************************************************************************************************************/	
	
	private static void copyFileWithoutQuery (String path, String query, boolean isFirstRun)
	{
		File inputFile = new File(path);
		String filename = (isFirstRun) ? "ProvenanceFacts2.iris" : "TPC-H_DB2.iris";
		File tempFile = new File(inputFile.getParent() + "\\" + filename);

		try 
		{
			BufferedReader reader = new BufferedReader(new FileReader(inputFile));
			BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

			String lineToRemove = query;
			String currentLine;

			while((currentLine = reader.readLine()) != null) 
			{
				// trim newline when comparing with lineToRemove
				String trimmedLine = currentLine.trim();
				if(trimmedLine.equals(lineToRemove)) 
					continue;
				writer.write(currentLine + System.getProperty("line.separator"));
			}
			
			writer.close(); 
			reader.close();
			/*if ( false == tempFile.renameTo(inputFile) )
				System.out.println("IrisOperation::deleteQueryFromFile: did not delete query from file.");*/
		}
		
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	
	/*************************************************************************************************************/
	/** Title: compileTopK
	/** Description: compiles the rules of IRIS				 
	/*************************************************************************************************************/	
	
	private static List<ICompiledRule> compile( List<IRule> rules, IFacts facts, ConfigurationWithTopK mConfiguration ) throws EvaluationException
	{
		assert rules != null;
		assert facts != null;
		assert mConfiguration != null;
		
		List<ICompiledRule> compiledRules = new ArrayList<ICompiledRule>();
		
		RuleCompilerWithTopK rc = new RuleCompilerWithTopK( facts, mConfiguration.equivalentTermsFactory.createEquivalentTerms(), mConfiguration );

		for( IRule rule : rules )
			compiledRules.add( rc.compileTopK( rule ) );
		
		return compiledRules;
	}
	
	
	/*************************************************************************************************************/
	/** Title: deleteFile																				
	/** Description: delete the DB file if it exists				 
	/*************************************************************************************************************/	
	
	private static void deleteFile( String path )
	{
		File file = new File( path );
		
		if(file.delete())
		{
			System.out.println(file.getName() + " is deleted");
		}
		else
		{
			System.out.println("Delete operation failed");
		}
	}
}
