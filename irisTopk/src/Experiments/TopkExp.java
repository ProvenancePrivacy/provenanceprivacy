package Experiments;

import Basics.Atom;
import Basics.Proton;
import Pattern.PatternNode;
import Top1.DerivationTree2;
import TopK.TopKFinder;
import TopKBasics.KeyMap2;
import org.deri.iris.ConfigurationWithTopK;
import org.deri.iris.EvaluationException;
import org.deri.iris.api.basics.IPredicate;
import org.deri.iris.api.basics.IRule;
import org.deri.iris.api.basics.ITuple;
import org.deri.iris.basics.Query;
import org.deri.iris.compiler.Parser;
import org.deri.iris.compiler.ParserException;
import org.deri.iris.evaluation.stratifiedbottomup.seminaive.SemiNaiveEvaluatorWithTopK;
import org.deri.iris.facts.FactsWithTopK;
import org.deri.iris.facts.IFacts;
import org.deri.iris.optimisations.magicsets.MagicSets;
import org.deri.iris.optimisations.rulefilter.RuleFilter;
import org.deri.iris.querycontainment.QueryContainment;
import org.deri.iris.rules.compiler.ICompiledRule;
import org.deri.iris.rules.compiler.RuleCompilerWithTopK;
import org.deri.iris.storage.IRelation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by user on 9/20/2017.
 */
public class TopkExp {

    public static String [] runTopK(String progPath, PatternNode patternRoot, int k, boolean negation) {
        File program = new File(progPath);
        Reader reader = null;
        try {
            reader = new FileReader(program);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // Parse the Datalog program.
        Parser parser = new Parser();
        try {
            parser.parse(reader);
        } catch (ParserException e) {
            e.printStackTrace();
        }

        // Retrieve the facts, rules and queries from the parsed program.
        Map<IPredicate, IRelation> factMap = parser.getFacts();
        List<IRule> rules = parser.getRules();

        // Create a default configuration.
        ConfigurationWithTopK configuration = new ConfigurationWithTopK();

        // Enable Magic Sets together with rule filtering.
        configuration.programOptmimisers.add(new RuleFilter());
        configuration.programOptmimisers.add(new MagicSets());

        // Convert the map from predicate to relation to a IFacts object.
        IFacts facts = new FactsWithTopK(factMap, configuration.relationFactory);

        int initialSize = 0;
        for( IPredicate predicate : facts.getPredicates() )
            initialSize += facts.get( predicate ).size();

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        // Evaluate all queries over the knowledge base.
        long startTime = System.currentTimeMillis();
        List<ICompiledRule> cr = compile(rules, facts, configuration);
        SemiNaiveEvaluatorWithTopK sn = new SemiNaiveEvaluatorWithTopK();
        try {
            sn.evaluateRules(cr, facts, configuration);
        } catch (EvaluationException e) {
            e.printStackTrace();
        }
        long endTime = System.currentTimeMillis();
        long durationSemiNaive = (endTime - startTime);

        startTime = System.currentTimeMillis();
        TopKFinder topk = new TopKFinder(cr, k);

//        topk.Topk("dealsWith_p_1", "('Slovakia', 'Nicaragua')");
        String relation = patternRoot.getName() + patternRoot.getNewName();
        if (true == patternRoot.isFullyInst() && negation == false) {
            topk.Topk(relation, convertParamsToFormat(patternRoot.getParams()) );
        }
        else if (true == negation) {
            topk.Topk(patternRoot.getNewName(), true);
        }
        else {
            topk.Topk(relation, false);
        }

        endTime = System.currentTimeMillis();
        long durationTopK = (endTime - startTime);
        long memUsed = ( KeyMap2.getInstance().Memory() );


        int endSize = 0;
        for( IPredicate predicate : facts.getPredicates() )
        {
            endSize += facts.get( predicate ).size();
        }

        String [] resultArr = new String[]{Long.toString(durationSemiNaive + durationTopK),
//                Long.toString(durationTopK),
                Long.toString(initialSize), Long.toString(endSize), Long.toString(memUsed)};
        // time for SN, time for SN, size of initial DB, size of final DB, memory

        return resultArr;
    }


    public static Long [] runTop1Diverse(String progPath, String patternRoot, ArrayList<DerivationTree2> treeArr) {
        KeyMap2.getInstance().Reset(); // added to prevent trees from previous iterations to be stored
        File program = new File(progPath);
        Reader reader = null;
        try {
            reader = new FileReader(program);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // Parse the Datalog program.
        Parser parser = new Parser();
        try {
            parser.parse(reader);
        } catch (ParserException e) {
            e.printStackTrace();
        }

        // Retrieve the facts, rules and queries from the parsed program.
        Map<IPredicate, IRelation> factMap = parser.getFacts();
        List<IRule> rules = parser.getRules();

        // Create a default configuration.
        ConfigurationWithTopK configuration = new ConfigurationWithTopK();

        // Enable Magic Sets together with rule filtering.
        configuration.programOptmimisers.add(new RuleFilter());
        configuration.programOptmimisers.add(new MagicSets());

        // Convert the map from predicate to relation to a IFacts object.
        IFacts facts = new FactsWithTopK(factMap, configuration.relationFactory);

        int initialSize = 0;
        for( IPredicate predicate : facts.getPredicates() )
            initialSize += facts.get( predicate ).size();

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        // Evaluate all queries over the knowledge base.
        long startTime = System.currentTimeMillis();
        List<ICompiledRule> cr = compile(rules, facts, configuration);
        SemiNaiveEvaluatorWithTopK sn = new SemiNaiveEvaluatorWithTopK();
        try {
            sn.evaluateRules(cr, facts, configuration);
        } catch (EvaluationException e) {
            e.printStackTrace();
        }
        long endTime = System.currentTimeMillis();
        long durationSemiNaive = (endTime - startTime);

        startTime = System.currentTimeMillis();
        TopKFinder topk = new TopKFinder(cr, 1);

        //get the top-1 tree
        Map<ITuple, List<DerivationTree2>> allTopOne = topk.Topk(patternRoot, true);
        ITuple rootTuple = allTopOne.keySet().iterator().next();
        DerivationTree2 top1 = allTopOne.get(rootTuple).get(0);
        treeArr.add(top1);
//        }

        endTime = System.currentTimeMillis();
        long durationTopK = (endTime - startTime);
        long memUsed = ( KeyMap2.getInstance().Memory() );


        int endSize = 0;
        for( IPredicate predicate : facts.getPredicates() )
        {
            endSize += facts.get( predicate ).size();
        }

        Long [] resultArr = new Long []{(durationSemiNaive + durationTopK),
                Integer.toUnsignedLong(initialSize), Integer.toUnsignedLong(endSize), memUsed};
        // time for SN, time for SN, size of initial DB, size of final DB, memory
        KeyMap2.getInstance().Reset(); // added to prevent trees from previous iterations to be stored
        return resultArr;
    }


    public static String convertParamsToFormat(Vector<Proton> params)
    {
        //['Slovakia', 'Nicaragua'] -> ('Slovakia', 'Nicaragua')
        String retVal = "(";
        for (Proton p : params)
        {
            retVal += "'" + p.getName() + "', ";
        }

        retVal = retVal.substring(0, retVal.length() - 2) + ")";
        return retVal;
    }


    private static List<ICompiledRule> compile( List<IRule> rules, IFacts facts, ConfigurationWithTopK mConfiguration )
    {
        assert rules != null;
        assert facts != null;
        assert mConfiguration != null;

        List<ICompiledRule> compiledRules = new ArrayList<ICompiledRule>();

        RuleCompilerWithTopK rc = new RuleCompilerWithTopK( facts, mConfiguration.equivalentTermsFactory.createEquivalentTerms(), mConfiguration );

        for( IRule rule : rules )
            try {
                compiledRules.add( rc.compileTopK( rule ) );
            } catch (EvaluationException e) {
                e.printStackTrace();
            }

        return compiledRules;
    }


    private static void removeContainedRules(List<IRule> rules)
    {
        Set<IRule> contained = new HashSet<>();
        QueryContainment qc = new QueryContainment(rules);
        for( IRule rule1 : rules )
        {
            for( IRule rule2 : rules )
            {
                if (false == rule1.equals(rule2)) {
                    Query q1 = new Query(rule1.getBody());
                    Query q2 = new Query(rule2.getBody());
                    try {
                        if (qc.checkQueryContainment(q1, q2)) {
                            contained.add(rule1);
                        }
                    } catch (EvaluationException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        rules.removeAll(contained);
    }
}

