package Experiments;

import Pattern.Pattern;
import Pattern.PatternNode;
import org.deri.iris.ConfigurationWithTopK;
import org.deri.iris.EvaluationException;
import org.deri.iris.api.basics.IPredicate;
import org.deri.iris.api.basics.IRule;
import org.deri.iris.api.basics.ITuple;
import org.deri.iris.compiler.Parser;
import org.deri.iris.compiler.ParserException;
import org.deri.iris.evaluation.stratifiedbottomup.seminaive.SemiNaiveEvaluator;
import org.deri.iris.facts.FactsWithTopK;
import org.deri.iris.facts.IFacts;
import org.deri.iris.optimisations.magicsets.MagicSets;
import org.deri.iris.optimisations.rulefilter.RuleFilter;
import org.deri.iris.rules.compiler.ICompiledRule;
import org.deri.iris.rules.compiler.RuleCompiler;
import org.deri.iris.storage.IRelation;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Created by user on 9/20/2017.
 */
public class SeminaiveExp {

    public static String [] runSeminaive(String path, boolean notCopyRelevantDerivedFacts, Set<String> leavesRelations, String nextFileToRun) {
        File program = new File(path);
        Reader reader = null;
        String[] resultArr = null;
        if (program.exists()) {
            try {
                reader = new FileReader(program);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                System.out.println("SeminaiveExp::runSeminaive: no such file");
            }

            // Parse the Datalog program.
            Parser parser = new Parser();
            try {
                parser.parse(reader);
            } catch (ParserException e) {
                e.printStackTrace();
            }

            // Retrieve the facts, rules and queries from the parsed program.
            Map<IPredicate, IRelation> factMap = parser.getFacts();
            List<IRule> rules = parser.getRules();

            // Create a default configuration.
            ConfigurationWithTopK configuration = new ConfigurationWithTopK();

            // Enable Magic Sets together with rule filtering.
            configuration.programOptmimisers.add(new RuleFilter());
            configuration.programOptmimisers.add(new MagicSets());

            // Convert the map from predicate to relation to a IFacts object.
            IFacts facts = new FactsWithTopK(factMap, configuration.relationFactory);

            int initialSize = 0;
            for (IPredicate predicate : facts.getPredicates())
                initialSize += facts.get(predicate).size();

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

            // Evaluate all queries over the knowledge base.
            long startTime = System.currentTimeMillis();
            List<ICompiledRule> cr = compile(rules, facts, configuration);
            SemiNaiveEvaluator sn = new SemiNaiveEvaluator();
            try {
                sn.evaluateRules(cr, facts, configuration);
            } catch (EvaluationException e) {
                System.out.println("SeminaiveExp::runSeminaive: problem with " + path);
                e.printStackTrace();
            }
            long endTime = System.currentTimeMillis();
            long durationSemiNaive = (endTime - startTime);

            int endSize = 0;
            for (IPredicate predicate : facts.getPredicates()) {
                endSize += facts.get(predicate).size();
            }

            resultArr = new String[]{Long.toString(durationSemiNaive),
                    Long.toString(initialSize), Long.toString(endSize)};// time for SN, size of initial DB, size of final DB

            if (false == notCopyRelevantDerivedFacts)
            {
                String curPath = getWorkingFilePath(path, nextFileToRun);
                writeLeavesFactsToDBFile(curPath, facts, leavesRelations);
            }
        }

        else {
            resultArr = new String[]{"0", "0", "0"};
        }
        return resultArr;
    }


    private static List<ICompiledRule> compile( List<IRule> rules, IFacts facts, ConfigurationWithTopK mConfiguration )
    {
        assert rules != null;
        assert facts != null;
        assert mConfiguration != null;

        List<ICompiledRule> compiledRules = new ArrayList<ICompiledRule>();

        RuleCompiler rc = new RuleCompiler( facts, mConfiguration.equivalentTermsFactory.createEquivalentTerms(), mConfiguration );

        for( IRule rule : rules )
            try {
                compiledRules.add( rc.compile( rule ) );
            } catch (EvaluationException e) {
                e.printStackTrace();
            }

        return compiledRules;
    }



    public static String getWorkingFilePath(String wrongNamePath, String rightName)
    {
        String pathNoFileName = wrongNamePath.substring(0, wrongNamePath.lastIndexOf(File.separator));
        return pathNoFileName + "\\" + rightName + ".iris";
    }



    public static void writeLeavesFactsToDBFile(String path, IFacts facts, Set<String> relations) {
        try{
            FileWriter writer = new FileWriter(path, true);
            BufferedWriter bw = new BufferedWriter(writer);
            for (IPredicate predicate : facts.getPredicates()) {
                if (relations.contains(predicate.toString())) {
                    String pred = predicate.toString();//.substring(0, predicate.toString().length() - 1);
                    for (int i = 0; i < facts.get(predicate).size(); i++) {
                        ITuple tuple = facts.get(predicate).get(i);
                        bw.write(pred + tuple + ".\n");
                    }
                }
            }
            bw.close();
            writer.close();
        } catch (IOException e) {
            System.out.println("SystemCommands::writeLeavesFactsToDBFile: Could not append program to file");
        }
    }
}
